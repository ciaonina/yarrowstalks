﻿using System;
using System.Collections.Generic;
using YSLib;
using YSLib.Products;
using YSLib.Products.Trigrams;

namespace YarrowStalks
{
    class Program
    {
        static void Main(string[] args)
        {
			var l1 = LineBuilder.Create(new[] { new ShotResult(Coin.Toss()), new ShotResult(Coin.Toss()), new ShotResult(Coin.Toss()) });
			var l2 = LineBuilder.Create(new[] { new ShotResult(Coin.Toss()), new ShotResult(Coin.Toss()), new ShotResult(Coin.Toss()) });
			var l3 = LineBuilder.Create(new[] { new ShotResult(Coin.Toss()), new ShotResult(Coin.Toss()), new ShotResult(Coin.Toss()) });
			

			var l4 = LineBuilder.Create(new[] { new ShotResult(Coin.Toss()), new ShotResult(Coin.Toss()), new ShotResult(Coin.Toss()) });
			var l5 = LineBuilder.Create(new[] { new ShotResult(Coin.Toss()), new ShotResult(Coin.Toss()), new ShotResult(Coin.Toss()) });
			var l6 = LineBuilder.Create(new[] { new ShotResult(Coin.Toss()), new ShotResult(Coin.Toss()), new ShotResult(Coin.Toss()) });

			var resolved = StackBuilder.Resolve(new List<BaseLine[]>
			{
				new BaseLine[] { l1, l2, l3}, new BaseLine[] { l4, l5, l6}
			});
			Trigram t1 = TrigramBuilder.Create(new[] { resolved[0][0], resolved[0][1], resolved[0][2] });
			Trigram t2 = TrigramBuilder.Create(new[] { resolved[1][0], resolved[1][1], resolved[1][2] });
			var _interpreter = new HexagramInterpreter();

	        var hex = _interpreter.Resolve(t1, t2);

			Console.WriteLine(hex.Sentency);
			Console.WriteLine(hex.Comment);
			Console.WriteLine(hex.Image);
			Console.WriteLine(hex.Series);
			Console.WriteLine(hex.MixedSign);
			Console.WriteLine(hex.AddictionalSentence);
	        Console.ReadLine();
        }
	}
}
