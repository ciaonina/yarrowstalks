﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using YSLib.Products;

namespace YSLib.Test
{
	public partial class HexagramExtractionProcessTest
	{
		[TestMethod]
		public void A_Coin_Toss_Should_Result_2_Or_3()
		{
			var value = Coin.Toss();
			Assert.IsTrue(value == 2 || value == 3, $"{value}");
			Console.WriteLine($"Coin toss:{value}");
		}
		[TestMethod]
		public void AtTheEndOfAShotOf_3Throw_ShouldBe_3_results()
		{
			var result = CoinShot.Run(3);
			Assert.AreEqual(3, result.Length);
		}
		[TestMethod]
		public void EveryShotResult_ShouldHave_A_Value_Between_2_and_3()
		{
			ShotResult[] result = CoinShot.Run(3);
			Assert.IsTrue(result.All(x => x.Value >= 2 && x.Value <= 3));
			var sb = new System.Text.StringBuilder();
			foreach (var shotResult in result)
			{
				sb.Append($"{shotResult.Value} + ");
			}
			Console.WriteLine($"{sb.ToString().TrimEnd('+')}");
		}
	}
}
