﻿using System.Collections.Generic;
using System.Linq;
using Castle.Components.DictionaryAdapter;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using YSLib.Products;
using YSLib.Products.Hexagrams;
using YSLib.Products.Trigrams;

namespace YSLib.Test
{
	[TestClass]
	public partial class HexagramExtractionProcessTest
	{
		private Coin _coin;
		private int[][] _grid;
		private List<Hexagram> _hexagrams;
		private List<Hexagram> _hexagramsWithoutAddictionalSentence;
		private List<Hexagram> _hexagramsWithAddictionalSentence;
		private HexagramInterpreter _interpreter;
		[TestInitialize]
		public void TestSetup()
		{
			_coin = new Coin();
			_interpreter = new HexagramInterpreter();
			_hexagrams = new List<Hexagram>()
			{
				new TheCreative(),
				new TheReceptive(),
				new TheInitialDifficulties(),
				new TheYouthFoolishness(),
				new TheWaiting(),
				new TheContention(),
				new TheArmy(),
				new TheSolidarity(),
				new TheStrengthOfTheLittleTamer(),
				new TheProceed(),
				new ThePeace(),
				new TheStagnation(),
				new TheLookForHarmony(),
				new TheGreatHarvest(),
				new TheModesty(),
				new TheFervor(),
				new TheFollow(),
				new TheFaultCorrection(),
				new TheApproach(),
				new TheContemplation(),
				new TheBiteThatBreaks(),
				new TheBeauty(),
				new TheCrumbling(),
				new TheReturn(),
				new TheInnocence(),
				new TheStrengthOfTheGreatTamer(),
				new TheNourishment(),
				new TheGreatExceed(),
				new TheAbyssal(),
				new TheBrightness(),
				new TheMutualInfluence(),
				new TheDuration(),
				new TheRetreat(),
				new TheGreatPower(),
				new TheProgress(),
				new TheObtenebrationLight(),
				new TheFamily(),
				new TheContrasting(),
				new TheDifficulties(),
				new TheLiberty(),
				new TheDecrease(),
				new TheGrafting(),
				new TheOverflowing(),
				new TheMeet(),
				new TheCollection(),
				new TheAscend(),
				new TheExhaustion(),
				new TheWell(),
				new TheRevolution(),
				new TheCrucible(),
				new TheThunder(),
				new TheStop(),
				new TheGradualProgress(),
				new TheGirlWhoMarries(),
				new TheAbundage(),
				new TheWanderer(),
				new TheProceedHumbly(),
				new TheClear(),
				new TheDissolution(),
				new TheDelimitation(),
				new TheInnerTruth(),
				new TheSmallExceed(),
				new TheAfterCompletion(),
				new TheBeforeTheAge()
			};
			_hexagramsWithoutAddictionalSentence = _hexagrams.Where(x => string.IsNullOrEmpty(x.AddictionalSentence)).ToList();
			_hexagramsWithAddictionalSentence = _hexagrams.Where(x => !string.IsNullOrEmpty(x.AddictionalSentence)).ToList();
			_grid = new int[8][];
			_grid[0] = new[] { 1, 34, 5, 26, 11, 9, 14, 43 };
			_grid[1] = new[] { 25, 51, 3, 27, 24, 42, 21, 17 };
			_grid[2] = new[] { 6, 40, 29, 4, 7, 59, 64, 47 };
			_grid[3] = new[] { 33, 62, 39, 52, 15, 53, 56, 31 };
			_grid[4] = new[] { 12, 16, 8, 23, 2, 20, 35, 45 };
			_grid[5] = new[] { 44, 32, 48, 18, 46, 57, 50, 28 };
			_grid[6] = new[] { 13, 55, 63, 22, 36, 37, 30, 49 };
			_grid[7] = new[] { 10, 54, 60, 41, 19, 61, 38, 58 };
		}
		[TestMethod]
		public void Quiann_and_Quian_became_the_creative()
		{
			BaseLine line = LineBuilder.Create(new ShotResult[3] { new ShotResult(3), new ShotResult(3), new ShotResult(3) });
			BaseLine line2 = LineBuilder.Create(new ShotResult[3] { new ShotResult(3), new ShotResult(3), new ShotResult(3) });
			BaseLine line3 = LineBuilder.Create(new ShotResult[3] { new ShotResult(3), new ShotResult(3), new ShotResult(3) });
			BaseLine line4 = LineBuilder.Create(new[] { new ShotResult(3), new ShotResult(2), new ShotResult(2) });
			BaseLine line5 = LineBuilder.Create(new ShotResult[3] { new ShotResult(3), new ShotResult(2), new ShotResult(2) });
			BaseLine line6 = LineBuilder.Create(new ShotResult[3] { new ShotResult(3), new ShotResult(2), new ShotResult(2) });
			var arr = new[] { line, line2, line3, line4, line5, line6 };
			List<BaseLine[]> resolved = StackBuilder.Resolve(new List<BaseLine[]>
			{
				new BaseLine[] { line, line2, line3}, new BaseLine[] { line4, line5, line6}
			});
			Trigram t1 = TrigramBuilder.Create(new[] { resolved[0][0], resolved[0][1], resolved[0][2] });
			t1.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Trigram t2 = TrigramBuilder.Create(new[] { resolved[1][0], resolved[1][1], resolved[1][2] });
			t2.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(t1, typeof(Quian));
			Assert.IsInstanceOfType(t2, typeof(Quian));
			Assert.IsInstanceOfType(_interpreter.Resolve(t1,t2), typeof(TheCreative));
		}
		[TestMethod]
		public void Zhen_and_Quian_became_The_Great_Power()
		{
			Trigram t1 = new Zhen();
			t1.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Trigram t2 = new Quian();
			t2.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(t1, typeof(Zhen));
			Assert.IsInstanceOfType(t2, typeof(Quian));
			Assert.IsInstanceOfType(_interpreter.Resolve(t1, t2), typeof(TheGreatPower));
		}
		[TestMethod]
		public void Kan_and_Quian_became_The_Waiting()
		{
			Trigram t1 = new Kan();
			t1.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Trigram t2 = new Quian();
			t2.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(t1, typeof(Kan));
			Assert.IsInstanceOfType(t2, typeof(Quian));
			Assert.IsInstanceOfType(_interpreter.Resolve(t1, t2), typeof(TheWaiting));
		}
		[TestMethod]
		public void Gen_and_Quian_became_The_strength_of_the_great_tamer()
		{
			Trigram t1 = new Gen();
			t1.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Trigram t2 = new Quian();
			t2.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(t1, typeof(Gen));
			Assert.IsInstanceOfType(t2, typeof(Quian));
			Assert.IsInstanceOfType(_interpreter.Resolve(t1, t2), typeof(TheStrengthOfTheGreatTamer));
		}
		[TestMethod]
		public void Xun_and_Li_became_the_family()
		{
			var xun = new Xun();
			xun.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var li = new Li();
			li.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(xun, li), typeof(TheFamily));
		}
		[TestMethod]
		public void Li_and_Li_became_the_Brightness()
		{
			var li = new Li();
			li.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var li1 = new Li();
			li1.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(li, li1), typeof(TheBrightness));
		}
		[TestMethod]
		public void Dui_and_Li_became_The_revolution()
		{
			var dui = new Dui();
			dui.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var li = new Li();
			li.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(dui, li), typeof(TheRevolution));
		}
		[TestMethod]
		public void Quian_and_Dui_became_the_Proceed()
		{
			var quian = new Quian();
			quian.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var dui = new Dui();
			dui.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(quian, dui), typeof(TheProceed));
		}
		[TestMethod]
		public void Zhen_and_Dui_became_The_girl_who_marries()
		{
			var zhen = new Zhen();
			zhen.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var dui = new Dui();
			dui.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(zhen, dui), typeof(TheGirlWhoMarries));
		}
		[TestMethod]
		public void Kan_and_Dui_became_the_Delimitation()
		{
			var kan = new Kan();
			kan.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var dui = new Dui();
			dui.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(kan, dui), typeof(TheDelimitation));
		}
		[TestMethod]
		public void Gen_and_Dui_becameThe_decrease()
		{
			var gen = new Gen();
			gen.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var dui = new Dui();
			dui.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(gen, dui), typeof(TheDecrease));
		}
		[TestMethod]
		public void Kun_and_Dui_became_The_Approach()
		{
			var kun = new Kun();
			kun.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var dui = new Dui();
			dui.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(kun, dui), typeof(TheApproach));
		}
		[TestMethod]
		public void Xun_and_Dui_became_The_Inner_Truth()
		{
			var xun = new Xun();
			xun.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var dui = new Dui();
			dui.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(xun, dui), typeof(TheInnerTruth));
		}
		[TestMethod]
		public void Li_and_Dui_became_The_Contrasting()
		{
			var li = new Li();
			li.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var dui = new Dui();
			dui.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(li, dui), typeof(TheContrasting));
		}
		[TestMethod]
		public void Dui_and_Dui_became_The_Clear()
		{
			var dui = new Dui();
			dui.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var dui1 = new Dui();
			dui1.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(dui, dui1), typeof(TheClear));
		}
		[TestMethod]
		public void Xun_and_Xun_became_The_proceed_humbly()
		{
			var xun = new Xun();
			xun.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var xun1 = new Xun();
			xun1.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(xun, xun1), typeof(TheProceedHumbly));
		}
		[TestMethod]
		public void Li_and_Xun_became_the_Crucible()
		{
			var li = new Li();
			li.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var xun = new Xun();
			xun.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(li, xun), typeof(TheCrucible));
		}
		[TestMethod]
		public void Dui_and_Xun_became_the_great_exceed()
		{
			var dui = new Dui();
			dui.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var xun = new Xun();
			xun.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(dui, xun), typeof(TheGreatExceed));
		}
		[TestMethod]
		public void Quian_and_Li_became_the_look_for_harmony()
		{
			var quian = new Quian();
			quian.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var li = new Li();
			li.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(quian, li), typeof(TheLookForHarmony));
		}
		[TestMethod]
		public void Zhen_and_Li_became_the_Abundage()
		{
			var zhen = new Zhen();
			zhen.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var li = new Li();
			li.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(zhen, li), typeof(TheAbundage));
		}
		[TestMethod]
		public void Kan_and_Li_became_the_After_completion()
		{
			var kan = new Kan();
			kan.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var li = new Li();
			li.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(kan, li), typeof(TheAfterCompletion));
		}
		[TestMethod]
		public void Gen_and_Li_became_the_Beauty()
		{
			var gen = new Gen();
			gen.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var li = new Li();
			li.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(gen, li), typeof(TheBeauty));
		}
		[TestMethod]
		public void Kun_and_Li_became_The_ObtenebrationLight()
		{
			var kun = new Kun();
			kun.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var li = new Li();
			li.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(kun, li), typeof(TheObtenebrationLight));
		}
		[TestMethod]
		public void Dui_and_Kun_became_the_Collection()
		{
			var dui = new Dui();
			dui.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var kun = new Kun();
			kun.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(dui, kun), typeof(TheCollection));
		}
		[TestMethod]
		public void Quian_and_Xun_became_the_Meet()
		{
			var quian = new Quian();
			quian.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var xun = new Xun();
			xun.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(quian, xun), typeof(TheMeet));
		}
		[TestMethod]
		public void Zhen_and_Xun_became_the_Duration()
		{
			var zhen = new Zhen();
			zhen.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var xun = new Xun();
			xun.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(zhen, xun), typeof(TheDuration));
		}
		[TestMethod]
		public void Kan_and_Xun_became_the_well()
		{
			var kan = new Kan();
			kan.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var xun = new Xun();
			xun.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(kan, xun), typeof(TheWell));
		}
		[TestMethod]
		public void Gen_and_Xun_became_The_fault_correction()
		{
			var gen = new Gen();
			gen.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var xun = new Xun();
			xun.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(gen, xun), typeof(TheFaultCorrection));
		}
		[TestMethod]
		public void Kun_and_Xun_became_The_Ascend()
		{
			var kun = new Kun();
			kun.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var xun = new Xun();
			xun.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(kun, xun), typeof(TheAscend));
		}
		[TestMethod]
		public void Quian_and_Kun_became_the_stagnation()
		{
			var quian = new Quian();
			quian.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var kun = new Kun();
			kun.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(quian, kun), typeof(TheStagnation));
		}
		[TestMethod]
		public void Zhen_and_Kun_became_the_fervor()
		{
			var zhen = new Zhen();
			zhen.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var kun = new Kun();
			kun.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(zhen, kun), typeof(TheFervor));
		}
		[TestMethod]
		public void Kan_and_Kun_became_the_Solidarity()
		{
			var kan = new Kan();
			kan.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var kun = new Kun();
			kun.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(kan, kun), typeof(TheSolidarity));
		}
		[TestMethod]
		public void Gen_and_Kun_became_the_Crumbling()
		{
			var gen = new Gen();
			gen.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var kun = new Kun();
			kun.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(gen, kun), typeof(TheCrumbling));
		}
		[TestMethod]
		public void Kun_and_Kun_became_the_Receptive()
		{
			var kun = new Kun();
			kun.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var kun1 = new Kun();
			kun1.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(kun, kun1), typeof(TheReceptive));
		}
		[TestMethod]
		public void Xun_and_Kun_became_the_Contemplation()
		{
			var xun = new Xun();
			xun.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var kun = new Kun();
			kun.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(xun, kun), typeof(TheContemplation));
		}
		[TestMethod]
		public void Li_and_Kun_became_the_Progress()
		{
			var li = new Li();
			li.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var kun = new Kun();
			kun.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(li, kun), typeof(TheProgress));
		}
		[TestMethod]
		public void Kan_and_Gen_became_the_difficulties()
		{
			var kan = new Kan();
			kan.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var gen = new Gen();
			gen.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(kan, gen), typeof(TheDifficulties));
		}
		[TestMethod]
		public void Gen_and_Gen_became_The_Stop()
		{
			var gen = new Gen();
			gen.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var gen1 = new Gen();
			gen1.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(gen, gen1), typeof(TheStop));
		}
		[TestMethod]
		public void Kun_and_Gen_became_The_Modesty()
		{
			var kun = new Kun();
			kun.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var gen = new Gen();
			gen.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(kun, gen), typeof(TheModesty));
		}
		[TestMethod]
		public void Xun_and_Gen_became_The_Gradual_Progress()
		{
			var xun = new Xun();
			xun.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var gen = new Gen();
			gen.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(xun, gen), typeof(TheGradualProgress));
		}
		[TestMethod]
		public void Li_and_Gen_became_the_Wanderer()
		{
			var li = new Li();
			li.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var gen = new Gen();
			gen.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(li, gen), typeof(TheWanderer));
		}
		[TestMethod]
		public void Dui_and_Gen_became_the_mutual_influence()
		{
			var dui = new Dui();
			dui.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var gen = new Gen();
			gen.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(dui, gen), typeof(TheMutualInfluence));
		}
		[TestMethod]
		public void Xun_and_Kan_became_The_dissolution()
		{
			var xun = new Xun();
			xun.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var kan = new Kan();
			kan.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(xun, kan), typeof(TheDissolution));
		}
		[TestMethod]
		public void Li_and_Kan_became_The_Before_the_age()
		{
			var li = new Li();
			li.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var kan = new Kan();
			kan.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(li, kan), typeof(TheBeforeTheAge));
		}
		[TestMethod]
		public void Dui_and_Kan_became_The_Exhaustion()
		{
			var dui = new Dui();
			dui.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var kan = new Kan();
			kan.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(dui, kan), typeof(TheExhaustion));
		}
		[TestMethod]
		public void Quian_and_Gen_became_the_Retreat()
		{
			var quian = new Quian();
			quian.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var gen = new Gen();
			gen.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(quian, gen), typeof(TheRetreat));
		}
		[TestMethod]
		public void Zhen_and_Gen_became_the_small_exceed()
		{
			var zhen = new Zhen();
			zhen.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var gen = new Gen();
			gen.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(zhen, gen), typeof(TheSmallExceed));
		}
		[TestMethod]
		public void Quian_and_Kan_became_the_Contention()
		{
			var quian = new Quian();
			quian.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var kan = new Kan();
			kan.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(quian, kan), typeof(TheContention));
		}
		[TestMethod]
		public void Zhen_and_Kan_became_The_Liberty()
		{
			var zhen = new Zhen();
			zhen.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var kan = new Kan();
			kan.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(zhen, kan), typeof(TheLiberty));
		}
		[TestMethod]
		public void Kan_and_Kan_became_The_Abyssal()
		{
			var kan = new Kan();
			kan.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var kan1 = new Kan();
			kan1.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(kan, kan1), typeof(TheAbyssal));
		}
		[TestMethod]
		public void Gen_and_Kan_became_The_Youth_Foolishness()
		{
			var gen = new Gen();
			gen.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var kan = new Kan();
			kan.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(gen, kan), typeof(TheYouthFoolishness));
		}
		[TestMethod]
		public void Kun_and_Kan_became_The_Army()
		{
			var kun = new Kun();
			kun.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var kan = new Kan();
			kan.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(kun, kan), typeof(TheArmy));
		}
		[TestMethod]
		public void Gen_and_Zhen_became_the_nourishment()
		{
			var gen = new Gen();
			gen.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var zhen = new Zhen();
			zhen.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(gen, zhen), typeof(TheNourishment));
		}
		[TestMethod]
		public void Kun_and_Zhen_became_the_return()
		{
			var kun = new Kun();
			kun.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var zhen = new Zhen();
			zhen.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(kun, zhen), typeof(TheReturn));
		}
		[TestMethod]
		public void Xun_and_Zhen_became_The_Grafting()
		{
			var xun = new Xun();
			xun.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var zhen = new Zhen();
			zhen.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(xun, zhen), typeof(TheGrafting));
		}
		[TestMethod]
		public void Li_and_Zhen_became_The_bite_that_breaks()
		{
			var li = new Li();
			li.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var zhen = new Zhen();
			zhen.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(li, zhen), typeof(TheBiteThatBreaks));
		}
		[TestMethod]
		public void Dui_and_Zhen_became_the_Follow()
		{
			var dui = new Dui();
			dui.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var zhen = new Zhen();
			zhen.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(dui, zhen), typeof(TheFollow));
		}
		[TestMethod]
		public void Xun_and_Quian_became_the_strength_of_the_little_tamer()
		{
			var xun = new Xun();
			xun.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var quian = new Quian();
			quian.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(xun, quian), typeof(TheStrengthOfTheLittleTamer));
		}
		[TestMethod]
		public void Li_and_Quian_became_the_great_harvest()
		{
			var li = new Li();
			li.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var quian = new Quian();
			quian.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(li, quian), typeof(TheGreatHarvest));
		}
		[TestMethod]
		public void Dui_and_Quian_became_the_overflowing()
		{
			var dui = new Dui();
			dui.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var quian = new Quian();
			quian.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(dui, quian), typeof(TheOverflowing));
		}
		[TestMethod]
		public void Quian_and_Zhen_became_The_Innocence()
		{
			var quian = new Quian();
			quian.Lines = new BaseLine[] {new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var zhen = new Zhen();
			zhen.Lines = new BaseLine[] { new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(quian, zhen), typeof(TheInnocence));
		}

		[TestMethod]
		public void Zhen_and_Zhen_became_The_Thunder()
		{
			var zhen = new Zhen();
			zhen.Lines = new BaseLine[] { new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var zhen1 = new Zhen();
			zhen1.Lines = new BaseLine[] { new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(zhen, zhen1), typeof(TheThunder));
		}
		[TestMethod]
		public void Kan_and_Zhen_became_The_initial_difficulties()
		{
			var kan = new Kan();
			kan.Lines = new BaseLine[] { new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var zhen = new Zhen();
			zhen.Lines = new BaseLine[] { new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(kan, zhen), typeof(TheInitialDifficulties));
		}
		[TestMethod]
		public void Kun_and_Quian_became_The_Peace()
		{
			var kun = new Kun();
			kun.Lines = new BaseLine[] { new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			var quian = new Quian();
			quian.Lines = new BaseLine[] { new FixedBrokenLine(), new FixedBrokenLine(), new FixedBrokenLine(), };
			Assert.IsInstanceOfType(_interpreter.Resolve(kun, quian), typeof(ThePeace));
		}
		[TestMethod]
		public void Kun_and_Quian_should_became_0_o_The_Peace()
		{
			var idx = _grid[new Quian().Id - 1][new Kun().Id - 1];
			var hexagram = _hexagrams.Single(x=> x.Id == idx);
			Assert.IsInstanceOfType(hexagram, typeof(ThePeace));
		}
	}
}