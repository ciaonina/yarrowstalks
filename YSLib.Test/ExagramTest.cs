﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using YSLib.Products;
using YSLib.Products.Hexagrams;
using YSLib.Products.Trigrams;

namespace YSLib.Test
{
	public partial class HexagramExtractionProcessTest
	{
		[TestMethod]
		public void every_hexagram_should_have_sentency_not_null_or_empty()
		{
			foreach (var hexagram in _hexagrams)
			{
				Assert.IsFalse(string.IsNullOrEmpty(hexagram.Sentency));
				Console.WriteLine(hexagram + " Sentency: " + hexagram.Sentency);
			}
		}
		[TestMethod]
		public void every_hexagram_should_have_comment_not_null_or_empty()
		{
			foreach (var hexagram in _hexagrams)
			{
				Assert.IsFalse(string.IsNullOrEmpty(hexagram.Comment));
				Console.WriteLine(hexagram + " Comment: " + hexagram.Comment);
			}
		}
		[TestMethod]
		public void every_hexagram_should_have_Image_not_null_or_empty()
		{
			foreach (var hexagram in _hexagrams)
			{
				Assert.IsFalse(string.IsNullOrEmpty(hexagram.Image));
				Console.WriteLine(hexagram + " Image: " + hexagram.Image);
			}
		}
		[TestMethod]
		public void every_hexagram_should_have_Series_not_null_or_empty()
		{
			foreach (var hexagram in _hexagrams)
			{
				Assert.IsFalse(string.IsNullOrEmpty(hexagram.Series));
				Console.WriteLine(hexagram + " Series: " + hexagram.Series);
			}
		}
		[TestMethod]
		public void every_hexagram_should_have_MixedSign_not_null_or_empty()
		{
			foreach (var hexagram in _hexagrams)
			{
				Assert.IsFalse(string.IsNullOrEmpty(hexagram.MixedSign), hexagram + " should have MixedSign not null or empty");
				Console.WriteLine(hexagram + " MixedSign: " + hexagram.MixedSign);
			}
		}
		[TestMethod]
		public void every_hexagramWithAddictionalSentence_should_have_AddictionalSentence_not_null_or_empty()
		{
			foreach (var hexagram in _hexagramsWithAddictionalSentence)
			{
				Assert.IsFalse(string.IsNullOrEmpty(hexagram.AddictionalSentence), hexagram.ToString() + " should have AddictionalSentence not null nor empty.");
			}
		}
		[TestMethod]
		public void every_hexagramsWithoutAddictionalSentence_should_have_AddictionalSentence_null_or_empty()
		{
			foreach (var hexagram in _hexagramsWithoutAddictionalSentence)
			{
				Assert.IsTrue(string.IsNullOrEmpty(hexagram.AddictionalSentence), hexagram.ToString() + " should have AddictionalSentence null or empty.");
			}
		}

		[TestMethod]
		public void every_hexagram_should_have_6_indexed_mobile_interpretation()
		{
			_hexagrams.ForEach(hexagram =>
			{
				Assert.IsTrue(hexagram.MobileInterpretations.Length >= 6);
			});
		}
		[TestMethod]
		public void ppp()
		{
			BaseLine line = LineBuilder.Create(new ShotResult[3] { new ShotResult(3), new ShotResult(3), new ShotResult(3) });
			BaseLine line2 = LineBuilder.Create(new ShotResult[3] { new ShotResult(3), new ShotResult(3), new ShotResult(3) });
			BaseLine line3 = LineBuilder.Create(new ShotResult[3] { new ShotResult(3), new ShotResult(3), new ShotResult(3) });
			BaseLine line4 = LineBuilder.Create(new[] { new ShotResult(3), new ShotResult(2), new ShotResult(2) });
			BaseLine line5 = LineBuilder.Create(new ShotResult[3] { new ShotResult(3), new ShotResult(2), new ShotResult(2) });
			BaseLine line6 = LineBuilder.Create(new ShotResult[3] { new ShotResult(3), new ShotResult(2), new ShotResult(2) });
			var arr = new[] { line, line2, line3, line4, line5, line6 };
			List<BaseLine[]> resolved = StackBuilder.Resolve(new List<BaseLine[]>
			{
				new BaseLine[] { line, line2, line3}, new BaseLine[] { line4, line5, line6}
			});
			var mobiles = new List<bool>();
			resolved.ForEach(lines =>{ mobiles.AddRange(lines.Select(l => l.isMobile())); });
			Trigram t1 = TrigramBuilder.Create(new[] { resolved[0][0], resolved[0][1], resolved[0][2] });
			Trigram t2 = TrigramBuilder.Create(new[] { resolved[1][0], resolved[1][1], resolved[1][2] });
			Assert.IsInstanceOfType(t1, typeof(Quian));
			Assert.IsInstanceOfType(t2, typeof(Quian));
			var result = _interpreter.Resolve(t1, t2);
			var expectedValue = "Talvolta salta giù nell'abisso. Nessuna sfortuna.";
			Assert.AreEqual(expectedValue, result.Sentency);
		}
	}
}
