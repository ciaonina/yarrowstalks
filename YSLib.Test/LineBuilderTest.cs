﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using YSLib.Products;
using YSLib.Products.Trigrams;

namespace YSLib.Test
{
	public partial class HexagramExtractionProcessTest
	{
		[TestMethod]
		public void _6_As_result_result_in_MobileCrossLine()
		{
			ShotResult[] p = new ShotResult[3] { new ShotResult(2), new ShotResult(2), new ShotResult(2) };
			var line = LineBuilder.Create(p);
			Assert.AreEqual(typeof(MobileBrokenLine), line.GetType(), "");
		}
		[TestMethod]
		public void _7_As_result_result_in_FixFullLine()
		{
			var p = new ShotResult[3] { new ShotResult(3), new ShotResult(2), new ShotResult(2) };
			var line = LineBuilder.Create(p);
			Assert.AreEqual(typeof(FixedFullLine), line.GetType(), "");
		}
		[TestMethod]
		public void _8_As_result_result_in_FixedBreakedLine()
		{
			var p = new ShotResult[3] { new ShotResult(3), new ShotResult(3), new ShotResult(2) };
			var line = LineBuilder.Create(p);
			Assert.AreEqual(typeof(FixedBrokenLine), line.GetType(), "");
		}
		[TestMethod]
		public void _9_As_result_result_in_MobileFullLine()
		{
			var p = new ShotResult[3] { new ShotResult(3), new ShotResult(3), new ShotResult(3) };
			var line = LineBuilder.Create(p);
			Assert.AreEqual(typeof(MobileFullLine), line.GetType(), "");
		}
		[TestMethod]
		public void after_the_extraction_should_be_6_Line()
		{
			var stack = StackBuilder.Create(6);
			Assert.AreEqual(6, stack.Length);
		}
		[TestMethod]
		public void after_the_extraction_every_line_should_not_be_null()
		{
			var stack = StackBuilder.Create(6);
			Assert.AreEqual(0, stack.Count(x => x == null));
		}
		[TestMethod]
		public void a_broken_mobile_line_became_a_full_fixed_line()
		{
			BaseLine linea_spezzata_mobile = LineBuilder.Create(new ShotResult[3] { new ShotResult(2), new ShotResult(2), new ShotResult(2) });
			LineBuilder.Create(new ShotResult[3] { new ShotResult(3), new ShotResult(2), new ShotResult(2) });
			List<BaseLine[]> trigrams = new List<BaseLine[]>() { new BaseLine[] { linea_spezzata_mobile/*, linea_intera_fissa */} };
			List<BaseLine[]> resolved = StackBuilder.Resolve(trigrams);
			Assert.IsInstanceOfType(resolved[0][0], typeof(FixedFullLine));
		}
		[TestMethod]
		public void a_full_fixed_line_became_a_broken_mobile_line()
		{
			LineBuilder.Create(new ShotResult[3] { new ShotResult(2), new ShotResult(2), new ShotResult(2) });
			BaseLine linea_intera_fissa = LineBuilder.Create(new ShotResult[3] { new ShotResult(3), new ShotResult(2), new ShotResult(2) });
			List<BaseLine[]> trigrams = new List<BaseLine[]>() { new BaseLine[] { linea_intera_fissa } };
			List<BaseLine[]> resolved = StackBuilder.Resolve(trigrams);
			Assert.IsInstanceOfType(resolved[0][0], typeof(MobileBrokenLine));
		}
		[TestMethod]
		public void a_full_fixed_line_doesnt_change()
		{
			BaseLine linea_intera_mobile = LineBuilder.Create(new ShotResult[3] { new ShotResult(3), new ShotResult(3), new ShotResult(3) });
			List<BaseLine[]> trigrams = new List<BaseLine[]>() { new BaseLine[] { linea_intera_mobile } };
			List<BaseLine[]> resolved = StackBuilder.Resolve(trigrams);
			Assert.IsInstanceOfType(resolved[0][0], typeof(MobileFullLine));
		}
		[TestMethod]
		public void a_broken_fixed_mobile_doesnt_change()
		{
			BaseLine linea_intera_mobile = LineBuilder.Create(new ShotResult[3] { new ShotResult(3), new ShotResult(3), new ShotResult(2) });
			List<BaseLine[]> trigrams = new List<BaseLine[]>() { new BaseLine[] { linea_intera_mobile } };
			List<BaseLine[]> resolved = StackBuilder.Resolve(trigrams);
			Assert.IsInstanceOfType(resolved[0][0], typeof(FixedBrokenLine));
		}
		[TestMethod]
		public void resolve_doesnt_change_trigrams_in_the_second_exagram()
		{
			BaseLine full_fixed_line = LineBuilder.Create(new ShotResult[3] { new ShotResult(3), new ShotResult(2), new ShotResult(2) });
			List<BaseLine[]> resolved = StackBuilder.Resolve(new List<BaseLine[]> { new BaseLine[] { full_fixed_line }, new BaseLine[] { full_fixed_line } });
			Assert.IsInstanceOfType(resolved[1][0], typeof(FixedFullLine));
		}
		[TestMethod]
		public void fff_became_Qian()
		{
			BaseLine full_fixed_line1 = LineBuilder.Create(new[] { new ShotResult(3), new ShotResult(2), new ShotResult(2) });
			BaseLine full_fixed_line2 = LineBuilder.Create(new ShotResult[3] { new ShotResult(3), new ShotResult(2), new ShotResult(2) });
			BaseLine full_fixed_line3 = LineBuilder.Create(new ShotResult[3] { new ShotResult(3), new ShotResult(2), new ShotResult(2) });
			Trigram quian = TrigramBuilder.Create(new[] { full_fixed_line1, full_fixed_line2, full_fixed_line3 });
			Assert.IsInstanceOfType(quian, typeof(Quian));

		}
		[TestMethod]
		public void bbf_became_Zhen()
		{
			BaseLine bl1 = LineBuilder.Create(new[] { new ShotResult(2), new ShotResult(2), new ShotResult(2) });
			BaseLine bl2 = LineBuilder.Create(new[] { new ShotResult(2), new ShotResult(2), new ShotResult(2) });
			BaseLine fl1 = LineBuilder.Create(new ShotResult[] { new ShotResult(3), new ShotResult(2), new ShotResult(2) });
			Trigram quian = TrigramBuilder.Create(new[] { bl1, bl2, fl1 });
			Assert.IsInstanceOfType(quian, typeof(Zhen));
		}
		[TestMethod]
		public void bbf_became_Zhen2()
		{
			BaseLine bl1 = LineBuilder.Create(new[] { new ShotResult(2), new ShotResult(2), new ShotResult(2) });
			BaseLine bl2 = LineBuilder.Create(new[] { new ShotResult(2), new ShotResult(2), new ShotResult(2) });
			BaseLine fl1 = LineBuilder.Create(new ShotResult[3] { new ShotResult(3), new ShotResult(3), new ShotResult(3) });
			Trigram quian = TrigramBuilder.Create(new[] { bl1, bl2, fl1 });
			Assert.IsInstanceOfType(quian, typeof(Zhen));
		}
	}
}
