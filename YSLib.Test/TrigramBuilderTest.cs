﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using YSLib.Products;
using YSLib.Products.Trigrams;

namespace YSLib.Test
{
	public partial class HexagramExtractionProcessTest
	{
		[TestMethod]
		public void the_first_fixed_line_remove_all_unmatching_hexagrams()
		{
			var quian = "fff";
			var zhen = "bbf";
			var kan = "bfb";
			var gen = "fbb";
			var kun = "bbb";
			var xun = "ffb";
			var li = "fbf";
			var dui = "bff";
			var arr = new List<string>()
			{
				quian,
				zhen,
				kan,
				gen,
				kun,
				xun,
				li,
				dui
			};
			var brokenline = LineBuilder.Create(new[] { new ShotResult(2), new ShotResult(2), new ShotResult(2) });
			var _1 = arr.Where(x => x.First().ToString() == (brokenline.isBroken() ? "b" : "f"));
			Assert.AreEqual(4, _1.Count());
		}
		[TestMethod]
		public void the_first_fixed_line_remove_all_unmatching_hexagrams2()
		{
			var quian = "fff";
			var zhen = "bbf";
			var kan = "bfb";
			var gen = "fbb";
			var kun = "bbb";
			var xun = "ffb";
			var li = "fbf";
			var dui = "bff";
			var arr = new List<string>()
			{
				quian,
				zhen,
				kan,
				gen,
				kun,
				xun,
				li,
				dui
			};
			var brokenline = LineBuilder.Create(new[] { new ShotResult(2), new ShotResult(2), new ShotResult(2) });
			var fixedline = LineBuilder.Create(new ShotResult[3] { new ShotResult(3), new ShotResult(2), new ShotResult(2) });
			var _1 = arr.Where(x => x.First().ToString() == (brokenline.isBroken() ? "b" : "f"));
			Assert.AreEqual(4, _1.Count());
			var _2 = _1.Where(x => x[1].ToString() == (fixedline.isBroken() ? "b" : "f"));
			Assert.AreEqual(2, _2.Count());
		}
		[TestMethod]
		public void mb_ff_fb_fb_fb_fb_resolve_gen_kun()
		{
			ShotResult[] p1 = new ShotResult[3] { new ShotResult(2), new ShotResult(2), new ShotResult(2) };
			BaseLine line = LineBuilder.Create(p1); // MobileBrokenLine
			ShotResult[] p2 = new ShotResult[3] { new ShotResult(3), new ShotResult(2), new ShotResult(2) };
			BaseLine line2 = LineBuilder.Create(p2); // FixedFullLine
			ShotResult[] p3 = new ShotResult[3] { new ShotResult(3), new ShotResult(3), new ShotResult(2) };
			BaseLine line3 = LineBuilder.Create(p3); // FixedBrokenLine
			ShotResult[] p4 = new ShotResult[3] { new ShotResult(3), new ShotResult(3), new ShotResult(2) };
			BaseLine line4 = LineBuilder.Create(p4); // FixedBrokenLine
			BaseLine line5 = LineBuilder.Create(p4); // FixedBrokenLine
			BaseLine line6 = LineBuilder.Create(p4); // FixedBrokenLine
			var arr = new[] { line, line2, line3, line4, line5, line6 };
			List<BaseLine[]> resolved = StackBuilder.Resolve(new List<BaseLine[]>
			{
				new BaseLine[] { line, line2, line3}, new BaseLine[] { line4, line5, line6}
			});
			Trigram e1 = TrigramBuilder.Create(new[] { resolved[0][0], resolved[0][1], resolved[0][2] });
			Trigram e2 = TrigramBuilder.Create(new[] { resolved[1][0], resolved[1][1], resolved[1][2] });
			Assert.IsInstanceOfType(e1, typeof(Gen));
			Assert.IsInstanceOfType(e2, typeof(Kun));

		}
		[TestMethod]
		public void mf_ff_fb_fb_fb_fb_resolve_gen_kun()
		{
			ShotResult[] p1 = new ShotResult[3] { new ShotResult(3), new ShotResult(3), new ShotResult(3) };
			BaseLine line = LineBuilder.Create(p1); // MobilefullLine
			ShotResult[] p2 = new ShotResult[3] { new ShotResult(3), new ShotResult(2), new ShotResult(2) };
			BaseLine line2 = LineBuilder.Create(p2); // FixedFullLine
			ShotResult[] p3 = new ShotResult[3] { new ShotResult(3), new ShotResult(3), new ShotResult(2) };
			BaseLine line3 = LineBuilder.Create(p3); // FixedBrokenLine
			ShotResult[] p4 = new ShotResult[3] { new ShotResult(3), new ShotResult(3), new ShotResult(2) };
			BaseLine line4 = LineBuilder.Create(p4); // FixedBrokenLine
			BaseLine line5 = LineBuilder.Create(p4); // FixedBrokenLine
			BaseLine line6 = LineBuilder.Create(p4); // FixedBrokenLine
			var arr = new[] { line, line2, line3, line4, line5, line6 };
			List<BaseLine[]> resolved = StackBuilder.Resolve(new List<BaseLine[]>
			{
				new BaseLine[] { line, line2, line3}, new BaseLine[] { line4, line5, line6}
			});
			Trigram e1 = TrigramBuilder.Create(new[] { resolved[0][0], resolved[0][1], resolved[0][2] });
			Trigram e2 = TrigramBuilder.Create(new[] { resolved[1][0], resolved[1][1], resolved[1][2] });
			Assert.IsInstanceOfType(e1, typeof(Gen));
			Assert.IsInstanceOfType(e2, typeof(Kun));
		}
		[TestMethod]
		public void bfb_became_Kan()
		{
			BaseLine bl1 = LineBuilder.Create(new[] { new ShotResult(2), new ShotResult(2), new ShotResult(2) });
			BaseLine fl1 = LineBuilder.Create(new ShotResult[] { new ShotResult(3), new ShotResult(2), new ShotResult(2) });
			BaseLine bl2 = LineBuilder.Create(new[] { new ShotResult(2), new ShotResult(2), new ShotResult(2) });
			Trigram kan = TrigramBuilder.Create(new[] { bl1, fl1, bl2 });
			Assert.IsInstanceOfType(kan, typeof(Kan));
		}
		[TestMethod]
		public void fbb_became_Gen()
		{
			BaseLine fl1 = LineBuilder.Create(new ShotResult[3] { new ShotResult(3), new ShotResult(2), new ShotResult(2) });
			BaseLine bl1 = LineBuilder.Create(new[] { new ShotResult(2), new ShotResult(2), new ShotResult(2) });
			BaseLine bl2 = LineBuilder.Create(new[] { new ShotResult(2), new ShotResult(2), new ShotResult(2) });
			Trigram gen = TrigramBuilder.Create(new[] { fl1, bl1, bl2 });
			Assert.IsInstanceOfType(gen, typeof(Gen));
		}
		[TestMethod]
		public void bbb_became_Kun()
		{
			BaseLine bl1 = LineBuilder.Create(new[] { new ShotResult(2), new ShotResult(2), new ShotResult(2) });
			BaseLine bl2 = LineBuilder.Create(new[] { new ShotResult(2), new ShotResult(2), new ShotResult(2) });
			BaseLine bl3 = LineBuilder.Create(new[] { new ShotResult(2), new ShotResult(2), new ShotResult(2) });
			Trigram kun = TrigramBuilder.Create(new[] { bl1, bl2, bl3 });
			Assert.IsInstanceOfType(kun, typeof(Kun));
		}
		[TestMethod]
		public void ffb_became_Xun()
		{
			BaseLine fl1 = LineBuilder.Create(new ShotResult[3] { new ShotResult(3), new ShotResult(2), new ShotResult(2) });
			BaseLine fl2 = LineBuilder.Create(new ShotResult[3] { new ShotResult(3), new ShotResult(2), new ShotResult(2) });
			BaseLine bl1 = LineBuilder.Create(new[] { new ShotResult(2), new ShotResult(2), new ShotResult(2) });
			Trigram xun = TrigramBuilder.Create(new[] { fl1, fl2, bl1 });
			Assert.IsInstanceOfType(xun, typeof(Xun));
		}
		[TestMethod]
		public void fbf_became_Li()
		{
			BaseLine fl1 = LineBuilder.Create(new ShotResult[3] { new ShotResult(3), new ShotResult(2), new ShotResult(2) });
			BaseLine bl1 = LineBuilder.Create(new[] { new ShotResult(2), new ShotResult(2), new ShotResult(2) });
			BaseLine fl2 = LineBuilder.Create(new ShotResult[3] { new ShotResult(3), new ShotResult(2), new ShotResult(2) });
			Trigram li = TrigramBuilder.Create(new[] { fl1, bl1, fl2 });
			Assert.IsInstanceOfType(li, typeof(Li));
		}
		[TestMethod]
		public void bff_became_Dui()
		{
			BaseLine bl1 = LineBuilder.Create(new[] { new ShotResult(2), new ShotResult(2), new ShotResult(2) });
			BaseLine fl1 = LineBuilder.Create(new ShotResult[3] { new ShotResult(3), new ShotResult(2), new ShotResult(2) });
			BaseLine fl2 = LineBuilder.Create(new ShotResult[3] { new ShotResult(3), new ShotResult(2), new ShotResult(2) });
			Trigram dui = TrigramBuilder.Create(new[] { bl1, fl1, fl2 });
			Assert.IsInstanceOfType(dui, typeof(Dui));
		}
	}
}
