﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace YSLib
{
	namespace Products
	{
		public static class CoinShot
		{
			public static ShotResult[] Run(int thrown)
			{
				var result = new List<ShotResult>();
				for (var i = 0; i < thrown; i++)
				{
					result.Add(new ShotResult(Coin.Toss()));
				}
				return result.ToArray();
			}
		}
		public class ShotResult
		{
			public ShotResult(int value)
			{
				Value = value;
			}
			public int Value { get; private set; }
		}
		public class Coin
		{
			private enum eYingYang
			{
				Ying = 2,
				Yang = 3
			}
			public static int Toss()
			{
				var randomNumber = new byte[1];
				using (var gen = new RNGCryptoServiceProvider())
				{
					gen.GetBytes(randomNumber);
					var rand = Convert.ToInt32(randomNumber[0]);
					return rand % 2 == 0 ? (int)eYingYang.Ying : (int)eYingYang.Yang;
				}
			}
		}
	}
}

