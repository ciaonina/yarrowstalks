﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YSLib
{
	namespace Products
	{
		public enum eLineType
		{
			MobileBrokenLine = 6,
			FixedFullLine = 7,
			FixedBrokenLine = 8,
			MobileFullLine = 9
		}
		public abstract class BaseLine
		{
			public abstract bool isBroken();
			public abstract bool isMobile();

		}
		public class MobileFullLine : BaseLine
		{
			public override bool isBroken()
			{
				return false;
			}

			public override bool isMobile()
			{
				return true;
			}
		}
		public class FixedBrokenLine : BaseLine
		{
			public override bool isBroken()
			{
				return true;
			}

			public override bool isMobile()
			{
				return false;
			}
		}
		public class FixedFullLine : BaseLine
		{
			public override bool isBroken()
			{
				return false;
			}

			public override bool isMobile()
			{
				return false;
			}
		}
		public class MobileBrokenLine : BaseLine
		{
			public override bool isBroken()
			{
				return true;
			}

			public override bool isMobile()
			{
				return true;
			}
		}
	}
}
