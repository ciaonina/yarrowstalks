﻿using System.Collections.Generic;
using System.Linq;
using YSLib.Products;
using YSLib.Products.Hexagrams;
using YSLib.Products.Trigrams;

namespace YSLib
{
	public class HexagramInterpreter
	{
		private int[][] Grid { get; }

		public HexagramInterpreter()
		{
			this.Grid = new int[8][];
			this.Grid[0] = new[] { 1, 34, 5, 26, 11, 9, 14, 43 };
			this.Grid[1] = new[] { 25, 51, 3, 27, 24, 42, 21, 17 };
			this.Grid[2] = new[] { 6, 40, 29, 4, 7, 59, 64, 47 };
			this.Grid[3] = new[] { 33, 62, 39, 52, 15, 53, 56, 31 };
			this.Grid[4] = new[] { 12, 16, 8, 23, 2, 20, 35, 45 };
			this.Grid[5] = new[] { 44, 32, 48, 18, 46, 57, 50, 28 };
			this.Grid[6] = new[] { 13, 55, 63, 22, 36, 37, 30, 49 };
			this.Grid[7] = new[] { 10, 54, 60, 41, 19, 61, 38, 58 };
			this.Trigrams = new List<Hexagram>()
			{
				new TheCreative(),
				new TheReceptive(),
				new TheInitialDifficulties(),
				new TheYouthFoolishness(),
				new TheWaiting(),
				new TheContention(),
				new TheArmy(),
				new TheSolidarity(),
				new TheStrengthOfTheLittleTamer(),
				new TheProceed(),
				new ThePeace(),
				new TheStagnation(),
				new TheLookForHarmony(),
				new TheGreatHarvest(),
				new TheModesty(),
				new TheFervor(),
				new TheFollow(),
				new TheFaultCorrection(),
				new TheApproach(),
				new TheContemplation(),
				new TheBiteThatBreaks(),
				new TheBeauty(),
				new TheCrumbling(),
				new TheReturn(),
				new TheInnocence(),
				new TheStrengthOfTheGreatTamer(),
				new TheNourishment(),
				new TheGreatExceed(),
				new TheAbyssal(),
				new TheBrightness(),
				new TheMutualInfluence(),
				new TheDuration(),
				new TheRetreat(),
				new TheGreatPower(),
				new TheProgress(),
				new TheObtenebrationLight(),
				new TheFamily(),
				new TheContrasting(),
				new TheDifficulties(),
				new TheLiberty(),
				new TheDecrease(),
				new TheGrafting(),
				new TheOverflowing(),
				new TheMeet(),
				new TheCollection(),
				new TheAscend(),
				new TheExhaustion(),
				new TheWell(),
				new TheRevolution(),
				new TheCrucible(),
				new TheThunder(),
				new TheStop(),
				new TheGradualProgress(),
				new TheGirlWhoMarries(),
				new TheAbundage(),
				new TheWanderer(),
				new TheProceedHumbly(),
				new TheClear(),
				new TheDissolution(),
				new TheDelimitation(),
				new TheInnerTruth(),
				new TheSmallExceed(),
				new TheAfterCompletion(),
				new TheBeforeTheAge()
			};
		}

		private List<Hexagram> Trigrams { get; }

		public Hexagram Resolve(Trigram t1, Trigram t2)
		{
			var mobiles = new List<bool>();
			mobiles.AddRange(t2.Lines.Select(l => l.isMobile()));
			mobiles.AddRange(t1.Lines.Select(l => l.isMobile()));

			
			var rr = Trigrams.Single(x => x.Id == Grid[t2.Id - 1][t1.Id - 1]);
			var ipoteticalResult = string.Empty;
			for (int i = 0; i < mobiles.Count; i++)
			{
				if (mobiles[i])
				{
					ipoteticalResult = rr.MobileInterpretations[i];
					break;
				}
			}

			rr.Sentency = !string.IsNullOrEmpty(ipoteticalResult) ? ipoteticalResult : rr.Sentency;
			return rr;
		}
	}
	public static class LineBuilder
	{
		public static BaseLine Create(IEnumerable<ShotResult> run)
		{
			BaseLine result = null;
			switch (run.Sum(x => x.Value))
			{
				case (int)eLineType.MobileBrokenLine:
					result = new MobileBrokenLine();
					break;
				case (int)eLineType.FixedFullLine:
					result = new FixedFullLine();
					break;
				case (int)eLineType.FixedBrokenLine:
					result = new FixedBrokenLine();
					break;
				case (int)eLineType.MobileFullLine:
					result = new MobileFullLine();
					break;
			}
			return result;
		}
	}
	public static class StackBuilder
	{
		public static BaseLine[] Create(int count)
		{
			var result = new List<BaseLine>();
			for (var i = 0; i < count; i++)
			{
				result.Add(LineBuilder.Create(CoinShot.Run(3)));
			}
			return result.ToArray();
		}
		public static List<BaseLine[]> Resolve(List<BaseLine[]> trigrams)
		{
			var results = new List<BaseLine[]>();
			for (var i = 0; i < trigrams.Count(); i++)
			{
				var newTrigram = new List<BaseLine>();
				foreach (var line in trigrams[i])
				{
					if (line.GetType() == typeof(MobileBrokenLine) && i == 0)
					{
						newTrigram.Add(new FixedFullLine());
					}
					else if (line.GetType() == typeof(FixedFullLine) && i == 0)
					{
						newTrigram.Add(new MobileBrokenLine());
					}
					else
					{
						newTrigram.Add(line);
					}
				}
				results.Add(newTrigram.ToArray());
			}
			return results;
		}
	}
	public static class TrigramBuilder
	{
		public static Trigram Create(BaseLine[] lines)
		{
			var arr = new List<string> { "fff", "bbf", "bfb", "fbb", "bbb", "ffb", "fbf", "bff" };
			var first = arr.Where(x => x[0].ToString() == (lines[0].isBroken() ? "b" : "f"));
			var second = first.Where(x => x[1].ToString() == (lines[1].isBroken() ? "b" : "f"));
			var third = second.Single(x => x[2].ToString() == (lines[2].isBroken() ? "b" : "f"));
			Trigram result = null;
			switch (third)
			{
				case "fff":
					result = new Quian {Lines = lines};
					break;
				case "bbf":
					result = new Zhen {Lines = lines};
					break;
				case "bfb":
					result = new Kan {Lines = lines};
					break;
				case "fbb":
					result = new Gen {Lines = lines};
					break;
				case "bbb":
					result = new Kun {Lines = lines};
					break;
				case "ffb":
					result = new Xun {Lines = lines};
					break;
				case "fbf":
					result = new Li {Lines = lines};
					break;
				case "bff":
					result = new Dui {Lines = lines};
					break;
			}
			return result;
		}
	}
}

