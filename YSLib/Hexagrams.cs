﻿namespace YSLib
{
	namespace Products
	{
		namespace Hexagrams
		{
			public abstract class Hexagram
			{
				public abstract int Id { get; }
				public abstract string Sentency { get; set; }
				public abstract string Comment { get; }
				public abstract string Image { get; }
				public abstract string Series { get; }
				public abstract string MixedSign { get; }
				public abstract string AddictionalSentence { get; }
				public abstract string[] MobileInterpretations { get; }
			}
			/// <summary>
			/// 1
			/// </summary>
			public class TheCreative : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
				  {
						"Il drago è nascosto sotto il lago.Non agire.",
						"Il drago compare nel campo.Propizio vedere un grande uomo.",
						"Un signore dev'essere attivo durante tutto il giorno e vigile la sera. Pericoloso. Nessuna sfortuna.",
						"Talvolta salta giù nell'abisso. Nessuna sfortuna.",
						"Il drago vola nel cielo.Propizio vedere un grande uomo.",
						"Il drago vola via.Ci sarà rimpianto.",
						"con la virtù del Cielo nessuno può pretendere di diventare il capo."
					};
					}
					
				}

				public override int Id => 1;
				public override string Sentency { get; set; } = "Il Creativo. Suprema riuscita. Propizio oracolo.";

				public override string Comment => "Il Creativo. Grande è la sublimità del Cielo che dà inizio a tutte le cose e pervade il cielo. Grazie a lui si muovono le nuvole, cade la pioggia, e tutti gli esseri fluiscono verso la loro forma particolare. Così chiaro e luminoso dall'inizio alla fine, ciascuno dei sei stadi lo completa a tempo debito, come salendo su sei draghi che volano nel cielo. La via del Cielo opera per mutamento e trasformazione, dando ad ogni cosa la sua vera natura; il «propizio oracolo» manifesta tutte le cose in una grande armonia. La sua virtù supera quelle di tutte le altre cose, garantendo pace e tranquillità a diecimila paesi.";
				public override string Image => "Il Cielo si muove vigoroso. Ispirandosi a questa immagine, il signore si rende forte e instancabile.";
				public override string Series => "Dopo che il Cielo e la Terra sono venuti all'esistenza, miriadi di esseri sono stati prodotti. Il Creativo e il Ricettivo sono l'origine, la sorgente della della creazione.";
				public override string MixedSign => "Il Creativo è forte.";
				public override string AddictionalSentence => "";

			}
			/// <summary>
			/// 2
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yin (6): Adesso si calpesta la brina. Il ghiaccio compatto verrà presto.
Commento: «Adesso si calpesta la brina. Il ghiaccio compatto verrà presto». Ciò vuol dire che lo Yin comincia a coagularsi e, continuando la sua via, si muta in ghiaccio compatto.
Linea l - Adesso la brina, poi il ghiaccio
Quello che vedete ora indica ciò che deve ancora accadere. Potete fare bei progetti per il futuro. Stroncate una situazione negativa sul nascere.
Linea 2 - Yin (6): Rigido e quadrato. Grandi venti soffieranno. [1] Nulla che non sia propizio.
Commento: Il movimento della seconda linea morbida è «rigido e quindi quadrato. E' senza intenzione; nulla che non sia propizio», perché nella natura della Terra c'è splendore.
Linea 2 - Rispettoso e corretto 
Siete nel posto giusto al momento giusto. Lasciate che le vostre azioni siano dirette e spontanee. Reagendo spontaneamente alle circostanze, le vostre azioni sono giuste.
Linea 3 - Yin (6): Splendore nascosto. L'oracolo è possibile [che sia favorevole]. Se si mette al servizio del Re, non cercherà rapido successo, ma porterà le cose al loro compimento.
Commento: «Splendore nascosto; l'oracolo è possibile»: bisogna farlo risplendere a tempo debito. «Se si mette al servizio del Re, non cercherà rapido successo, ma porterà le cose al loro compimento», ciò mostra che lo splendore della saggezza è grande.
Linea 3 - Splendore nascosto 
Lavorate sullo sfondo e portate a termine fedelmente le cose. Non è né il momento né il luogo per reclamare credito per voi stessi. Potreste dispensare consigli confidenziali. 
 
Linea 4 - Yin (6): Chiudere un sacco di grano. Nessuna sfortuna. Nessuna gloria.
Commento: «Chiudere un sacco di grano; nessuna sfortuna», con la prudenza non ci sarà danno.
Linea 4 - Un sacco chiuso 
Rimanete in silenzio. Siate prudenti nell'assumervi nuove responsabilità. Le vostre parole e le vostre azioni saranno esposte a critiche. Traete profitto da ciò di cui disponete. È ormai cosa fatta.
Linea 5 - Yin (6): Una veste inferiore gialla. Totalmente fausto.
Commento: «Una veste inferiore gialla: totalmente fausto», perché la linea è centrale [quinta linea].
Linea 5 - Veste gialla ricamata 
Potrebbe esservi assegnato un incarico che non vi spetta. È una vera fortuna, se accettate di essere secondi nel comando e se rimanete leali. È pericoloso tentare di prendere tutto.
Linea 6 - Yin (6): I draghi combattono nei campi. Il sangue cade come pioggia.
Commento: «I draghi combattono nei campi»: qui la via finisce.
Linea 6 - I draghi combattono nella foresta 
La situazione si è spinta fin dove era possibile. È tempo di correggere gli squilibri. Quando l'oscillazione del pendolo ha raggiunto il suo massimo torna indietro. La fine e l'inizio.
Tutte le linee mobili - Yin (6): Oracolo propizio a lungo termine.
Commento: Tutte le linee Yin. «Oracolo propizio a lungo termine»: grande sarà la conclusione.
				 */
			/// </summary>
			public class TheReceptive : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Adesso si calpesta la brina. Il ghiaccio compatto verrà presto.",
							"Rigido e quadrato. Grandi venti soffieranno. [1] Nulla che non sia propizio.",
							"Splendore nascosto. L'oracolo è possibile [che sia favorevole]. Se si mette al servizio del Re, non cercherà rapido successo, ma porterà le cose al loro compimento.",
							"Chiudere un sacco di grano. Nessuna sfortuna. Nessuna gloria.",
							"Una veste inferiore gialla. Totalmente fausto.",
							"I draghi combattono nei campi. Il sangue cade come pioggia.",
							"Oracolo propizio a lungo termine.",
						};
					}
					
				}
				public override int Id => 2;
				public override string Sentency { get; set; } = "Il Ricettivo. Suprema riuscita. Propizio oracolo per una cavalla. Per un signore con un luogo in cui andare: se tenta di fare da guida si disorienta, se segue trova la guida: propizio. Trova amici ad ovest ed a sud, ma li perde ad est ed a nord. Oracolo per chi è in pace: fausto.";

				public override string Comment => "Il Ricettivo. Grande è la sublimità della Terra che dà vita a tutte le cose, seguendo ed accettando le regole del cielo. La Terra è ricca, raccoglie tutte le cose ed è dotata di virtù senza limiti. Abbraccia tutto nella sua ampiezza e tutto illumina nella sua grandezza; così tutti gli esseri pervengono alla riuscita. La «cavalla» appartiene allo stesso genere della Terra: entrambe spaziano sulla terra illimitata. Le linee tutte morbide presagiscono un «propizio oracolo». «Un signore con un luogo in cui andare: se tenta di fare da guida si disorienta», poi con devozione si mette sulla strada giusta. «Trova amici a ovest ed a sud», e con essi, suoi affini, va avanti; «ma li perde a est ed a nord», alla fine arriverà buona fortuna: infatti «chi è in pace» ottiene un responso fausto, perchè i confini della Terra non hanno limiti.";
				public override string Image => "La Terra è l'accogliente dedizione. Ispirandosi a questa immagine, il signore, con la sua sublime virtù, sostiene tutte le cose.";
				public override string Series => "Dopo che il Cielo e la Terra sono venuti all'esistenza, miriadi di esseri sono stati prodotti. Il Creativo e il Ricettivo sono l'origine, la sorgente della della creazione.";
				public override string MixedSign => "Il Ricettivo è cedevole";
				public override string AddictionalSentence => "";
			}
			/// <summary>
			/// 3
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yang (9): Titubanza e impedimento. Oracolo propizio per chi rimane a casa. Propizio designare vassalli.
Commento: Pur con «titubanza e impedimento», la volontà è corretta perchè la nobile linea [dura] scende alla posizione più bassa; ciò conquista l'appoggio della gente.
Linea 1 - Ritardi, rimanete dove siete
Una persona forte in una posizione incerta. Quando saprete ciò che volete, troverete ciò di cui avete bisogno. Chiedete aiuto agli altri.
Linea 2 - Yin (6): Difficoltà si ammassano, è difficile procedere. Cavallo e  carro non vanno avanti. Non alleato coi banditi ma cerca un matrimonio. Oracolo per una donna che non ha concepito: dopo dieci anni concepirà.
Commento: La «difficoltà» presagita dalla seconda linea morbida è dovuta al fatto che essa sta sopra una linea dura. «Concepire dopo dieci anni», vuol dire ritorno alla situazione normale.
Linea 2 - Dopo dieci anni lei acconsente a sposarsi
Resistete agli inviti pressanti di un aspirante socio finché non vi sentite pronti. Non cercate soluzioni immediate. Avete la forza di rifiutare e aspettare. È così che il partner più debole controlla quello più forte.
Linea 3 - Yin (6): Cacciare il cervo senza guardabosco. Perdersi nella foresta. Un signore sa che è meglio rinunciare. Chi continua a inseguire, avrà umiliazione.
Commento: «Cacciare il cervo senza guardabosco», cioè essere troppo bramosi della selvaggina: «un signore vi rinuncia». «Chi continua a inseguire, avrà umiliazione»: ciò conduce alla fine.
Linea 3 - A caccia senza una guida, perso nella foresta
Potreste sentirvi deboli e soli eppure voler proseguire. Senza una guida attraverso la foresta della vita, vi perdereste. 
 
Linea 4 - Yin (6): Cavallo e carro non vanno avanti. Cercare un matrimonio. Per chi va avanti: fausto. Nulla che non sia propizio.
Commento: «Cercare» e «andare avanti»: ciò è chiarezza.
Linea 4 - Cavallo e carrozza in attesa di muoversi
Siete pronti a muovervi. Il prossimo passo è trovare coloro a cui vorreste unirvi e aspettare che vi invitino.
Linea 5 - Yang (9): Si ammassano i propri cibi grassi. [1] Oracolo per le piccole cose: fausto; oracolo per quelle grandi: disastroso.
Commento: «Si ammassano i propri cibi grassi», perché la beneficenza non è ancora splendida.
Linea 5 - Difficoltà nel distribuire la carne grassa
Intralci e ritardi nel ricevere ricompense e benefici. Accontentatevi di piccoli guadagni. Solo le questioni minori troveranno una soluzione positiva. Affrontate un grande problema un pezzo alla volta.
Linea 6 - Yin (6): Cavallo e carro non vanno avanti. Lacrime e sangue sgorgano.
Commento: «Lacrime e sangue sgorgano»: come potrebbe ciò durare a lungo?
Linea 6 - In sella a un cavallo fermo, scorrono lacrime di sangue
Vi trovate nel mezzo di una situazione a cui non eravate preparati. Se avete la volontà di uscirne, supererete le difficoltà.
				 */
			/// </summary>
			public class TheInitialDifficulties : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Titubanza e impedimento. Oracolo propizio per chi rimane a casa. Propizio designare vassalli.",
							"Difficoltà si ammassano, è difficile procedere. Cavallo e  carro non vanno avanti. Non alleato coi banditi ma cerca un matrimonio. Oracolo per una donna che non ha concepito: dopo dieci anni concepirà.",
							"Cacciare il cervo senza guardabosco. Perdersi nella foresta. Un signore sa che è meglio rinunciare. Chi continua a inseguire, avrà umiliazione.",
							"Cavallo e carro non vanno avanti. Cercare un matrimonio. Per chi va avanti: fausto. Nulla che non sia propizio.",
							"Si ammassano i propri cibi grassi. [1] Oracolo per le piccole cose: fausto; oracolo per quelle grandi: disastroso.",
							"In sella a un cavallo fermo, scorrono lacrime di sangue. Vi trovate nel mezzo di una situazione a cui non eravate preparati. Se avete la volontà di uscirne, supererete le difficoltà.",
						};
					}
					
				}
				public override int Id => 3;
				public override string Sentency { get; set; } = "La Difficoltà iniziale. Suprema riuscita. Propizio oracolo. Non agire quando c'è un luogo in cui andare. Propizio designare vassalli.";

				public override string Comment => "La Difficoltà iniziale. Le linee dure e quelle morbide cominciano a incontrarsi [dopo l'omogeneità dei primi due esagrammi] e generano con difficoltà. Il trigramma inferiore è \"mobile\" e quello superiore è \"pericoloso\". Oracolo di «suprema riuscita». Il Tuono e la Pioggia, fanno crescere rigogliosamente le cose ma la creazione del cielo può essere misteriosa e confusa. «Propizio designare vassalli», ma non ci sarà tranquillità.";
				public override string Image => "Nuvole (= Acqua) sopra il Tuono: questa è l'immagine della Difficoltà iniziale. Ispirandosi ad essa, il signore usa regole per mettere ordine.";
				public override string Series => "Dopo che il Cielo e la Terra sono venuti all'esistenza, miriadi di esseri sono stati prodotti. Queste miriadi di esseri riempiono lo spazio tra Cielo e Terra. Per questo segue il segno: la Difficoltà iniziale. Difficoltà iniziale significa colmare.";
				public override string MixedSign => "La Difficoltà iniziale è visibile, ma non ha ancora perso la sua dimora.";
				public override string AddictionalSentence => "";
			}
			/// <summary>
			/// 4
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yin (6): Illuminare il giovane stolto. Propizio ricorrere a disciplina o rimuovere ceppi e catene. Chi va avanti avrà umiliazione.
Commento: «Propizio ricorrere a disciplina o rimuovere ceppi e catene» per far rispettare la legge.
Linea 1 - La nuova pianta si apre il varco nella terra
La conoscenza libera la mente dalla confusione. Cercate di fare subito chiarezza su ciò che non comprendete. L'ignoranza è la volontà di ignorare.
Linea 2 - Yang (9): Essere paziente col giovane stolto. Fausto. Portare a casa una moglie: fausto. Un figlio può sostenere la famiglia.
Commento: «Un figlio può sostenere la famiglia», perché la linea dura [seconda linea] e la linea morbida [quinta linea] corrispondono.
Linea 2 - Mettete su casa e famiglia
In questo momento avete qualcosa da offrire ed è sufficiente per cominciare. Siate tolleranti verso coloro che per la prima volta si assumono nuove responsabilità.
Linea 3 - Yin (6): Non prendere la ragazza come moglie: se vede l'uomo di bronzo, getta via se stessa. [1] Nulla è propizio.
Commento: «Non prendere la ragazza come moglie»: procedere così non è devoto [come deve esserlo una linea morbida].
Linea 3 - Gettarsi via per denaro
Nel vostro entusiasmo e nella vostra ignoranza potreste perdere il controllo e darvi al primo offerente che si presenta. Resistete alle tentazioni di un facile profitto. 
 
Linea 4 - Yin (6): Stolto rinchiuso. Umiliazione.
Commento: «Umiliazione» per lo «stolto rinchiuso», perché è solo e lontano dalla stabilità [che si trova nella seconda e sesta linea].
Linea 4 - Imprigionato nella follia
Avete perso il contatto con la realtà. Nella vostra ignoranza vi siete assunti troppe responsabilità. Se rifiutate con ostinazione qualsiasi consiglio la vita vi darà una lezione.
Linea 5 - Yin (6): Giovane stolto ma genuino. Fausto.
Commento: Il responso è «fausto» per il «giovane stolto ma genuino», perché [la linea morbida] è devota e docile.
Linea 5 - Un giovane germoglio fortunato
Avete la fortuna del principiante. Non nutrite dubbi sulle vostre capacità. Devoti e gentili, cordiali e onesti, sapete accettare un consiglio.
Linea 6 - Yang (9): Punire il giovane stolto. Non propizio per i banditi. Propizio difendersi dai banditi.
Commento: «E' propizio difendersi dai banditi», perchè il superiore e l'inferiore si corrispondono.
Linea 6 - Non prevaricate, prevenite le prevaricazioni
Un bravo maestro trasmette conoscenza e libera la mente dell' allievo. Un cattivo maestro instilla violenza e insegna pregiudizi.
				 */
			/// </summary>
			public class TheYouthFoolishness : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"La nuova pianta si apre il varco nella terra. La conoscenza libera la mente dalla confusione. Cercate di fare subito chiarezza su ciò che non comprendete. L'ignoranza è la volontà di ignorare.",
							"Essere paziente col giovane stolto. Fausto. Portare a casa una moglie: fausto. Un figlio può sostenere la famiglia.",
							"Non prendere la ragazza come moglie: se vede l'uomo di bronzo, getta via se stessa. [1] Nulla è propizio.",
							"Stolto rinchiuso. Umiliazione.",
							"Giovane stolto ma genuino. Fausto.",
							"Non prevaricate, prevenite le prevaricazioni. Un bravo maestro trasmette conoscenza e libera la mente dell' allievo. Un cattivo maestro instilla violenza e insegna pregiudizi.",
						};
					}
					
				}
				public override int Id => 4;
				public override string Sentency { get; set; } = "La Stoltezza giovanile. Riuscita. «Non sono stato io a rivolgermi al giovane stolto. È stato lui a rivolgersi a me». Quando la prima divinazione dà responso, ripetere più volte le divinazioni genera confusione e non dà responso. Propizio oracolo.";

				public override string Comment => "La Stoltezza giovanile: sotto Monte c'è il \"pericoloso\"; \"pericoloso\" e \"quieto\" producono la Stoltezza giovanile. «La Stoltezza giovanile: riuscita»: si può andare avanti verso la riuscita, se si agisce nel tempo giusto. «Non sono stato io a rivolgermi al giovane stolto; è stato lui a rivolgersi a me»: la volontà trova corrispondenza [tra la seconda e la quinta linea]. «La prima divinazione dà responso», a causa della linea dura centrale [seconda linea]. «Ripetere più volte le divinazioni è irriverente e non dà responso»: perchè l'irriverenza mostra la stoltezza giovanile. La stoltezza giovanile può essere corretta con l'educazione. Un saggio ha successo.";
				public override string Image => "Sotto il Monte sgorgano Acque sorgive: questa è l'immagine della Stoltezza giovanile. Ispirandosi ad essa, il signore nutre la sua virtù con azioni corrette.";
				public override string Series => "Quando, dopo le difficoltà iniziali, le cose sono appena venute alla luce, sono sempre ravvolte alla nascita da immaturità. Per questo segue il segno: Stoltezza giovanile. Poiché stoltezza giovanile significa giovanile immaturità. Questo è lo stato delle cose nella loro giovinezza.";
				public override string MixedSign => "Stoltezza giovanile significa confusione e susseguente illuminazione.";
				public override string AddictionalSentence => "";
			}
			/// <summary>
			/// 5
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yang (9): Aspettare in periferia. [1] Propizio dare prova di perseveranza. Nessuna sfortuna.
Commento: «Aspettare in periferia», senza fretta o agitazione. «Propizio dare prova di perseveranza», cioè non allontanarsi dalla norma.
Linea 1 - Sotto la pioggia
Avete la forza e la capacità di affrontare una situazione pericolosa. Ma non è ancora tempo di muovervi, poiché il pericolo è ancora lontano. Siate pazienti. Per ora rimanete al riparo dalla pioggia.
Linea 2 - Yang (9): Aspettare sulla sabbia. [1] Ci saranno piccole lagnanze. Alla fine fausto.
Commento: «Aspettare sulla sabbia»: si rimane pacati poiché la linea è centrale. Malgrado le «piccole lagnanze», «alla fine fausto».
Linea 2 - Fradicio sulla sponda del fiume
Vicini al pericolo, ma non in pericolo. Come trovarsi sulla sponda di un fiume e a un tratto essere investiti da una grande onda. Come essere criticati e non reagire. Tenetevi saldi, l'onda passerà.
Linea 3 - Yang (9): Aspettare nel fango. [1] Provocherà l'arrivo del bandito.
Commento: «Aspettare nel fango»:  i guai si nascondono nel trigramma esterno. «Provocherà l'arrivo del bandito», ma un comportamento attento e prudente può scongiurare il disastro.
Linea 3 - Imprigionato nel fango
Nell'impazienza di attraversare il fiume, vi siete mossi prima di quanto avreste dovuto e ora siete a metà strada, imprigionato nel fango. I predatori sono in agguato vicino all'acqua, in attesa di un momento come questo. 
 
Linea 4 - Yin (6): Aspettare nel sangue. [1] Uscirà dalla tana.
Commento: «Aspettare nel sangue»: perché è [una linea morbida] devota e obbediente.
Linea 4 - Sanguinanti, strappati dalla vostra tana
Potreste essere feriti e starvene nascosti. L'invito a uscire allo scoperto porrà fine al conflitto. Non vi accadrà nulla di male. È possibile che dobbiate porre fine all'attesa e prendere una decisione.
Linea 5 - Yang (9): Aspettare bevendo e mangiando. [1] Oracolo fausto.
Commento: «Aspettare bevendo e mangiando; oracolo fausto», perché [la linea] è centrale e corretta.
Linea 5 - Servito di cibo e vino
Dopo aver felicemente attraversato il fiume, potete riposare e divertirvi. Forse il pericolo non è ancora del tutto passato, ma voi avete fatto tutto ciò che è in vostro potere per assicurare un esito positivo.
Linea 6 - Yin (6): Si rientra nella tana. Arriveranno tre ospiti non invitati. Bisogna trattarli con riguardo. Alla fine fausto. 
Commento: «Arriveranno degli ospiti non invitati. Bisogna trattarli con riguardo; alla fine fausto»: benché la linea non sia adatta [alla posizione], non ci saranno grandi perdite.
Linea 6 - Onorare tre ospiti non invitati
Date il benvenuto a un'influenza inaspettata nella vostra vita. Potreste anche non saperlo, ma avete bisogno di un cambiamento e questo lo è. Accettate ciò che vi viene incontro.
				 */
			/// </summary>
			public class TheWaiting : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Aspettare in periferia. [1] Propizio dare prova di perseveranza. Nessuna sfortuna.",
							"Aspettare sulla sabbia. [1] Ci saranno piccole lagnanze. Alla fine fausto.",
							"Aspettare nel fango. [1] Provocherà l'arrivo del bandito.",
							"Aspettare nel sangue. [1] Uscirà dalla tana.",
							"Servito di cibo e vino. Dopo aver felicemente attraversato il fiume, potete riposare e divertirvi. Forse il pericolo non è ancora del tutto passato, ma voi avete fatto tutto ciò che è in vostro potere per assicurare un esito positivo.",
							"Si rientra nella tana. Arriveranno tre ospiti non invitati. Bisogna trattarli con riguardo. Alla fine fausto.",
						};
					}
					
				}
				public override int Id => 5;
				public override string Sentency { get; set; } = "L'Attesa. [1] Essere sinceri. Suprema riuscita. Fausto oracolo. Propizio attraversare il grande fiume.";

				public override string Comment => "L'Attesa significa: trattenersi. Davanti c'è il \"pericoloso\" ma le linee dure del \"forte\" prevengono ogni caduta nel pericolo. Questo significa che non si finisce nella debolezza o nella confusione. «Essere sinceri: suprema riuscita: fausto oracolo»: la posizione della linea celeste [quinta linea Yang] è centrale e corretta. «Propizio attraversare il grande fiume»: procedere porterà buoni risultati.";
				public override string Image => "Le Nuvole (= Acqua) stanno sopra il Cielo: questa è l'immagine dell'Attesa. Ispirandosi ad essa, il signore mangia e beve, festeggiando con gioia.";
				public override string Series => "Quando le cose sono ancora piccole non bisogna lasciarle senza alimento. Per questo segue il segno: l'Attesa. L'Attesa significa la via per mangiare e bere.";
				public override string MixedSign => "Attendere significa non procedere.";
				public override string AddictionalSentence => "";
			}
			/// <summary>
			/// 6
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yin (6): Non persistere nella causa. Ci saranno piccole lagnanze. Alla fine fausto.
Commento: «Non persistere nella causa»: la controversia non può andare avanti a lungo. Malgrado «le piccole lagnanze», il giudizio sarà chiaro.
Linea 1 - Non imponete le vostre esigenze
Un malinteso può essere evitato con una ritirata. La vostra posizione non è abbastanza salda perché possiate imporre le vostre esigenze. Potreste essere criticati, ma ogni cosa andrà a buon fine.
Linea 2 - Yang (9): Persa la causa, ritornare e scappare. La sua città è di trecento casate. Nessuna catastrofe.
Commento: «Persa la causa, ritornare e scappare», fuggendo: in una posizione inferiore, contendere col superiore non fa che causare danni.
Linea 2 - Ritiratevi nella vostra città 
Non avete possibilità in questo conflitto. L'avversario è nettamente più forte. Retrocedete finché siete ancora in tempo. Ritiratevi in un posto in cui vi sentite al sicuro.
Linea 3 - Yin (6): Nutrirsi di virtù antica. Oracolo di pericolo. Alla fine fausto. Se ci si mette al servizio del Re, non si avrà alcun successo. [Oracolo per un signore: fausto.] [1]
Commento: «Nutrirsi di virtù antica», in quanto supporta la linea superiore, è fausto.
Linea 3 - Vivete della vostra eredità
Vi trovate in una posizione incerta e vi sentite minacciati. Mantenete la vostra indipendenza e vivete entro le vostre possibilità. Affidatevi alle strategie che già per voi si sono rivelate efficaci.
Linea 4 - Yang (9): Incapace di vincere la causa. Ritornare e accettare la sentenza. Cambiare la propria opinione. Oracolo per chi è in pace: fausto.
Commento: «Ritornare e accettare la sentenza; cambiare la propria opinione; oracolo per chi è in pace», così non ci sarà fallimento.
Linea 4 - Lamentela respinta 
Un impulso vi ha portato a esprimere una lamentela che è stata respinta. Dimenticatevene e guardate avanti. Accettate il fatto che non sempre le cose vanno come volete voi.
Linea 5 - Yang (9): Intraprendere la causa. Totalmente fausto.
Commento: «Intraprendere la causa: totalmente fausto», perché [questa linea] è centrale e corretta.
Linea 5 - Appianate le dispute, suprema buona sorte
Le vostre lagnanze sono giustificate. Ora avete l'opportunità di sistemare i disaccordi in maniera giusta e leale, cosicché la vera amicizia possa crescere. 
Linea 6 - Yang (9): Forse viene conferita una cintura di cuoio. Ma, alla fine del mattino, tre volte viene ripresa.
Commento: Non è degno di rispetto chi è insignito di un'onorificenza perché ha vinto una causa.
Linea 6 - Assegnata una cintura di cuoio, ci viene strappata tre volte quel giorno
Esigendo soddisfazione in una disputa o aumentando il vostro vantaggio con metodi sleali, potreste vincere. Ma i profitti ottenuti con la forza ben presto si dissolvono.
				 */
			/// </summary>
			public class TheContention : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Non persistere nella causa. Ci saranno piccole lagnanze. Alla fine fausto.",
							"Persa la causa, ritornare e scappare. La sua città è di trecento casate. Nessuna catastrofe.",
							"Nutrirsi di virtù antica. Oracolo di pericolo. Alla fine fausto. Se ci si mette al servizio del Re, non si avrà alcun successo. [Oracolo per un signore: fausto.] [1]",
							"Incapace di vincere la causa. Ritornare e accettare la sentenza. Cambiare la propria opinione. Oracolo per chi è in pace: fausto.",
							"Intraprendere la causa. Totalmente fausto.",
							"Forse viene conferita una cintura di cuoio. Ma, alla fine del mattino, tre volte viene ripresa.",
						};
					}
					
				}
				public override int Id => 6;
				public override string Sentency { get; set; } = "La Lite. Essere sinceri. La verità è bloccata. A metà strada fausto. Alla fine disastroso. Propizio vedere un grande uomo. Non è propizio attraversare il grande fiume.";

				public override string Comment => "La Lite. Il trigramma superiore è \"forte\", quello inferiore è \"pericoloso\"; \"pericoloso\" e \"forte\" producono la Lite. «La Lite. Essere sinceri. La verità è bloccata; fausto nel mezzo» dell'impedimento e del pericolo perché è sopraggiunta una linea dura a occupare la posizione centrale [seconda linea]. «Alla fine disastroso»: ciò presagisce l'insuccesso della lite. «Propizio vedere un grande uomo», perché si onora ciò che è centrale e corretto. «Non è propizio attraversare il grande fiume», perché c'è il pericolo di cadere nell'abisso.";
				public override string Image => "Il Cielo e l'Acqua vanno verso direzioni opposte: questa è l'immagine della Lite. Ispirandosi ad essa, il signore riflette a lungo prima di iniziare qualsiasi attività.";
				public override string Series => "Cibi e bevande sono certamente motivo di lite. Perciò segue il segno: la Lite.";
				public override string MixedSign => "Lite significa non amare.";
				public override string AddictionalSentence => "";
			}
			/// <summary>
			/// 7
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yin (6): Un esercito in spedizione si deve muovere con disciplina. Non buono. Disastroso.
Commento: «Un esercito in spedizione si deve muovere con disciplina»: senza la disciplina ci sarà disastro.
Linea 1 - Un esercito deve mettersi in marcia in buon ordine
All'inizio di ogni impresa è saggio avere un piano. Riordinate le vostre risorse, riflettete, e mettete in pratica la disciplina. Fatevi carico del vostro destino.
Linea 2 - Yang (9): In mezzo all'esercito. Fausto. Nessuna sfortuna. Il Re conferirà tre onorificenze.
Commento: «In mezzo all'esercito; fausto», perché si gode del favore del cielo. «Il Re conferirà tre onorificenze», pensando al benessere delle miriadi di stati.
Linea 2 - Una triplice onorificenza
Venite premiati per la vostra dedizione e per i servizi resi. Vi sono concessi tre desideri. Un comandante responsabile all'altezza delle esigenze di una missione importante.
Linea 3 - Yin (6): L'esercito  forse trasporta cadaveri sui carri. Disastroso.
Commento: «L'esercito forse trasporta cadaveri sui carri», vuol dire: grande sconfitta.
Linea 3 - Cadaveri nella carrozza
Disordine e fallimento dovuti a una leadership incompetente e conflittuale, investita di un'autorità scarsamente rappresentativa. Trovate un capo competente in grado di ristabilire l'ordine. Siete voi questa persona, oppure preferireste abbandonare il campo?
Linea 4 - Yin (6): L'esercito si schiera a sinistra (= si ritira) per la terza volta. Nessuna sfortuna.
Commento: «Schierarsi a sinistra per la terza volta: nessuna sfortuna», perché le regole non vengono trasgredite.
Linea 4 - Accampatevi lontani dal nemico
Non è il momento giusto per avanzare. Per evitare i guai è necessaria una ritirata strategica. Vengono effettuati aggiustamenti e deviazioni per preservare il piano generale.
Linea 5 - Yin (6): Il cacciatore torna con la preda. Propizio tornare coi prigionieri. Nessuna sfortuna. Il primogenito è al comando dell'esercito. Il secondo figlio sta trasportando cadaveri sul carro. Oracolo di disastro.
Commento: «Il primogenito è al comando dell'esercito»: si muove in avanti perché questa è una linea centrale. «Il secondo figlio sta trasportando cadaveri sul carro», perché la linea non è adatta alla posizione.
Linea 5 - Catturate la selvaggina
In questo momento avete l'opportunità di agire in maniera decisiva. Affidate a ciascuno la missione che meglio può svolgere. Siate sempre a conoscenza di ciò che accade. Fermatevi quando avrete raggiunto un obiettivo ragionevole.
Linea 6 - Yin (6): Il grande signore dà ordini, fonda stati e infeuda famiglie. Non usare le persone mediocri.
Commento: «Il grande signore dà ordini» ottenendo risultati, perché la linea è correttamente posizionata. «Non usare le persone mediocri», perché i tumulti nel Paese sarebbero inevitabili.
Linea 6 - Stabilite i compensi
Missione compiuta. Ricompensate coloro che hanno agito bene. Assumete soltanto i più competenti, in modo che i profitti rimangano e non vadano persi.
				 */
			/// </summary>
			public class TheArmy : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Un esercito in spedizione si deve muovere con disciplina. Non buono. Disastroso.",
							"In mezzo all'esercito. Fausto. Nessuna sfortuna. Il Re conferirà tre onorificenze.",
							"L'esercito  forse trasporta cadaveri sui carri. Disastroso.",
							"L'esercito si schiera a sinistra (= si ritira) per la terza volta. Nessuna sfortuna.",
							"Il cacciatore torna con la preda. Propizio tornare coi prigionieri. Nessuna sfortuna. Il primogenito è al comando dell'esercito. Il secondo figlio sta trasportando cadaveri sul carro. Oracolo di disastro.",
							"Il grande signore dà ordini, fonda stati e infeuda famiglie. Non usare le persone mediocri.",
						};
					}
					
				}
				public override int Id => 7;
				public override string Sentency { get; set; } = "L'Esercito. Oracolo fausto per un grande uomo. Nessuna sfortuna.";

				public override string Comment => "L'Esercito è simbolo di una grande massa di gente. «Oracolo» significa anche correttezza. Le masse possono essere corrette, forse da un Re. La linea dura [seconda linea] è centrale e trova corrispondente appoggio [con la quinta linea e con tutte le altre]. \"Pericoloso\" e \"devoto\". Chi guida il mondo in questo modo sottomette il popolo all'ubbidienza. Essendo un segno «fausto», come potrebbe esserci sfortuna?";
				public override string Image => "L'Acqua dentro alla Terra: questa è l'immagine dell'Esercito. Ispirandosi ad essa, il signore è generoso verso la gente per riunire intorno a sé le masse popolari.";
				public override string Series => "Quando vi è lite le masse di certo si sollevano. Segue per questo il segno: l'Esercito. Esercito significa massa.";
				public override string MixedSign => "L'Esercito è cosa triste.";
				public override string AddictionalSentence => "";
			}
			/// <summary>
			/// 8
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yin (6): Essendo sinceri, unirsi con loro. Nessuna sfortuna. Essere sinceri come una terrina colma. Per coloro che arrivano tardi: inaspettata calamità. [2] Fausto.
Commento: «Unirsi con loro»: la prima linea morbida è un segno fausto, nonostante l'«inaspettata calamità».
Linea 1 - Una semplice tazza, traboccante di sincerità
Quando incontrate gli altri per la prima volta potreste sentirvi solo e insicuro. Con il cuore sincero e puro li attrarrete verso di voi.
Linea 2 - Yin (6): Unirsi con qualcuno dall'interno. Oracolo: fausto.
Commento: «Unirsi con qualcuno dall'interno»: in questo modo non si perde se stessi.
Linea 2 - Amicizia all'interno del gruppo
Appartenete al gruppo, oppure avete un legame con esso. Fiducia e stima di voi stessi.
Linea 3 - Yin (6): Unirsi con le persone sbagliate. [Disastroso] [3].
Commento: «Unirsi con le persone sbagliate»: non è dannoso fare cosi?
Linea 3 - Cattive compagnie
Fate attenzione nel concedere aiuto a persone prive di scrupoli. L'adulazione potrebbe avere annebbiato il vostro giudizio. Siete in cattiva compagnia.
Linea 4 - Yin (6): Unirsi con le forze esterne. Oracolo: fausto.
Commento: «Unirsi con le forze esterne», quelle dotate di saggezza, obbedendo al superiore [quinta linea].
Linea 4 - Amici al di fuori
Dare o ricevere supporto da coloro che sono esterni rispetto al gruppo. Rivolgete la vostra attenzione ai nuovi venuti, agli stranieri, a coloro che non provengono dalla vostra cerchia immediata.
Linea 5 - Yang (9): Unirsi in cerchio (= manifestare l'unione). Il Re fa circondare la preda da tre lati, ma la belva gli sfugge davanti agli occhi: la gente del luogo non si spaventa. Fausto.
Commento: Il responso «fausto» del «manifestare l'unione» è dovuto al fatto che questa linea si trova in una posizione centrale e corretta. Lasciare perdere gli indocili e inseguire i devoti: questo è ciò che si intende con «la belva gli sfugge davanti agli occhi». «La gente del luogo non si spaventa»: utilizza la linea centrale del trigramma superiore.
Linea 5 - Lasciate fuggire parte della selvaggina
Il modo migliore per avere amici è permettere loro di stare con voi per propria scelta. Non è necessaria alcuna coercizione.
Linea 6 - Yin (6): Unirsi con coloro che non hanno un capo. Disastroso.
Commento: «Unirsi con coloro che non hanno un capo»: ciò non approda a nulla.
Linea 6 - Scarsa disponibilità alla cooperazione
Una persona non qualificata è al comando e gli altri nutrono scarsa fiducia. Quando si parte con il piede sbagliato è difficile creare un'atmosfera di fiducia.
 
				 */
			/// </summary>
			public class TheSolidarity : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Essendo sinceri, unirsi con loro. Nessuna sfortuna. Essere sinceri come una terrina colma. Per coloro che arrivano tardi: inaspettata calamità. [2] Fausto.",
							"Unirsi con qualcuno dall'interno. Oracolo: fausto.",
							"Unirsi con le persone sbagliate. [Disastroso] [3].",
							"Unirsi con le forze esterne. Oracolo: fausto.",
							"Unirsi in cerchio (= manifestare l'unione). Il Re fa circondare la preda da tre lati, ma la belva gli sfugge davanti agli occhi: la gente del luogo non si spaventa. Fausto.",
							"Unirsi con coloro che non hanno un capo. Disastroso.",
						};
					}
					
				}
				public override int Id => 8;
				public override string Sentency { get; set; } = "La Solidarietà. Fausto. Alla prima divinazione: suprema [riuscita] [1]. Oracolo a lungo termine: nessuna sfortuna. Verrà gente da paesi tumultuosi; per chi è in ritardo: disastroso.";

				public override string Comment => "La Solidarietà. «Fausto»: solidarietà vuol dire appoggio e il trigramma inferiore è \"devoto\" e subordinato. «Se si ripete la divinazione: suprema [riuscita]. Oracolo a lungo termine: nessuna sfortuna», perché la linea dura [quinta linea] è centrale. «Verrà gente da paesi tumultuosi»: perché, alla linea superiore [quinta linea], l'inferiore [seconda linea e tutte le altre] corrisponde. «Per chi è in ritardo: disastroso», perché il suo cammino è finito.";
				public override string Image => "L'Acqua sulla Terra: questa è l'immagine della Solidarietà. Ispirandosi ad essa, gli antichi Re concessero feudi a miriadi di famiglie nobili e mantennero amichevoli rapporti coi vassalli.";
				public override string Series => "Tra le masse vi è certamente una ragione per associarsi. Per questo segue il segno: la Solidarietà. Essere solidali significa radunarsi.";
				public override string MixedSign => "La Solidarietà è cosa lieta.";
				public override string AddictionalSentence => "";
			}
			/// <summary>
			/// 9
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yang (9): Ritornare percorrendo la stessa strada. Come potrebbe essere una sfortuna? Fausto.
Commento: «Ritornare percorrendo la stessa strada»: il significato è fausto.
Linea 1 - Ritorno sul sentiero giusto
Voltate le spalle a una compagnia poco salutare e ritornate ai bei giorni e ai bei modi di un tempo. Nel vostro cuore sapete ciò che è giusto.
Linea 2 - Yang (9): Bisogna tirarlo indietro. Fausto.
Commento: «Bisogna tirarlo indietro»: [la linea] è centrale; in questa posizione non ci si perde.
Linea 2 - Lasciarsi condurre sul sentiero giusto
Essere rimessi in riga. Siete stati convinti, o siete voi a convincere, a tornare sulla retta via. Due persone possono aiutarsi nel superare un ostacolo comune.
Linea 3 - Yang (9): Dal carro si è staccata una ruota raggiata. Il marito e la moglie litigano.
Commento: «Il marito e la moglie litigano», e non riescono a mettere ordine in casa.
Linea 3 - Le ruote si staccano dal carro, litigano moglie e marito
Litigare e dare la colpa a qualcun altro quando qualcosa accade. Fermatevi, prima che questo comportamento vi prenda la mano. La vostra felicità dipende da questo.
Linea 4 - Yin (6): Essere sinceri. Il sangue scorre via. La preoccupazione esce. Nessuna sfortuna.
Commento: «Essendo sinceri, la preoccupazione esce»: la volontà è identica a quella del superiore [quinta linea].
Linea 4 - Il discorso schietto evita lo spargimento di sangue 
Avete la calma e la sincerità necessarie per allontanare l'ansia e la minaccia di conflitto. In questo momento non conviene un approccio con la forza. 
 
Linea 5 - Yang (9): Essere sinceri conduce a legarsi insieme. Far arricchire anche i vicini.
Commento: «Essere sinceri conduce a legarsi insieme»: non si deve monopolizzare la ricchezza solo per sé stessi.
Linea 5 - Condividere le ricchezze con il vicino
Non usate gli altri e non fatevi usare. I rapporti dovrebbero recare benefici a tutti. La fiducia reciproca è alla base degli impegni vincolanti, specie quando c'è di mezzo il denaro.
Linea 6 - Yang (9): Viene la pioggia, cessa la pioggia. Si può ancora terminare di coltivare. Oracolo per una moglie: pericoloso. La luna è quasi piena, e, se il signore imprende qualcosa: pericoloso.
Commento: «Viene la pioggia, cessa la pioggia. Si può ancora terminare di coltivare. Se il signore imprende qualcosa: pericoloso», perché ci sono molti dubbi da chiarire.
Linea 6 - Arriva la pioggia, se ne va la pioggia
Avete superato la tempesta. Per il momento tutto va bene. Approfittatene per ricostruire la vostra energia e le vostre risorse. I problemi non sono ancora finiti.
				 */
			/// </summary>
			public class TheStrengthOfTheLittleTamer : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Ritornare percorrendo la stessa strada. Come potrebbe essere una sfortuna? Fausto.",
							"Bisogna tirarlo indietro. Fausto.",
							"Dal carro si è staccata una ruota raggiata. Il marito e la moglie litigano.",
							"Essere sinceri. Il sangue scorre via. La preoccupazione esce. Nessuna sfortuna.",
							"Essere sinceri conduce a legarsi insieme. Far arricchire anche i vicini.",
							"Viene la pioggia, cessa la pioggia. Si può ancora terminare di coltivare. Oracolo per una moglie: pericoloso. La luna è quasi piena, e, se il signore imprende qualcosa: pericoloso.",
						};
					}
					
				}
				public override int Id => 9;
				public override string Sentency { get; set; } = "La Forza domatrice del piccolo. Riuscita. Dense nuvole ma nessuna pioggia, dalle nostre contrade occidentali.";

				public override string Comment => "La Forza domatrice del piccolo. Una linea morbida [quarta linea] si trova nella giusta posizione; le linee di sopra e di sotto [terza e quinta linea dura] le corrispondono. Questa è la Forza domatrice del piccolo: \"forte\" e Mite. Poiché le linee forti sono centrali si può realizzare la propria volontà. «Riuscita. Dense nuvole ma nessuna pioggia»: vuol dire che ci sarà progresso. «Stanno provenendo dalle nostre contrade occidentali»: ma non ancora effettivo.";
				public override string Image => "Il Vento soffia al di sopra del Cielo: questa è l'immagine della Forza domatrice del piccolo. Ispirandosi ad essa, il signore raffina l'aspetto esteriore della sua virtù.";
				public override string Series => "Con la solidarietà si riesce sicuramente a domare. Perciò segue: la Forza domatrice del piccolo.";
				public override string MixedSign => "La Forza domatrice del piccolo è esigua.";
				public override string AddictionalSentence => "";
			}
			/// <summary>
			/// 10
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yang (9): Procedere con scarpe di seta. Nell'andare avanti: nessuna sfortuna.
Commento: «Procedere con scarpe di seta»: si vuole procedere da soli.
Linea 1 - Con le scarpe basse 
Se vi trovate in basso è meglio che facciate le cose semplici e sarete al sicuro. Vivete secondo le vostre possibilità.
Linea 2 - Yang (9): Procedere sulla via semplice e piana. Oracolo per un uomo solitario: fausto.
Commento: «Oracolo per un uomo solitario: fausto», perché è in posizione centrale e non si lascia disorientare.
Linea 2 - Procedere per una strada facile
Siate onesti e aperti nei vostri rapporti e le cose seguiranno il vostro corso. State calmi e confondetevi nella folla. Non lasciatevi turbare da pensieri di perdite o guadagni.
Linea 3 - Yin (6): Orbo ma riesce a vedere; zoppo ma riesce a camminare. La tigre ha la coda calpestata e morde l'uomo: disastroso. Un guerriero si mette al servizio di un grande signore.
Commento: «Orbo ma riesce a vedere», anche se non chiaramente. «Zoppo ma riesce a camminare», anche se non al passo con gli altri. Il «disastro» del «mordere l'uomo» è dovuto al fatto che la linea non è adatta alla posizione. «Un guerriero si mette al servizio di un grande signore», perché la volontà è solida.
Linea 3 - Procede sulla coda della tigre, divorato vivo
State forzando la sorte, fate il passo più lungo della gamba. Non è il momento di mettere ogni cosa a repentaglio. Forse credete di poter fare molto di più di quanto non sappiate realmente fare.
Linea 4 - Yang (9): Procedere sulla coda della tigre. Si è pietrificati dalla paura. Alla fine fausto.
Commento: «Si è pietrificati dalla paura; alla fine fausto»: la volontà sarà realizzata.
Linea 4 - Procede sulla coda della tigre, prudente e poi fortunato
Correre un rischio potrebbe farvi paura, ma ce la farete. Ora siete abbastanza forti per rischiare, purché non perdiate di vista i pericoli. 
 
Linea 5 - Yang (9): Procedere con fretta decisa. Oracolo: pericoloso.
Commento: «Procedere con fretta decisa; oracolo: pericoloso», sebbene questa linea occupi una posizione corretta e appropriata [a una linea dura].
Linea 5 - Piedi sensibili
Camminate con attenzione. I piedi sensibili in scarpe sottili non sono adatti a escursioni pesanti. Se vi avventurate, aspettatevi problemi. Siate pazienti e aspettate il vostro momento.
Linea 6 - Yang (9): Osservare il proprio procedere passato. Discernere i presagi. Totalmente fausto.
Commento: «Totalmente fausto» nella [linea] in alto: ci sarà una grande e buona fortuna.
Linea 6 - Guarda il tuo procedere
Se non vi sentite sicuri, riguardate ciò che avete fatto finora per sapere dove siete. Se invece sentite che tutto va bene proseguite con fiducia.

				 */
			/// </summary>
			public class TheProceed : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Procedere con scarpe di seta. Nell'andare avanti: nessuna sfortuna.",
							"Procedere sulla via semplice e piana. Oracolo per un uomo solitario: fausto.",
							"Orbo ma riesce a vedere; zoppo ma riesce a camminare. La tigre ha la coda calpestata e morde l'uomo: disastroso. Un guerriero si mette al servizio di un grande signore.",
							"Procedere sulla coda della tigre. Si è pietrificati dalla paura. Alla fine fausto.",
							"Procedere con fretta decisa. Oracolo: pericoloso.",
							"Osservare il proprio procedere passato. Discernere i presagi. Totalmente fausto.",
						};
					}
					
				}
				public override int Id => 10;
				public override string Sentency { get; set; } = "Il Procedere. Procedere sulla coda della tigre; essa non morde. Riuscita.";

				public override string Comment => "Il Procedere: una linea morbida [terza linea]sovrasta due linee dure [prima e seconda linea]. Il \"gioioso\" ha corrispondenza con il Creativo: questo è il significato della frase «Procedere sulla coda della tigre; essa non morde. Riuscita». La linea dura [quinta linea] è centrale e corretta, nella posizione divina [quinta posizione] del Procedere, priva di umiliazione, luminosa e chiara.";
				public override string Image => "Il Cielo sopra e il Lago sotto: questa è l'immagine del Procedere. Ispirandosi ad essa, il signore distingue il superiore dall'inferiore e consolida la volontà della gente.";
				public override string Series => "Quando gli esseri vengono domati nasce la morale, per questo segue il segno: il Procedere.";
				public override string MixedSign => "Ciò che procede non dimora.";
				public override string AddictionalSentence => "Il segno \"il Procedere\" mostra il fondamento della virtù. E' armonioso e raggiunge la meta. Produce armoniosa condotta.";
			}
			/// <summary>
			/// 11
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yang (9): Sradicare la gramigna e le sue radici, insieme a ciò che è simile. Per un'impresa: fausto.
Commento: «Sradicare la gramigna;  fausto per un'impresa»: la volontà è rivolta al trigramma esterno.
Linea 1 - Legato alle radici
La vostra attenzione è concentrata verso l'esterno. L'energia che emanate ispira i vostri simili a unirsi a voi. Create dei legami e li usate a buon fine.
Linea 2 - Yang (9): Abbracciare le terre incolte, attraversare a guado il fiume [1] , non trascurare ciò che è distante,  la compagnia si dissolve. Si ottiene onore camminando nel centro.
Commento: «Abbracciare le terre incolte», «si ottiene onore camminando nel centro», perché la splendore è grande.
Linea 2 - Aggrappatevi a una zucca vuota, non verrete spazzati via
Avete bisogno di un giubbotto di salvataggio per sopravvivere a un periodo difficile di transizione. Qualcosa ò qualcuno a cui affidarvi. In momenti come questi è importante saper distinguere ciò che è utile da ciò che è dannoso.
Linea 3 - Yang (9): Non c'è pianura senza pendenza, non c'è partenza senza ritorno. Oracolo per una faccenda difficile: nessuna sfortuna. Non dubitare della propria sincerità. Si avrà fortuna in ciò che ancora si possiede.
Commento: «Non c'è partenza senza ritorno»: questo è il confine tra il Cielo [linee dure] e la Terra [linee morbide].
Linea 3 - Non c'è pianura senza collina, non c'è andare senza venire
Niente dura per sempre. Alla discesa segue la salita. Avete la forza per cavalcare i cambiamenti e conservare la mente serena. Il pendolo oscilla sempre in due direzioni.
Linea 4 - Yin (6): Fluttuante, fluttuante. Non ricco a causa dei vicini. Non essere guardinghi ma sinceri.
Commento: «Fluttuante, fluttuante; non ricco»: hanno tutti perso ciò che è solido. «Non essere guardinghi ma sinceri»: i desideri vengono dall'intimo del cuore.
Linea 4 - Non vi vantate del vostro benessere con il vicino
Potreste trovarvi in condizioni di richiedere il supporto di altri. Fatelo con spirito di sincerità e amicizia. La grande ostentazione di benessere genererà soltanto invidia.
 
Linea 5 - Yin (6): Il re Yi dà sua figlia in sposa. Ciò reca felicità. Totalmente fausto.
Commento: «Ciò reca felicità; totalmente fausto»; poiché questa linea è centrale, i desideri si possono realizzare.
Linea 5 - Il padre dà la figlia in sposa
Date con generosità e ricevete con generosità. Delegate ad altri posizioni di responsabilità. Se la domanda riguarda un matrimonio o un'unione di altro genere, avrete successo nei vostri intenti.
Linea 6 - Yin (6): Le mura sono crollate nel fossato. «Non servirsi dell'esercito»: dalla capitale un ordine è proclamato. Oracolo di umiliazione.
Commento: «Le mura sono crollate nel fossato»: gli ordini si sono confusi.
Linea 6 - Le mura del castello crollano nel fossato
Alla fine le mura di cinta crolleranno nel fossato. Ciò che è stato costruito ritornerà allo stato originario. In tempo di pace le difese potrebbero non essere più necessarie. La fine di un'era.
				 */
			/// </summary>
			public class ThePeace : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Sradicare la gramigna e le sue radici, insieme a ciò che è simile. Per un'impresa: fausto.",
							"Abbracciare le terre incolte, attraversare a guado il fiume [1] , non trascurare ciò che è distante,  la compagnia si dissolve. Si ottiene onore camminando nel centro.",
							"Non c'è pianura senza pendenza, non c'è partenza senza ritorno. Oracolo per una faccenda difficile: nessuna sfortuna. Non dubitare della propria sincerità. Si avrà fortuna in ciò che ancora si possiede.",
							"Fluttuante, fluttuante. Non ricco a causa dei vicini. Non essere guardinghi ma sinceri.",
							"Il re Yi dà sua figlia in sposa. Ciò reca felicità. Totalmente fausto.",
							"Le mura sono crollate nel fossato. «Non servirsi dell'esercito»: dalla capitale un ordine è proclamato. Oracolo di umiliazione.",
						};
					}
					
				}
				public override int Id => 11;
				public override string Sentency { get; set; } = "La Pace. Il piccolo se ne va, il grande viene. Fausto. Riuscita.";

				public override string Comment => "La Pace. «Il piccolo se ne va [il trigramma della Terra è uscente], il grande viene [il trigramma del Cielo è entrante]. Fausto. Riuscita». Il Cielo e la Terra interagiscono, creando una condizione favorevole a tutti gli esseri. Chi sta sopra interagisce con chi sta sotto: li unisce la volontà comune. Il trigramma interno è Yang, quello esterno è Yin; dentro c'è il \"forte\" e fuori il \"devoto\"; dentro c'è il signore e fuori la persona mediocre. La via del signore si allunga, quella della persona mediocre scompare.";
				public override string Image => "Il Cielo e la Terra interagiscono: questa è l'immagine della Pace. Ispirandosi ad essa, il signore usa la sue doti per completare  il corso del Cielo e della Terra, per assistere i loro movimenti, e così aiuta la gente.";
				public override string Series => "Quando si procede con la pace, allora regna la tranquillità; per questo segue il segno: la Pace. Pace significa unione, interrelazione.";
				public override string MixedSign => "I segni \"il Ristagno\" e \"la Pace\" sono opposti per loro natura.";
				public override string AddictionalSentence => "";
			}
			/// <summary>
			/// 12
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yin (6): Sradicare la gramigna e le sue radici, insieme a ciò che è simile. Oracolo: fausto. Riuscita.
Commento: «Sradicare la gramigna;  oracolo fausto»: la volontà è di un signore.
Linea 1 - Estirpate le erbacce dalle radici
All'inizio l'ostacolo può ancora essere rimosso, prima che abbia messo radici. Se non avete la forza di sopraffarlo, allontanatevene. Chiedete l'aiuto di amici.
Linea 2 - Yin (6): Abbracciare l'adulazione. [1] Per un uomo meschino: fausto. Ristagno per un grande uomo. Riuscita.
Commento: «Ristagno per un grande uomo: riuscita»: in questo modo non si confonde con le masse.
Linea 2 - Persone umili devono accettare umili offerte
In periodi di difficoltà talvolta siamo costretti ad adulare persone indegne. Separatevi da queste persone non appena vi è possibile.
Linea 3 - Yin (6): Abbracciare la vergogna. [2]
Commento: «Abbracciare la vergogna», perché questa linea non è adatta alla posizione.
Linea 3 - Un'offerta avvolta nelle foglie di palma
Vi trovate in svantaggio e cercate disperatamente di piacere. State fingendo di essere chi non siete? Riuscite a essere semplicemente voi stessi?
Linea 4 - Yang (9): C'è un ordine. Nessuna sfortuna. Come esseri uniti insieme, condividere la felicità.
Commento: «C'è un ordine. Nessuna sfortuna»: la volontà è realizzabile.
Linea 4 - Ritrovate autorità e amici preziosi
Malgrado il vostro nervosismo in una situazione incerta, trovate il supporto che cercavate. Avete grandi potenzialità. Le cose diventano più semplici.
Linea 5 - Yang (9): Il ristagno scema. Per un grande uomo: fausto. «Se ne andrà? Se ne andrà?» Legato ad un frondoso albero di gelso.
Commento: Il responso «fausto» «per un grande uomo» è dovuto alla posizione corretta e appropriata di questa linea.
Linea 5 - Uniti al vostro destino
Non resistendo al vostro destino, vi fondete con esso. Dubbi e timori non hanno più il potere di trattenervi. La fine dei tempi difficili. Rafforzate la vostra posizione.
Linea 6 - Yang (9): Il ristagno si capovolge. Prima ristagno. Poi gioia.
Commento: Il «ristagno» alla fine svanisce. Come potrebbe infatti durare a lungo?
Linea 6 - Il tempo delle vacche magre è finito
Tutti gli ostacoli sono stati abbattuti. I giorni felici sono tornati. Avete conservato la fede nei tempi difficili, prosperate quando le cose vanno per il verso giusto.
				 */
			/// </summary>
			public class TheStagnation : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Sradicare la gramigna e le sue radici, insieme a ciò che è simile. Oracolo: fausto. Riuscita.",
							"Abbracciare l'adulazione. [1] Per un uomo meschino: fausto. Ristagno per un grande uomo. Riuscita.",
							"Abbracciare la vergogna. [2]",
							"C'è un ordine. Nessuna sfortuna. Come esseri uniti insieme, condividere la felicità.",
							"Il ristagno scema. Per un grande uomo: fausto. «Se ne andrà? Se ne andrà?» Legato ad un frondoso albero di gelso.",
							"Il ristagno si capovolge. Prima ristagno. Poi gioia.",
						};
					}
					
				}
				public override int Id => 12;
				public override string Sentency { get; set; } = "Il Ristagno, a causa di persone malvagie. Oracolo non propizio per il signore. Il grande se ne va, il piccolo viene.";

				public override string Comment => "Il Ristagno. «A causa di persone malvagie; oracolo non propizio per il signore. Il grande se ne va [il trigramma del Cielo è uscente], il piccolo viene [il trigramma della Terra è entrante]». Il Cielo e la Terra non interagiscono e tutti gli esseri non comunicano fra di loro. Quando il superiore non interagisce con l'inferiore, nel mondo le potenze statali vanno in rovina. Il trigramma interno è Yin, quello esterno è Yang,  dentro c'è il \"devoto\" e fuori il \"forte\",  dentro c'è la persona mediocre e fuori il signore. La via della persona mediocre si allunga, e quella del signore scompare.";
				public override string Image => "Il Cielo e la Terra non interagiscono: questa è l'immagine del Ristagno. Ispirandosi ad essa, il signore si ritira nel suo valore interiore per scongiurare il pericolo e non cerca l'onore in cambio dei benefici.";
				public override string Series => "Le cose non possono essere in unione durevole; per questo segue il segno: il Ristagno.";
				public override string MixedSign => "I segni \"il Ristagno\" e \"la Pace\" sono opposti per loro natura.";
				public override string AddictionalSentence => "";
			}
			/// <summary>
			/// 13
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yang (9): Gli amici si riuniscono al cancello. Nessuna sfortuna.
Commento: Si esce dal «cancello» per «unirsi agli amici»: come potrebbe esserci sfortuna?
Linea 1 - Incontrare gli amici oltre il cancello
Riunitevi con altri in campo aperto. Aprite le porte e il cuore a tutti senza distinzioni. Non è necessaria la riservatezza. Quando incontrate qualcuno per la prima volta, guardate i suoi lati migliori, senza pregiudizi.
Linea 2 - Yin (6): Gli amici si riuniscono nel tempio del proprio clan. Umiliazione.
Commento: «Gli amici si riuniscono nel tempio del proprio clan»: è la via dell'umiliazione.
Linea 2 - Problemi con gli amici nel tempio
Potreste legarvi al primo che capita, o solo a coloro che vi adulano. Liti in famiglia, lotte interne e favoritismi. Scegliete gli amici con cautela e state lontani dai guai
Linea 3 - Yang (9): Nascondere le truppe nel bosco. Occupare le alte colline: per tre anni non ci sarà sviluppo.
Commento: «Nascondere le truppe nel bosco», perché il nemico è forte. «Per tre anni non ci sarà sviluppo»: si potrebbe forse realizzare qualcosa?
Linea 3 - In cerca di amici con le armi nascoste
Alcuni cercano amicizia celando intenzioni aggressive. Gli amici dovrebbero aggregarsi di propria volontà. Se non fate favoritismi tutti si fideranno di voi.
Linea 4 - Yang (9): Salire sul muro fortificato; non si può attaccare. Fausto.
Commento: «Salire sul muro fortificato», significa che, in base alle regole morali, non si conquista. «Fausto»: nelle difficoltà, ritornare alla norma.
Linea 4 - Salire sulle mura e non assalire
Salite in alto. Fermatevi in una posizione sicura. Se fate questo non saranno necessarie forza e aggressione.
Linea 5 - Yang (9): Gli amici. Prima essi piangono poi ridono, perché il grosso delle armate si è congiunto a loro.
Commento: «Gli amici prima», perché cosi vuole la linea centrale e continua. «Il grosso delle armate si è congiunto»: ciò vuol dire che è stata riportata la vittoria.
Linea 5 - Lacrime e poi sorrisi
Ciò che temete probabilmente accadrà. Sopravviverete al pericolo e vi troverete di nuovo con i vostri amici.
Linea 6 - Yang (9): Gli amici si riuniscono in periferia. Nessun rimpianto.
Commento: «Gli amici si riuniscono in periferia», ciò significa che la volontà non è stata ancora realizzata.
Linea 6 - Con gli altri fuori dalle mura
Vi trovate in un territorio sconosciuto, circondati da persone nuove. Siate aperti e cordiali. La fine di una cosa e l'inizio di un'altra.
				 */
			/// </summary>
			public class TheLookForHarmony : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Gli amici si riuniscono al cancello. Nessuna sfortuna.",
							"Gli amici si riuniscono nel tempio del proprio clan. Umiliazione.",
							"Nascondere le truppe nel bosco. Occupare le alte colline: per tre anni non ci sarà sviluppo.",
							"Salire sul muro fortificato; non si può attaccare. Fausto.",
							"Gli amici. Prima essi piangono poi ridono, perché il grosso delle armate si è congiunto a loro.",
							"Gli amici si riuniscono in periferia. Nessun rimpianto.",
						};
					}
					
				}
				public override int Id => 13;
				public override string Sentency { get; set; } = "L'Amicizia tra uomini. Gli amici si riuniscono nel campo. Riuscita. Propizio attraversare il grande fiume. Propizio oracolo per il signore.";

				public override string Comment => "L'Amicizia tra uomini: la linea morbida si trova nella sua posizione centrale [seconda linea] e ha corrispondenza con [la linea dura nel] Cielo. Questa è l'Amicizia tra uomini, il cui testo dice: «I compagni si riuniscono nel campo. Riuscita. Propizio attraversare il grande fiume». Il Cielo è in azione. La figura è \"luminoso\" e \"forte\", corretta e corrispondente [nella seconda e nella quinta linea]. Un «signore» corretto. Solo un tale signore è in grado di unire la volontà di quanti vivono sotto il cielo.";
				public override string Image => "Il Cielo e il Fuoco: questa è l'immagine dell'Amicizia tra uomini. Ispirandosi ad essa, il signore organizza i clan e distingue gli esseri.";
				public override string Series => "Le cose non possono rimanere sempre in ristagno. Per questo segue il segno: l'Amicizia tra uomini.";
				public override string MixedSign => "L'Amicizia tra uomini avvicina gli esseri.";
				public override string AddictionalSentence => "";
			}
			/// <summary>
			/// 14
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yang (9): Nessun rapporto. Svantaggio. [1] Non sfortuna. Nelle difficoltà, nessuna sfortuna.
Commento: La prima linea dura del Grande possesso non ha «nessun rapporto»; questo è uno «svantaggio».
Linea 1 - Se non nuocerete ad altri, gli altri non vi nuoceranno. 
Non date agli altri motivo di risentirsi per la vostra buona sorte. La gentilezza da parte vostra suscita gentilezza nei vostri confronti. State lontani dai guai e i guai si terranno lontani da voi. Proteggetevi senza essere aggressivi.
Linea 2 - Yang (9): Un grande carro per il trasporto. C'è un luogo in cui andare. Nessuna sfortuna.
Commento: «Un grande carro per il trasporto»: [la linea è dura], caricata e centrale; le cose non si perdono.
Linea 2 - Illesi su una grande carrozza
Siete sufficientemente equilibrati e saggi per mettere a frutto il vostro talento. Lungo il percorso qualcuno vi aiuta: state viaggiando con stile.
 
Linea 3 - Yang (9): I nobili sono al banchetto del Figlio del Cielo. I meschini non vi hanno accesso.
Commento: «I nobili sono al banchetto del Figlio del Cielo»: i meschini fanno solo del male a se stessi.
Linea 3 - Solo una grande persona può fare una simile offerta
Condividete con altri la vostra fortuna. Solo una persona generosa può compiere un gesto simile. Sfruttare la fortuna per un guadagno personale non funzionerà sul lungo periodo.
 
Linea 4 - Yang (9): Non essere arrogante. Nessuna sfortuna.
Commento: «Non essere arrogante. Nessuna sfortuna»: è una chiara saggia decisione.
Linea 4 - Non vantarsi tiene lontano il male
Potreste avere fortuna e non sentire il bisogno di vantarvene. Così eviterete il risentimento altrui. Non è necessario che fingiate. Siate voi stessi.
Linea 5 - Yin (6): La sincerità suscita comunicazione e rispetto. Fausto.
Commento: «La sincerità suscita comunicazione», e la sincerità aiuta la realizzazione della volontà. Il responso «fausto» derivato dal «rispetto» risulta facile e non richiede alcuna preparazione.
Linea 5 - Fiducia reciproca, buona sorte
La forza del comando è incoraggiare altri a dare spontaneamente il meglio di sé. La vostra sincerità ispirerà altri a essere aperti e onesti. La vostra più grande risorsa è l'autodisciplina.
 
Linea 6 - Yang (9): Dal cielo viene la protezione. Fausto. Nulla che non sia propizio.
Commento: Il Grande possesso è «fausto» nella linea in alto, perché «dal cielo viene la protezione».
Linea 6 - Con l'aiuto del cielo fate qualsiasi cosa
I benefici che ricevete, il talento, la fortuna sono doni dell'universo. Siatene grati e fatene buon uso. Alla fine le vostre imprese saranno premiate dalla sorte.
 
				 */
			/// </summary>
			public class TheGreatHarvest : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Nessun rapporto. Svantaggio. [1] Non sfortuna. Nelle difficoltà, nessuna sfortuna.",
							"Un grande carro per il trasporto. C'è un luogo in cui andare. Nessuna sfortuna.",
							"I nobili sono al banchetto del Figlio del Cielo. I meschini non vi hanno accesso.",
							"Non essere arrogante. Nessuna sfortuna.",
							"La sincerità suscita comunicazione e rispetto. Fausto.",
							"Dal cielo viene la protezione. Fausto. Nulla che non sia propizio.",
						};
					}
					
				}
				public override int Id => 14;
				public override string Sentency { get; set; } = "Il Grande possesso. Suprema riuscita.";

				public override string Comment => "Il Grande possesso: la linea morbida ottiene una posizione di rispetto esattamente nel grande centro [quinta posizione], le altre linee le corrispondono sopra e sotto [seconda linea e tutte le altre]. Questo è il Grande possesso. Il carattere [del trigramma inferiore] è \"forte\" e la figura [del trigramma superiore] \"luminoso\", trovando corrispondenza nel cielo e muovendosi in armonia col tempo; per questo è scritto: «suprema riuscita».";
				public override string Image => "Il Fuoco è sopra il Cielo: questa è l'immagine del Grande possesso. Ispirandosi ad essa, il signore impedisce il male e sviluppa il bene, obbedendo così alla benigna volontà del cielo.";
				public override string Series => "Per associazione tra uomini le cose sicuramente ci appartengono. Per questo segue il segno: il Grande possesso.";
				public override string MixedSign => "Il Grande possesso indica la moltitudine.";
				public override string AddictionalSentence => "";
			}
			/// <summary>
			/// 15
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yin (6): Modesto, modesto. Il signore riesce ad attraversare il grande fiume. Fausto.
Commento: «Il signore» «modesto nella sua modestia» è umile grazie al controllo di se stesso.
Linea 1 - Modesto con modestia, attraversate il grande fiume
La vostra sicurezza priva di arroganza vi porta avanti con successo. Le imprese difficili hanno buon esito quando fate semplicemente ciò che va fatto.
Linea 2 - Yin (6): Modestia che si estrinseca. Oracolo fausto.
Commento: «Modestia che si estrinseca; oracolo fausto», perché questa linea è centrale e raggiunge il cuore.
Linea 2 - L'espressione della serenità
Avete reputazione di modestia. Essere competente senza pretese è parte di voi, è quello che siete, ciò per cui la gente vi conosce. È la naturale espressione di voi stessi.
Linea 3 - Yang (9): Modestia diligente. Per un signore, alla fine fausto.
Commento: «Un signore» «diligente e modesto» è rispettato da tutti.
Linea 3 - Modesto malgrado i risultati
Fate tranquillamente ciò che dovete fare portandolo a compimento. I risultati non modificano il vostro atteggiamento umile. Il successo non vi dà alla testa. State facendo il vostro dovere.
Linea 4 - Yin (6): Nulla che non sia propizio.  Modestia coraggiosa.
Commento: «Nulla che non sia propizio;  modestia coraggiosa», perché non viola la regola.
Linea 4 - Propizio è agire
La modestia non può più essere una scusa per l'inattività. Fate ciò che deve essere fatto senza perdere la vostra umiltà.
Linea 5 - Yin (6): Non ricco, a causa dei vicini. Propizio intervenire con forza. Nulla che non sia propizio.
Commento: «Propizio intervenire con forza» contro quelli che non si sottomettono.
Linea 5 - Sventura da un vicino, meglio attaccare per primi
È necessario che vi proteggiate dalla sventura. È il vostro momento per agire. Se la vostra è una giusta causa e non vi vanterete del successo, riuscirete nei vostri intenti.
 
Linea 6 - Yin (6): Modestia che si estrinseca. Propizio mandare l'esercito a punire la capitale.
Commento: «Estrinsecando la modestia» non si riesce ancora a realizzare la volontà; ma «ricorrendo all'esercito per punire la capitale» è possibile.
Linea 6 - Fate marciare l'esercito, espugnate le città
Sfruttate la vostra reputazione di capo disinteressato per attirare gli altri verso la vostra causa. Anche per coloro che amano stare sul fondo della scena giunge il momento di agire con decisione
 
				 */
			/// </summary>
			public class TheModesty : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Modesto, modesto. Il signore riesce ad attraversare il grande fiume. Fausto.",
							"Modestia che si estrinseca. Oracolo fausto.",
							"Modestia diligente. Per un signore, alla fine fausto.",
							"Nulla che non sia propizio.  Modestia coraggiosa.",
							"Non ricco, a causa dei vicini. Propizio intervenire con forza. Nulla che non sia propizio.",
							"Modestia che si estrinseca. Propizio mandare l'esercito a punire la capitale.",
						};
					}
					
				}
				public override int Id => 15;
				public override string Sentency { get; set; } = "La Modestia. Riuscita. Il signore riesce a portare a termine le cose.";

				public override string Comment => "La Modestia: riuscita. La via del cielo manda giù il favore e illumina tutto; la via della terra, umilmente, si rivolge all'insù. La legge del cielo è: rendere vuoto ciò che è colmo e accrescere ciò che è modesto. La legge della terra è: alterare ciò che è colmo e riempire ciò che è modesto. Spiriti e dei danneggiano ciò che è colmo e benedicono ciò che è modesto; la legge dell'uomo è: disprezzare ciò che è colmo e amare ciò che è modesto. Nelle posizioni elevate la modestia risplende. Nelle posizioni basse la modestia non può essere scavalcata. Questo è il «portare a termine le cose» del signore.";
				public override string Image => "Il Monte dentro la Terra: questa è l'immagine della Modestia. Ispirandosi ad essa, il signore indebolisce ciò che è eccessivo e rafforza ciò che è insufficiente, bilancia le cose e le rende uguali.";
				public override string Series => "Chi possiede cose grandi non deve riempirle troppo: per questo segue il segno: la Modestia.";
				public override string MixedSign => "Per la Modestia tutto è agevole.";
				public override string AddictionalSentence => "La modestia indica l'impugnatura della virtù. La modestia onora ed è luminosa. La modestia serve per mettere ordine nei costumi.";
			}
			/// <summary>
			/// 16
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yin (6): Gridare il fervore. Disastroso.
Commento: La prima linea morbida dice che «gridare il fervore» porta la volontà ad esaurirsi e conduce al «disastro».
Linea 1 - Vi vantate in pubblico, sventura
Vi aspettate di più di ciò che la situazione può dare. Forse fingete di essere più felici di quanto non siate, vi vantate di risultati non ancora consolidati. Rilassatevi, alleggeritevi.
Linea 2 - Yin (6): Incatenato ad una roccia. Ciò non durerà fino al finire del giorno. Oracolo fausto.
Commento: «Ciò non durerà fino al finire del giorno» e «l'oracolo» è  «fausto», perché la linea è centrale e corretta.
Linea 2 - Saldi come la roccia, è giunto il vostro momento
La vostra fermezza e costanza pagheranno. Se non vacillerete nei vostri principi la fortuna sarà a vostro favore. Fate ciò che deve essere fatto. È giunto il vostro momento.
Linea 3 - Yin (6): Guardare in alto crogiolandosi nel fervore. Rimpianto. Per chi tarda: ci sarà rimpianto.
Commento: «Guardare in alto crogiolandosi nel fervore» conduce al «rimpianto», perché la posizione [della linea] non è appropriata.
Linea 3 - Cercare altrove incoraggiamento
È meglio che vi affidiate ai vostri stessi sforzi e non vi rivolgiate ad altri perché provvedano a voi. Soltanto voi potete cambiare le cose. Nessun altro lo farà per voi.
Linea 4 - Yang (9): Accorto fervore. Otterrai molte cose. Non dubitare. Gli amici si riuniscono attorno a te come attorno ad una fibbia i capelli.
Commento: «Accorto fervore: otterrai molte cose», e la tua volontà sarà completamente realizzata.
Linea 4 - La fonte d'ispirazione
Siete una fonte d'ispirazione per altre persone, una luce che guida. Sapete come incoraggiare gli altri a seguire la vostra direzione.
 
Linea 5 - Yin (6): Oracolo di malattia. Durerà a lungo, ma non porterà alla morte.
Commento: La quinta linea morbida è «oracolo di malattia», perché sovrasta una linea dura. La malattia «durerà a lungo, ma non porterà alla morte», perché questa linea si trova nella posizione centrale e non indica alcun segno di morte.
Linea 5 - Malattia cronica, ma non morte
La pigrizia e la mancanza di motivazione non si risolvono lasciando che altri facciano quello che spetterebbe a voi di fare. Ciò vi renderà soltanto più deboli e darà ad altri il potere su di voi.
Linea 6 - Yin (6): Oscuro fervore. Un cambiamento porterà a una buona fine. Nessuna sfortuna.
Commento: «Oscuro fervore» è la linea in alto: come potrebbe ciò durare a lungo?
Linea 6 - Entusiasmo malriposto, cambiate
Siete rimasti delusi dal vostro stesso entusiasmo. Intorno a voi c'è il buio. Se cambiate i vostri modi non subirete danni. Distoglietevi dai vostri sforzi confusi e sprecati
 
				 */
			/// </summary>
			public class TheFervor : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Gridare il fervore. Disastroso.",
							"Incatenato ad una roccia. Ciò non durerà fino al finire del giorno. Oracolo fausto.",
							"Guardare in alto crogiolandosi nel fervore. Rimpianto. Per chi tarda: ci sarà rimpianto.",
							"Accorto fervore. Otterrai molte cose. Non dubitare. Gli amici si riuniscono attorno a te come attorno ad una fibbia i capelli.",
							"Oracolo di malattia. Durerà a lungo, ma non porterà alla morte.",
							"Oscuro fervore. Un cambiamento porterà a una buona fine. Nessuna sfortuna.",
						};
					}
					
				}
				public override int Id => 16;
				public override string Sentency { get; set; } = "Il Fervore. Propizio designare vassalli. E far avanzare l'esercito.";

				public override string Comment => "Il Fervore: la linea dura trova l'appoggio [di tutte le altre linee morbide] e la volontà riesce a realizzarsi. \"Devoto\" e \"mobile\": questo è il Fervore. Infatti, il Fervore è \"devoto\" e \"mobile\", come il cielo e la terra. Inoltre «designare vassalli e far avanzare l'esercito» è come il cielo e la terra, essendo devoto nel movimento. Il sole e la luna non escono dalle loro orbite e le quattro stagioni non deviano dal loro corso: con tale movimento e devozione i saggi impartirono punizioni giuste e la gente gli obbedì. Il tempo del Fervore riveste grande importanza.";
				public override string Image => "Il Tuono erompe sonoro dalla Terra: questa è l'immagine del Fervore. Ispirandosi ad essa, gli antichi Re composero musica per lodare la virtù e fecero abbondanti offerte al supremo Iddio, riverendo i loro antenati.";
				public override string Series => "Quando si possiede grandezza e si è modesti viene di certo il fervore. Per questo segue il segno: il Fervore.";
				public override string MixedSign => "Fervore conduce a inerzia.";
				public override string AddictionalSentence => "Gli eroi introdussero porte doppie e guardiani notturni con nacchere, per affrontare i predoni. Questo lo trassero certamente dal segno: il Fervore.";
			}
			/// <summary>
			/// 17
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yang (9): Cambiamento al palazzo. Oracolo fausto. Uscire di casa per incontrare gente porta successo.
Commento: «Cambiamento al palazzo»: [La prima linea, essendo dura] segue correttamente ed è «fausta». «Uscire di casa per incontrare gente porta successo», non ci si perde.
Linea 1 - Perseverate nonostante la perdita, uscite e muovetevi
Siete colpiti da una sfortuna dalla quale però vi salvate. Un impedimento potrebbe stimolarvi all'attività. Alla fine torna a vostro vantaggio.
 
Linea 2 - Yin (6): Chi si lega al bambino perde l'adulto.
Commento: «Chi si lega al bambino...»: ciò significa che non si possono avere due cose contemporaneamente.
Linea 2 - Tenere il piccolo, perdere il grande
Il cammino non è ancora finito. Se vi fermate a qualcosa di inferiore perderete la possibilità di guadagnare le cose migliori.
Linea 3 - Yin (6): Chi si lega all'adulto perde il bambino. Chi segue riesce ad ottenere ciò che vuole. Oracolo propizio per chi rimane a casa.
Commento: «Chi si lega all'adulto...»: la volontà lascia andare ciò che sta sotto.
Linea 3 - Prendi il grande, lascia andare il piccolo
Siete molto vicini allo scopo. Rinunciate a qualcosa di minor valore per raggiungere l'obiettivo.
Linea 4 - Yang (9): Seguire per ottenere facilmente risultati. Oracolo:  disastroso. Chi è sincero, agisce in accordo con la giusta via e ha chiarezza. Come potrebbe esserci sfortuna?
Commento: Chi «segue per ottenere facilmente risultati», avrà disastro. «Chi è sincero, agisce in accordo con la giusta via» e riuscirà ad ottenere brillanti risultati.
Linea 4 - La caccia è terminata
Avete ottenuto tutto ciò di cui avevate bisogno. Sarebbe saggio che vi prendeste una pausa. Continuare sarebbe indice di ingordigia e attirerebbe invidia.
Linea 5 - Yang (9): Sincero nel bene. Fausto.
Commento: «Sincero nel bene. Fausto», perché la posizione [della linea] è corretta e centrale.
Linea 5 - La lealtà è premiata
Le azioni leali sono ricompensate dal successo. Possano verità e bontà seguirvi ogni giorno della vostra vita.
Linea 6 - Yin (6): Afferrati e legati, obbedienti e tenuti saldi. Il Re li sacrifica sul Monte dell'Ovest.
Commento: «Afferrati e legati»: la linea in alto è la fine.
Linea 6 - Legato da una fune, o da un vincolo di lealtà
Ora il cacciatore è catturato, dal nemico o dai legami di lealtà. Si può essere prigionieri della negatività e dei desideri, oppure dedicarsi ad aspirazioni più elevate.
				 */
			/// </summary>
			public class TheFollow : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Cambiamento al palazzo. Oracolo fausto. Uscire di casa per incontrare gente porta successo.",
							"Chi si lega al bambino perde l'adulto.",
							"Chi si lega all'adulto perde il bambino. Chi segue riesce ad ottenere ciò che vuole. Oracolo propizio per chi rimane a casa.",
							"Seguire per ottenere facilmente risultati. Oracolo:  disastroso. Chi è sincero, agisce in accordo con la giusta via e ha chiarezza. Come potrebbe esserci sfortuna?",
							"Sincero nel bene. Fausto.",
							"Afferrati e legati, obbedienti e tenuti saldi. Il Re li sacrifica sul Monte dell'Ovest.",
						};
					}
					
				}
				public override int Id => 17;
				public override string Sentency { get; set; } = "Il Seguire. Suprema riuscita. Propizio oracolo. Nessuna sfortuna.";

				public override string Comment => "Il Seguire: una linea dura scende sotto le linee morbide. I trigrammi sono il \"mobile\" e il \"gioioso\": questo è il Seguire. «Suprema riuscita; propizio oracolo; nessuna sfortuna»; ogni cosa sotto il cielo segue il corso del tempo. Il tempo del Seguire riveste grande importanza.";
				public override string Image => "Dentro il Lago c'è il Tuono: questa è l'immagine del Seguire. Ispirandosi ad essa, il signore, quando scende la notte, rincasa per ristorarsi e riposare.";
				public override string Series => "Dove vi è fervore ci sono certamente seguaci. Per questo viene il segno: il Fervore.";
				public override string MixedSign => "Il Seguire non tollera vecchie ragioni.";
				public override string AddictionalSentence => "Gli eroi addomesticarono il bue e imbrigliarono il cavallo. Così fu possibile trascinare pesanti carichi e raggiungere lontane regioni, e ciò fu utile al mondo. Questo lo trassero certamente dal segno: il Seguire.";
			}
			/// <summary>
			/// 18
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yin (6): Riparare quello che fu guastato dal padre. Quando ha un figlio, per il padre defunto, nessuna sfortuna. Pericoloso, ma alla fine fausto.
Commento: «Riparare quello che fu guastato dal padre», vuol dire continuare la volontà paterna.
Linea 1 - Il figlio maschio aiuta il padre, si addice
Vedete la necessità e vi mettete in moto per porvi rimedio. L'energia e l'entusiasmo vi sosterranno, anche se non avete esperienza in ciò che vi accingete a fare.
Linea 2 - Yang (9): Riparare quello che fu guastato dalla madre. L'oracolo non è possibile [che sia favorevole].
Commento: «Riparare quello che fu guastato dalla madre»: ottiene la via centrale.
Linea 2 - Il figlio maschio aiuta la madre, non si addice
Questa problema potrebbe non riguardarvi. In Cina sono le figlie a occuparsi delle madri e i figli dei padri.
Linea 3 - Yang (9): Riparare quello che fu guastato dal padre. Qualche piccolo rimpianto; nessuna grande sfortuna.
Commento: Chi «ripara quello che fu guastato dal padre», alla fine non avrà sfortuna.
Linea 3 - Costretti ad aiutare il padre
Spazientiti e pieni di rincrescimento per dover trascurare la vostra vita al fine di aiutare qualcun altro. Con il tempo le cose prenderanno la piega migliore.
 
Linea 4 - Yin (6): Riparare senza sforzo quello che fu guastato dal padre.  [2] Continuando così si vedrà umiliazione.
Commento: Chi «ripara senza sforzo quello che fu guastato dal padre», continuando così avrà umiliazione.
Linea 4 - Non aiutare il padre
Avete ignorato qualcosa, finché non si è tramutata in una crisi. Cercare di porre rimedio ora potrebbe soltanto peggiorare le cose. Aspettate finché non troverete il modo giusto di procedere.
Linea 5 - Yin (6): Riparare quello che fu guastato dal padre. Si ottiene lode.
Commento: «Riparare quello che fu guastato dal padre. Si ottiene lode»: perché la linea sostiene [la linea superiore].
Linea 5 - Ricevete lodi per aver aiutato vostro padre
Qualcuno ha bisogno del vostro aiuto e voi ricevete lodi nel darlo. Vale la pena farlo, anche se potreste avere qualche rimpianto.
Linea 6 - Yang (9): Non servire né il Re né i nobili. Porsi mete più elevate. [Disastroso] [3]
Commento: «Non servire né il Re né i nobili»: la volontà è giusta ed appropriata.
Linea 6 - Non servite il potente, servite un nobile scopo
Con una motivazione irreprensibile potreste agire in accordo con le più nobili aspirazioni, anche se ciò implica la contravvenzione ad alcune regole.
				 */
			/// </summary>
			public class TheFaultCorrection : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Riparare quello che fu guastato dal padre. Quando ha un figlio, per il padre defunto, nessuna sfortuna. Pericoloso, ma alla fine fausto.",
							"Riparare quello che fu guastato dalla madre. L'oracolo non è possibile [che sia favorevole].",
							"Riparare quello che fu guastato dal padre. Qualche piccolo rimpianto; nessuna grande sfortuna.",
							"Riparare senza sforzo quello che fu guastato dal padre.  [2] Continuando così si vedrà umiliazione.",
							"Riparare quello che fu guastato dal padre. Si ottiene lode.",
							"Non servire né il Re né i nobili. Porsi mete più elevate. [Disastroso] [3]",
						};
					}
					
				}
				public override int Id => 18;
				public override string Sentency { get; set; } = "La Correzione del guasto. Suprema riuscita. Propizio attraversare il grande fiume, tre giorni prima o tre giorni dopo il giorno iniziale. [1]";

				public override string Comment => " La Correzione del guasto. Le linee dure sono sopra e quelle morbide sono sotto; il Mite e il \"quieto\" producono la Correzione del guasto. La Correzione del guasto presagisce «suprema riuscita» regolando l'ordine in tutto il mondo. «Propizio attraversare il grande fiume», ciò significa che l'andare avanti otterrà risultati; «tre giorni prima o tre giorni dopo il giorno iniziale»:  ad ogni fine segue un nuovo inizio. Questa è la legge del cielo.";
				public override string Image => "Sotto il Monte soffia il Vento: questa è l'immagine della Correzione del guasto. Ispirandosi ad essa, il signore scuote la gente e ne rafforza la virtù.";
				public override string Series => "Quando si seguono altri con gioia, vi sono certamente opere da imprendere. Per questo segue il segno: la Correzione del guasto. Correggere le cose guaste significa imprendere opere.";
				public override string MixedSign => "Correzione del guasto. In seguito vi è ordine.";
				public override string AddictionalSentence => "";
			}
			/// <summary>
			/// 19
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yang (9): Avvicinarsi comunitario. Oracolo fausto.
Commento: «Avvicinarsi comunitario: oracolo fausto», perché la volontà si muove correttamente [la linea dura nella prima posizione].
Linea 1 - Amici in visita
Vi riunite con gli amici. Siete fonte di ispirazione.
Linea 2 - Yang (9): Avvicinarsi comunitario. Fausto. Nulla che non sia propizio.
Commento: «Avvicinarsi comunitario: fausto, nulla che non sia propizio», ma non è obbediente all'ordine [in seconda posizione non c'è una linea morbida].
Linea 2 - Essere d'esempio per gli altri
Passate da una vecchia situazione a una nuova. Gli ostacoli verranno sormontati. Esercitate un'influenza positiva sugli altri.
Linea 3 - Yin (6): Avvicinarsi compiaciuto. Nulla che sia propizio. Poiché già ci si è addolorati per questo, nessuna sfortuna.
Commento: «Avvicinarsi compiaciuto»: la posizione della linea non è appropriata. «Poiché già ci si è addolorati per questo», la sfortuna non durerà a lungo.
Linea 3 - Mosse avventate, pessima idea
Un atteggiamento compiaciuto e noncurante in un momento di crisi vi creerà problemi. Dovete individuare al più presto il vostro errore ed eviterete il peggio.
Linea 4 - Yin (6): Avvicinarsi completo. Nessuna sfortuna.
Commento: «Avvicinarsi completo; nessuna sfortuna», perché la posizione [della linea] è appropriata.
Linea 4 - Trattamento perfetto
La vostra sincerità e integrità persuade gli altri. Siete nella posizione per risolvere una crisi con calma e discrezione.
Linea 5 - Yin (6): Avvicinarsi saggio. Questo si addice a un grande signore. Fausto.
Commento: «Questo si addice a un grande signore»: ciò è detto per la posizione centrale in quanto procede avanti.
Linea 5 - Comportamento saggio
Possedete la saggezza per guardare dentro e trovare la maniera giusta di affrontare una decisione importante. In questa linea si riuniscono le nobili qualità della saggezza, della compassione e dell'azione.
Linea 6 - Yin (6): Avvicinarsi magnanimo. Fausto. Nessuna sfortuna.
Commento: Il responso «fausto» riservato all'«avvicinarsi magnanimo» è dovuto alla volontà che è nel trigramma interno.
Linea 6 - Incontro sincero, nessuna macchia
Il massimo è già stato raggiunto e siete andati oltre. Da questa nuova prospettiva siete liberi di esprimere le sensazioni più profonde su ciò che è giusto e vero.
				 */
			/// </summary>
			public class TheApproach : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Avvicinarsi comunitario. Oracolo fausto.",
							"Avvicinarsi comunitario. Fausto. Nulla che non sia propizio.",
							"Avvicinarsi compiaciuto. Nulla che sia propizio. Poiché già ci si è addolorati per questo, nessuna sfortuna.",
							"Avvicinarsi completo. Nessuna sfortuna.",
							"Avvicinarsi saggio. Questo si addice a un grande signore. Fausto.",
							"Avvicinarsi magnanimo. Fausto. Nessuna sfortuna.",
						};
					}
					
				}
				public override int Id => 19;
				public override string Sentency { get; set; } = "L'Avvicinamento. Suprema riuscita. Propizio oracolo. Quando si arriva all'ottavo mese lunare, disastroso.";

				public override string Comment => "L'Avvicinamento. Le linee dure penetrano e crescono:  \"gioioso\" e \"devoto\". La linea dura [seconda linea] è centrale e trova corrispondenza [dalla quinta linea]. «Suprema riuscita» a causa di questa correttezza: questa è la legge del cielo. «Quando si arriva all'ottavo mese lunare, disastroso», perché il declino non si fa aspettare a lungo.";
				public override string Image => "La Terra sopra il Lago: questa è l'immagine dell'Avvicinamento. Ispirandosi ad essa, il signore educa  e si prende cura della gente incessantemente e, generosamente, la protegge senza limiti.";
				public override string Series => "Quando vi sono grandi opere da imprendere è possibile diventare grandi. Per questo segue il segno: l'Avvicinamento. Avvicinamento significa diventare grandi.";
				public override string MixedSign => "Il significato dei segni l'Avvicinamento e la Contemplazione è che essi in parte danno, in parte tolgono.";
				public override string AddictionalSentence => "";
			}
			/// <summary>
			/// 20
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yin (6): Osservare con ingenuità. Per una persona mediocre: nessuna sfortuna. Rimpianto per un signore.
Commento: «Osservare con ingenuità», come dice la prima linea morbida, è la via delle persone mediocri.
Linea 1 - La visione di un bambino
È giusto che un bambino abbia una visione del mondo semplice e innocente. Per un adulto, invece, ciò potrebbe rivelare pregiudizi, ristrettezza di vedute e ignoranza.
Linea 2 - Yin (6): Osservare di nascosto. Propizio oracolo per una donna nubile.
Commento: «Osservare di nascosto»: benché l'oracolo sia propizio alla donna, rimane pur sempre vergognoso.
Linea 2 - Sbirciare da dietro una tenda
La paura e l'estraneità motivano il vostro rifiuto a partecipare. Va bene così. Se però siete coinvolti, agite con coraggio.
Linea 3 - Yin (6): Osservare la propria vita che va avanti o torna indietro.
Commento: «Osservare la propria vita che va avanti o torna indietro», non allontanandosi dalla giusta via.
Linea 3 - Osservare gli alti e bassi della vita
Guardate con attenzione il vostro passato per prendere le decisioni migliori per il futuro. Mettete in evidenza e sfruttate le vostre migliori caratteristiche.
 
Linea 4 - Yin (6): Osservare la splendore dell'impero. Propizio per coloro che il re invita.
Commento: Chi «osserva lo splendore dell'impero», sarà onorato come ospite.
Linea 4 - L'ospite osserva l'impero
Siete stati invitati a partecipare a un'avventura. Giudicatene il valore dal trattamento che vi viene riservato e dall'atmosfera del nuovo ambiente.
Linea 5 - Yang (9): Osservare la propria vita. Per un signore: nessuna sfortuna.
Commento: «Osservare la propria vita» cioè osservando la gente.
Linea 5 - Osservare se stessi, senza macchia
Avete sufficiente chiarezza nella mente per valutare le vostre stesse azioni. Il vostro valore si misura con l'effetto che avete su coloro che vi circondano.
Linea 6 - Yang (9): Osservare la vita altrui. Per un signore: nessuna sfortuna.
Commento: «Osservare la vita altrui»: la volontà non è ancora appagata.
Linea 6 - Osservate gli altri, senza macchia
Dalla vostra posizione potete osservare e formarvi un'opinione intelligente sulla condizione altrui, senza per questo rimanerne coinvolto.
				 */
			/// </summary>
			public class TheContemplation : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Osservare con ingenuità. Per una persona mediocre: nessuna sfortuna. Rimpianto per un signore.",
							"Osservare di nascosto. Propizio oracolo per una donna nubile.",
							"Osservare la propria vita che va avanti o torna indietro.",
							"Osservare la splendore dell'impero. Propizio per coloro che il re invita.",
							"Osservare la propria vita. Per un signore: nessuna sfortuna.",
							"Osservare la vita altrui. Per un signore: nessuna sfortuna.",
						};
					}
					
				}
				public override int Id => 20;
				public override string Sentency { get; set; } = "La Contemplazione. Si compie l'abluzione ma non il rito sacrificale. Essere sinceri con alto contegno.";

				public override string Comment => "La Contemplazione. La grande visione sta in alto. \"Devoto\" e Mite. [La quinta linea è] centrale e corretta e osserva tutto il mondo: questa è la Contemplazione. «Si compie l'abluzione, ma non il rito sacrificale: essere sinceri con alto contegno»: quelli al di sotto contemplano e sono trasformati. Contemplando la divina legge del cielo, le quattro stagioni non cambiano mai la loro successione. Ispirandosi a questa divina legge, i saggi hanno istituito il sistema educativo e ottenuto il consenso del mondo intero.";
				public override string Image => "Il Vento soffia sulla Terra: questa è l'immagine della Contemplazione. Ispirandosi ad essa, gli antichi Re ispezionavano le regioni del mondo, osservavano la gente e istituivano il sistema educativo.";
				public override string Series => "Quando le cose sono grandi si possono contemplare. Per questo segue il segno: la Contemplazione.";
				public override string MixedSign => "Il significato dei segni l'Avvicinamento e la Contemplazione è che essi in parte danno, in parte tolgono.";
				public override string AddictionalSentence => "";
			}
			/// <summary>
			/// 21
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yang (9): Avere i piedi nei ceppi, mutilando le dita. Nessuna sfortuna.
Commento: «Avere i piedi nei ceppi, mutilando le dita»: [la volontà] non può andare avanti.
Linea 1 - I ceppi ai piedi prevengono gli errori
All'inizio venite trattenuti dal compiere una mossa sbagliata. Una parola di avvertimento è tutto ciò di cui avete bisogno per correggervi.
Linea 2 - Yin (6): Mordere carne tenera, mutilando il naso. Nessuna sfortuna.
Commento: «Mordere carne tenera, mutilando il naso», perché questa linea morbida sta sopra una linea dura.
Linea 2 - Mordere così a fondo finché il naso scompare
Siete impazienti e ossessivi. Siete troppo vicini a una situazione per poterla giudicare.
Linea 3 - Yin (6): Mordere carne secca. Ci si avvelena. Piccola umiliazione. Nessuna sfortuna.
Commento: «Ci si avvelena», perché la posizione di questa linea morbida non è appropriata.
Linea 3 - Mordete la carne secca, trovate il veleno
Affrontare le amare esperienze del passato vi permetterà di progredire. Potreste provare vergogna per qualcosa che avete commesso. Avete l'occasione di porre rimedio.
Linea 4 - Yang (9): Mordere carne cartilaginosa essiccata. Si trovano frecce di metallo. Propizio per una faccenda difficile. Oracolo fausto.
Commento: «Propizio per una faccenda difficile; oracolo fausto», ma non ancora splendido.
Linea 4 - Mordete la carne cartilaginosa, trovate la punta di una freccia
Siete determinati ad andare al cuore di una faccenda difficile e annosa. Se aggredite il problema con energia, ne uscirete vincitori.
 
Linea 5 - Yin (6): Mordere carne di muscolo essiccata. Si trova oro giallo. Oracolo pericoloso. Nessuna sfortuna.
Commento: «Oracolo pericoloso; nessuna sfortuna», a causa della posizione [non] appropriata [della linea]. [1]
Linea 5 - Mordete la carne secca pregiata, trovate l'oro, state attenti
La vostra azione risoluta vi ha procurato una ricompensa di valore. Se vi attaccherete troppo al premio sarà la vostra rovina. Un tempo si pensava che ingoiare l'oro potesse essere fatale.
Linea 6 - Yang (9): Portare la gogna al collo, mutilando le orecchie. Disastroso.
Commento: «Portare la gogna al collo, mutilando le orecchie»: ciò significa non udire chiaramente.
Linea 6 - Indossare un giogo pesante che copre le orecchie
Siete andati troppo lontani e siete sordi ai consigli degli altri. Prigioniero della vostra stessa ignoranza arrogante e sventata. Avete orecchie che non sentono.
				 */
			/// </summary>
			public class TheBiteThatBreaks : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Avere i piedi nei ceppi, mutilando le dita. Nessuna sfortuna.",
							"Mordere carne tenera, mutilando il naso. Nessuna sfortuna.",
							"Mordere carne secca. Ci si avvelena. Piccola umiliazione. Nessuna sfortuna.",
							"Mordere carne cartilaginosa essiccata. Si trovano frecce di metallo. Propizio per una faccenda difficile. Oracolo fausto.",
							"Mordere carne di muscolo essiccata. Si trova oro giallo. Oracolo pericoloso. Nessuna sfortuna.",
							"Portare la gogna al collo, mutilando le orecchie. Disastroso.",
						};
					}
					
				}
				public override int Id => 21;
				public override string Sentency { get; set; } = "Il Morso che spezza. Riuscita. Propizio intraprendere una causa legale.";

				public override string Comment => "Il Morso che spezza. C'è qualcosa stretto tra le mascelle: questo è il Morso che spezza. «Il Morso che spezza: riuscita». Le linee dure e le linee morbide sono [equamente] divise tra \"mobile\" e \"luminoso\". Il Tuono e il Fulmine [Fuoco] si uniscono in una sequenza di linee. La linea morbida occupa la posizione centrale e sale nel trigramma superiore. Benché le linee non siano adatte alla loro posizione, è «propizio intraprendere una causa legale».";
				public override string Image => "Tuono e fulmine (=Fuoco): questa è l'immagine del Morso che spezza. Ispirandosi ad essa, gli antichi Re stabilivano chiaramente le punizioni ed rendevano salde le leggi.";
				public override string Series => "Quando vi è cosa da poter contemplare, allora vi è cosa che crea unione. Per questo segue il segno: il Morso che spezza. Spezzare mordendo significa unire.";
				public override string MixedSign => "Spezzare mordendo significa fagocitare.";
				public override string AddictionalSentence => "Quando il sole stava al culmine il Divino Coltivatore teneva mercato. Egli radunava gli uomini sulla terra e raccoglieva le derrate sulla terra. Essi le scambiavano tra loro, poi ritornavano a casa, e ogni cosa andava al suo posto. Questo lo trasse certamente dal segno: il Morso che spezza.";
			}
			/// <summary>
			/// 22
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yang (9): Abbellirsi i piedi. Lasciare il carro e mettersi a camminare.
Commento: «Lasciare il carro e mettersi a camminare»: perchè la prima linea non ha nessuna linea su cui salire.
Linea 1 - Con i piedi adorni, abbandonate la carrozza e camminate
Desiderate essere visti come realmente siete. Fidatevi delle vostre opinioni anziché di quelle altrui. È il momento che vi reggiate sui vostri bei piedi. Le persone semplici preferiscono camminare.
Linea 2 - Yin (6): Abbellirsi la barba.
Commento: «Abbellirsi la barba»: questa linea supporta la linea superiore.
Linea 2 - Regolare la barba
Avere un bell'aspetto vi fa sentire bene. Migliorate voi stessi imitando le azioni virtuose degli altri. L'imitazione è la forma più sincera di adulazione. Se volete inserirvi, adeguatevi al comportamento altrui. Guardatevi dalla vanità.
Linea 3 - Yang (9): Abbellirsi con la rugiada. Oracolo a lungo termine: fausto.
Commento: Un «oracolo fausto a lungo termine» significa che niente alla fine deluderà.
Linea 3 - Gli ornamenti risplendono per la rugiada
Adoperate la vostra volontà per farvi coinvolgere e per affrontare le difficoltà, se volete cambiare le cose. Quando date il meglio di voi stessi attirate l'attenzione favorevole degli altri.
Linea 4 - Yin (6): Abbellirsi e risplendere di bianco, [2] come un bianco cavallo alato. Non alleato coi banditi ma cerca un matrimonio.
Commento: La quarta linea morbida occupa una posizione appropriata ma lascia dei dubbi. «Non alleato coi banditi ma cerca un matrimonio» significa: alla fine nessuna calamità.
Linea 4 - In sella a un cavallo bianco; non viene per depredare, ma per sposarsi
Se in una nuova circostanza vi sentite goffi, dichiarate le vostre intenzioni pacifiche e verrete accettato e guardato con fiducia.
Linea 5 - Yin (6): Abbellire il giardino in mezzo alle colline, sebbene piccolo e povero è il rotolo di seta. Umiliazione ma alla fine fausto.
Commento: «Fausto» in una quinta linea morbida significa felicità.
Linea 5 - Sebbene l'involto di seta donato sia piccolo, è decorato con scene di un giardino
Sebbene i vostri doni non siano grandiosi, sono belli e offerti con le migliori intenzioni. Doni modesti offerti con buone intenzioni sono sempre bene accetti.
Linea 6 - Yang (9): Abbellirsi di bianco. [3] Nessuna sfortuna.
Commento: «Abbellirsi di bianco; nessuna sfortuna»: perché la linea in alto realizza la volontà.
Linea 6 - Vestito di bianco
A volte la manifestazione più fine è semplice e naturale. Gli eccessivi ornamenti nascondono la natura essenziale delle cose. Bellezza è verità e verità è bellezza.
				 */
			/// </summary>
			public class TheBeauty : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Abbellirsi i piedi. Lasciare il carro e mettersi a camminare.",
							"Abbellirsi la barba.",
							"Abbellirsi con la rugiada. Oracolo a lungo termine: fausto.",
							"Abbellirsi e risplendere di bianco, [2] come un bianco cavallo alato. Non alleato coi banditi ma cerca un matrimonio.",
							"Abbellire il giardino in mezzo alle colline, sebbene piccolo e povero è il rotolo di seta. Umiliazione ma alla fine fausto.",
							"Abbellirsi di bianco. [3] Nessuna sfortuna.",
						};
					}
					
				}
				public override int Id => 22;
				public override string Sentency { get; set; } = "La Bellezza. Riuscita. Moderatamente propizio quando c'è un luogo in cui andare.";

				public override string Comment => "La Bellezza. «Riuscita»: una linea morbida viene ad adornare le [due] linee dure [che le stanno sotto e sopra], quindi «riuscita». Una linea dura sale sulla vetta per abbellire le [due] linee morbide [sottostanti]: perciò «moderatamente propizio quando c'è un luogo in cui andare». Questa è la forma del cielo [1]. Le forme che sono \"luminoso\" (trigramma inferiore) e \"quieto\" (trigramma superiore) sono la forma dell'uomo. Osservando la forma del cielo si può calcolare il mutamento dei tempi; osservando la forma dell'uomo si può trasformare la società.";
				public override string Image => "Sotto il Monte c'è il Fuoco: questa è l'immagine della Bellezza. Ispirandosi ad essa, un signore fa chiarezza su tutti gli affari amministrativi e non si azzarda a pronunciare sentenze approssimative.";
				public override string Series => "Le cose non devono mai congiungersi all'improvviso e brutalmente. Per questo segue il segno: la Bellezza. Bellezza significa ornamento.";
				public override string MixedSign => "Bellezza significa colorito naturale.";
				public override string AddictionalSentence => "";
			}
			/// <summary>
			/// 23
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yin (6): Il letto si sgretola nelle gambe. Un sogno il cui oracolo è disastroso. [1]
Commento: «Il letto si sgretola nelle gambe»; la distruzione comincia dalla base.
Linea 1 - Le gambe del letto si spaccano
L'inizio di un'influenza destabilizzante. Ciò su cui contate si sta erodendo. Riparatelo ora. Potreste incontrare mancanza di disciplina e incertezza d'intenti in voi stessi o negli altri.
Linea 2 - Yin (6): Il letto si sgretola nel bordo. Un sogno il cui oracolo è disastroso. [1]
Commento: «Il letto si sgretola nel bordo», perchè non si hanno associati.
Linea 2 - La struttura del letto si spacca
La struttura della vostra vita va deteriorandosi. La situazione richiede attenzione e rimedio. Quelli su cui contate potrebbero non rispettare gli obblighi.
Linea 3 - Yin (6): Sgretolamento con loro. Nessuna sfortuna.
Commento: «Sgretolamento con loro. Nessuna sfortuna», perché manca [una relazione con le linee] sopra e sotto.
Linea 3 - Ridotto a misura, senza colpa
È il momento di rompere con le cattive compagnie e di legare con gli amici onesti. Vanno operate scelte e distinzioni che potrebbero dividervi da qualche compagno.
Linea 4 - Yin (6): Il letto si sgretola nella pelle. Disastroso.
Commento: «Il letto si sgretola nella pelle»: si avvicina la sciagura.
Linea 4 - Il materasso si squarcia
Questa è una posizione molto instabile. La minima deviazione vi metterà in una situazione di immediato pericolo. Non siete immuni dalla sventura. Siete prossimi al disastro.
Linea 5 - Yin (6): Branco di pesci. Le donne di palazzo saranno favorite. Nulla che non sia propizio.
Commento: «Le donne di palazzo saranno favorite»: alla fine nessuna calamità.
Linea 5 - Un pesce all'amo, come una dama di palazzo
Non controllate più la situazione o il vostro destino. Concedete la vittoria, sappiate perdere e vi eviterete un decadimento peggiore.
Linea 6 - Yang (9): Un bel frutto non ancora mangiato. Il signore ottiene un carro;  al meschino si sgretola la capanna.
Commento: «Il signore ottiene un carro», trasportato dalla gente, mentre «al meschino si sgretola la capanna»: alla fine è inservibile.
Linea 6 - Rimane un grosso frutto non ancora mangiato
Questo è un inizio e una fine. Il seme esiste per rinnovarsi. Coloro che si aggrappano alle verità eterne sopravviveranno. Coloro che si attaccano alla superficie verranno distrutti.
				 */
			/// </summary>
			public class TheCrumbling : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Il letto si sgretola nelle gambe. Un sogno il cui oracolo è disastroso. [1]",
							"Il letto si sgretola nel bordo. Un sogno il cui oracolo è disastroso. [1]",
							"Sgretolamento con loro. Nessuna sfortuna.",
							"Il letto si sgretola nella pelle. Disastroso.",
							"Branco di pesci. Le donne di palazzo saranno favorite. Nulla che non sia propizio.",
							"Un bel frutto non ancora mangiato. Il signore ottiene un carro;  al meschino si sgretola la capanna.",
						};
					}
					
				}
				public override int Id => 23;
				public override string Sentency { get; set; } = "Lo Sgretolamento. Non è propizio andare in alcun luogo.";

				public override string Comment => "Lo Sgretolamento. Lo Sgretolamento significa rovina. La linea morbida muta in una linea dura [in alto]. «Non è propizio andare in alcun luogo», perché le persone meschine [le linee morbide] stanno diventando sempre più forti. \"Devoto\" e \"quieto\": questo è il significato tratto dall'osservazione dell'immagine. Un signore deve rispettare le regole secondo cui si alternano la scomparsa e la crescita, il vuoto e il pieno, nel cielo.";
				public override string Image => "Il Monte si appoggia sulla Terra: questa è l'immagine dello Sgretolamento. Ispirandosi ad essa, un superiore deve trattare con generosità gli inferiori, per mantenere la pace della propria posizione.";
				public override string Series => "Se si eccede nell'abbellimento allora la riuscita si esaurisce. Per questo segue il segno: lo Sgretolamento. Lo Sgretolamento significa rovina.";
				public override string MixedSign => "Sgretolarsi significa putrefazione.";
				public override string AddictionalSentence => "";
			}
			/// <summary>
			/// 24
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yang (9): Ritorno senza essersi allontanati troppo. Nessun danno o rimpianto. Totalmente fausto.
Commento: «Ritorno senza essersi allontanati troppo»: così si coltiva la propria virtù.
Linea 1 - Ritorno da una breve distanza
Ritornare al vostro obiettivo di partenza è facile, dal momento che avete compiuto una lieve deviazione. Gli errori saranno subito evidenti e presto corretti. Nulla è andato perso.
Linea 2 - Yin (6): Ritorno soddisfatto. Fausto.
Commento: Segno «fausto» per chi «ritorna soddisfatto»: grazie alla bontà [della linea] sottostante.
Linea 2 - Un ritorno tranquillo
Rimetterete ordine nella vostra vita correggendo gli errori nelle associazioni. Comportatevi con onestà e considerazione con coloro dai quali dipendete. Rispettate i consigli validi.
Linea 3 - Yin (6): Ritorno  molteplice (= triste). [1] Pericoloso. Nessuna sfortuna.
Commento: Il «pericolo» nel «ritorno molteplice» significa che non ci sarà sfortuna.
Linea 3 - Ritorno molteplice
Non potete evitare questa situazione: dovrete affrontarla e porvi rimedio. La perseveranza è la chiave per ovviare a questi pericoli.
Linea 4 - Yin (6): Camminando nel mezzo, ritorno da soli.
Commento: «Camminando nel mezzo, ritorno da soli», seguendo la giusta via.
Linea 4 - Partenza con altri, ritorno da soli
Siete in compagnia di qualcuno che vi porta fuori strada. Potrebbe essere come viaggiare da soli.
Linea 5 - Yin (6): Ritorno agevole. Nessun rimpianto.
Commento: «Ritorno agevole; nessun rimpianto»: essendo centrale è capace di esaminare se stesso.
Linea 5 - Ritorno sincero
Esaminando i vostri motivi siete capaci di riprendere il sentiero giusto. Aggrapparsi agli errori del passato non è utile.
Linea 6 - Yin (6): Ritorno perdendo la strada. Disastroso. Ci sarà una catastrofe. Per un'impresa militare: si subirà una grave sconfitta, con disastro per il governatore dello stato. Non sarà più possibile qualsiasi campagna di attacco nei prossimi dieci anni.
Commento: Il «disastro» derivato dal «ritornare perdendo la strada» è dovuto alla trasgressione della regola cui un sovrano deve attenersi.
Linea 6 - Ritorno fallito
Avete in mano l'ultima estremità di qualcosa. Troppo poco e troppo tardi. È una faccenda di breve durata, un incontro fugace. Un piccolo ostacolo da sormontare prima di raggiungere la vostra destinazione.
				 */
			/// </summary>
			public class TheReturn : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Ritorno senza essersi allontanati troppo. Nessun danno o rimpianto. Totalmente fausto.",
							"Ritorno soddisfatto. Fausto.",
							"Ritorno  molteplice (= triste). [1] Pericoloso. Nessuna sfortuna.",
							"Camminando nel mezzo, ritorno da soli.",
							"Ritorno agevole. Nessun rimpianto.",
							"Ritorno perdendo la strada. Disastroso. Ci sarà una catastrofe. Per un'impresa militare: si subirà una grave sconfitta, con disastro per il governatore dello stato. Non sarà più possibile qualsiasi campagna di attacco nei prossimi dieci anni.",
						};
					}
					
				}
				public override int Id => 24;
				public override string Sentency { get; set; } = "Il Ritorno. Riuscita. Uscire e rientrare senza male. Amici arrivano. Nessuna sfortuna. La stessa è la via dell'andare e del tornare e al settimo giorno viene il ritorno. Propizio quando c'è un luogo in cui andare.";

				public override string Comment => "Il Ritorno: «riuscita». La linea dura ritorna [dalla sua posizione nell'esagramma 23] in basso: \"Mobile\" e \"devoto\". «Uscire e rientrare senza male», e «amici arrivano; nessuna sfortuna». «La stessa è la via dell'andare e del tornare e al settimo giorno viene il ritorno». Questa è la legge dei corpi celesti. «Propizio quando c'è un luogo in cui andare», perché la linea dura è destinata a crescere. Insomma, il Ritorno ci fa capire la legge intrinseca del cielo e della terra.";
				public override string Image => "Il Tuono sotto la Terra: questa è l'immagine del Ritorno. Ispirandosi ad essa, gli antichi Re nel giorno del solstizio invernale facevano chiudere le porte della città, impedendo l'ingresso ai mercanti e agli stranieri, mentre i sovrani quel giorno non ispezionavano i lori territori.";
				public override string Series => "Le cose non possono rimanere annientate per sempre. Quando ciò che sta in alto è completamente sgretolato, esso ritorna da sotto. Per questo segue il segno: il Ritorno.";
				public override string MixedSign => "Ritorno significa tornare di nuovo.";
				public override string AddictionalSentence => "Il ritorno è il tronco della virtù. Il ritorno è piccolo e tuttavia differente dalle cose esteriori. Il ritorno serve alla conoscenza di se stessi.";
			}
			/// <summary>
			/// 25
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yang (9): Avanzare inaspettato. Fausto.
Commento: «Avanzare inaspettato»: la volontà sarà realizzata.
Linea 1 - Proseguite, non aspettatevi nulla
Se all'inizio le vostre intenzioni sono pure, lo saranno anche le azioni che le seguiranno. Agite senza aspettarvi nulla, senza cercare ricompense e avrete successo.
Linea 2 - Yin (6): Si ottiene un raccolto senza aver arato, si coltiva un campo senza aver dissodato la terra. In questo modo è propizio quando c'è un luogo in cui andare.
Commento: «Si ottiene un raccolto senza aver arato», ma non c'è ancora la ricchezza.
Linea 2 - Mietere senza aver seminato
Fatevi coinvolgere appieno in ciò che avete a portata di mano. Lasciate i risultati a se stessi. Sfruttate i vantaggi di cui disponete qui e adesso. Lasciate che le cose accadano con naturalezza.
Linea 3 - Yin (6): Disgrazia inaspettata. Una mucca legata da qualcuno:  è guadagno per il passante e perdita per il contadino.
Commento: Il «passante» ha avuto la mucca, mentre il «contadino» una disgrazia.
Linea 3 - Il viandante prende il bue; la sua vincita e la perdita del contadino
La vittoria di una persona implica sempre la sconfitta di un'altra. Fortuna o sfortuna inattesa. Attaccamento alla perdita e alla vincita. Il tentativo di assicurarsi contro le perdite e garantirsi le vincite è vano.
 
Linea 4 - Yang (9): L'oracolo è possibile [che sia favorevole]. Nessuna sfortuna.
Commento: «L'oracolo è possibile; nessuna sfortuna»: mantenersi saldi.
Linea 4 - Perseverate, non vi accadrà nulla di male
Siete in un periodo di transizione, potreste sentirvi isolati e impotenti. Andrà tutto bene se soltanto avrete fiducia nel vostro stesso giudizio e agirete di conseguenza.
Linea 5 - Yang (9): Malattia inaspettata. Non utilizzare medicine. L'esito sarà felice.
Commento: Una «medicina inaspettata» è meglio che non la si sperimenti.
Linea 5 - Non adoperate medicine quando tutto va bene
Una sfortuna inattesa si allontanerà così com'è venuta, da sola. Qualsiasi azione potrebbe soltanto peggiorare le cose. Potreste farvi carico dei problemi di qualcun altro.
Linea 6 - Yang (9): Avanzare inaspettato. Provocherà una disgrazia. Nulla è propizio.
Commento: «Avanzare inaspettato» è causa di disastro alla fine.
Linea 6 - Viaggiare senza un piano attira disgrazia
È finito il tempo in cui potevate affidarvi all'ispirazione del momento. Anziché brancolare, fermatevi per un momento e preparate un piano.
				 */
			/// </summary>
			public class TheInnocence : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Avanzare inaspettato. Fausto.",
							"Si ottiene un raccolto senza aver arato, si coltiva un campo senza aver dissodato la terra. In questo modo è propizio quando c'è un luogo in cui andare.",
							"Disgrazia inaspettata. Una mucca legata da qualcuno:  è guadagno per il passante e perdita per il contadino.",
							"L'oracolo è possibile [che sia favorevole]. Nessuna sfortuna.",
							"Malattia inaspettata. Non utilizzare medicine. L'esito sarà felice.",
							"Avanzare inaspettato. Provocherà una disgrazia. Nulla è propizio.",
						};
					}
					
				}
				public override int Id => 25;
				public override string Sentency { get; set; } = "L'Innocenza. Suprema riuscita. Propizio oracolo. Se non si è retti, seguirà una disgrazia. Non è propizio andare in alcun luogo.";

				public override string Comment => "L'Innocenza. Una linea dura scende dal trigramma esterno per dominare il trigramma interno: \"mobile\" e \"forte\". La quinta linea dura centrale trova corrispondenza con la seconda linea morbida. «Suprema riuscita» a causa di questa correttezza: questa è la volontà del cielo. «Se non si è retti, seguirà una disgrazia. Non è propizio andare in alcun luogo». Come si può procedere quando l'inaspettato arriva? Senza la protezione della legge celeste, forse si potrebbe compiere qualche movimento?";
				public override string Image => "Il Tuono romba sotto il Cielo dappertutto: questa è l'immagine dell'Innocenza. Ispirandosi ad essa, gli antichi Re nutrivano, ricchi di virtù, tutti gli esseri, in accordo con l'andamento delle stagioni.";
				public override string Series => "Ritornando si diventa liberi da colpa. Per questo segue il segno: l'Innocenza.";
				public override string MixedSign => "L'inaspettato significa disgrazia dal destino.";
				public override string AddictionalSentence => "";
			}
			/// <summary>
			/// 26
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yang (9): C'è un pericolo. E' propizio fermarsi.
Commento: «C'è un pericolo. E' propizio fermarsi», per non esporsi al disastro.
Linea 1 - Il pericolo è vicino, desistete
Adesso non siete abbastanza forti per correre avanti. Indietreggiate ed evitate i guai. Un amico influente potrebbe consigliarvi di rallentare per evitare sventure.
Linea 2 - Yang (9): Il carro ha perso i raggi delle ruote.
Commento: «Il carro ha perso i raggi delle ruote»: la posizione è centrale, quindi nessuna calamità.
Linea 2 - L'asse cade dalla carrozza
Durante la corsa avete perso l'equilibrio e tutto è andato distrutto. Non è colpa vostra. Fate le riparazioni necessarie e poi proseguite.
Linea 3 - Yang (9): I più bei cavalli per un inseguimento. Oracolo propizio per una faccenda difficile. Ogni giorno ci si esercita nel guidare i carri e nel difendersi con le armi. Propizio quando c'è un luogo in cui andare.
Commento: «Propizio quando c'è un luogo in cui andare»: perché concorda con la [linea] superiore nella volontà.
Linea 3 - Esercitatevi ogni giorno nel guidare la carrozza e nel difendervi con le armi
La via davanti a voi è aperta e siete propensi ad avanzare. Nel desiderio smodato di procedere potreste diventare incauti. Perfezionate le capacità che vi sono necessarie.
Linea 4 - Yin (6): Il finimento frontale di un bue giovane. Totalmente fausto.
Commento: Il responso «totalmente fausto» indicato dalla quarta linea morbida presagisce felicità.
Linea 4 - Coprite le corna di un giovane toro
È facile imprimere una direzione a una grande forza quando ancora si trova ai primi stadi. Correggete le cattive abitudini prima che si impadroniscano di voi e rendete più salde quelle buone.
Linea 5 - Yin (6): La zanna che cresce in un cinghiale castrato. [1] Fausto.
Commento: Il responso «fausto» della quinta linea morbida [che sostiene una sesta linea dura] presagisce buona fortuna.
Linea 5 - Legare un maiale castrato
Siete in grado di trasformare positivamente le azioni dannose, scoprendo la fonte d'errore e ponendovi rimedio. L'impiego della forza bruta non sarà mai efficace quanto la comprensione del problema.
Linea 6 - Yang (9): Raggiungere la via del cielo. Riuscita.
Commento: «Raggiungere la via del cielo»: la via è largamente aperta per avanzare.
Linea 6 - Percorrere la strada celeste
Gli eventi raggiungono un culmine e poi imboccano il cammino contrario. Quando avrete ogni cosa sotto controllo le vostre azioni saranno più facili, meno faticose.
				 */
			/// </summary>
			public class TheStrengthOfTheGreatTamer : Hexagram

			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"C'è un pericolo. E' propizio fermarsi.",
							"Il carro ha perso i raggi delle ruote.",
							"I più bei cavalli per un inseguimento. Oracolo propizio per una faccenda difficile. Ogni giorno ci si esercita nel guidare i carri e nel difendersi con le armi. Propizio quando c'è un luogo in cui andare.",
							"Il finimento frontale di un bue giovane. Totalmente fausto.",
							"La zanna che cresce in un cinghiale castrato. [1] Fausto.",
							"Raggiungere la via del cielo. Riuscita.",
						};
					}
					
				}
				public override int Id => 26;
				public override string Sentency { get; set; } = "La Forza domatrice del grande. Propizio oracolo. Fausto non mangiare a casa. Propizio attraversare il grande fiume.";

				public override string Comment => "La Forza domatrice del grande: [i trigrammi sono] saldo e \"forte\", solido e vero, splendente e luminoso, con un quotidiano rinnovo della virtù. La linea dura in alto onora colui che è degno. L' arresto (= \"quieto\") di ciò che è \"forte\" significa ferma correttezza. «Fausto non mangiare a casa», perché vengono nutrite le persone meritevoli. «Propizio attraversare il grande fiume»: [la quinta linea] corrisponde [alla seconda linea] nel trigramma del Cielo.";
				public override string Image => "Il Cielo sotto il Monte: questa è l'immagine della Forza domatrice del grande. Ispirandosi ad essa, il signore fa conoscenza di molti fatti e detti del passato per rafforzare la propria virtù.";
				public override string Series => "Quando vi è innocenza, allora si può domare. Per questo segue il segno: la Forza domatrice del grande.";
				public override string MixedSign => "La Forza domatrice del grande poggia sul tempo.";
				public override string AddictionalSentence => "";
			}
			/// <summary>
			/// 27
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yang (9): Tu metti da parte il tuo guscio sacro di tartaruga e mi osservi con gli angoli della bocca cadenti. Disastroso.
Commento: «Mi osservi con gli angoli della bocca cadenti» : ciò non è davvero onorevole.
Linea 1 - Trascurando la vostra zuppa di tartaruga, mi guardate mangiare
Avete tutto ciò di cui avete bisogno, eppure guardate gli altri con avidità e invidia. Potreste rinunciare al nutrimento spirituale per la cupidigia terrena.
Linea 2 - Yin (6): Volgersi verso il basso per cercare nutrimento. [1] Deviare dalla norma per cercare nutrimento dal colle. [2] Per un'impresa: disastroso.
Commento: La seconda linea morbida è «disastrosa per un'impresa», perché si procede senza associati.
Linea 2 - Se mangiate fino a ingozzarvi, vi prenderete una botta sulla schiena
Siete avidi e fate del male a voi stessi. Non conoscete limiti. Non vi accorgete quando la misura è colma.
Linea 3 - Yin (6): Deviare per cercare nutrimento. [3] Oracolo disastroso. Per dieci anni non farne uso. Nulla che sia propizio.
Commento: «Per dieci anni non farne uso», perché contrasta troppo con la retta via.
Linea 3 - Uno schiaffo sul viso
Mangiare cibo scadente non gratifica, così si è indotti a mangiare di nuovo. È l'avidità che vi ha portato a questo. Rompete le abitudini. Che cosa vi procurerà una soddisfazione durevole?
Linea 4 - Yin (6): Volgersi verso il basso per cercare nutrimento. [1] Fausto. Una tigre fissa, in agguato; la sua brama è insaziabile. Nessuna sfortuna.
Commento: Il responso «fausto» del «volgersi verso il basso per cercare nutrimento» dipende dal fatto che la linea superiore diffonde splendore.
Linea 4 - Vi riempite la bocca, la tigre guarda oltre di voi
È il momento adatto per soddisfare esigenze e desideri. La tigre simboleggia l'intensità delle vostre passioni. Sapete come evitare il pericolo della cupidigia e del desiderio.
Linea 5 - Yin (6): Deviare dalla norma. [2] Oracolo fausto per chi rimane a casa. Non bisogna attraversare il grande fiume.
Commento: «Oracolo fausto per chi rimane a casa»: perché la linea segue devotamente la linea superiore.
Linea 5 - Abbandono della via prestabilita
Anche se il potere è vostro, accettate l'influenza di coloro che ammirate. Potreste trovarvi di fronte alla necessità di agire in maniera poco ortodossa. Non fate gesti eclatanti.
Linea 6 - Yang (9): Origine del nutrimento. Pericoloso. Fausto. Propizio attraversare il grande fiume.
Commento: «Origine del nutrimento; pericoloso, ma fausto»: ci sarà una grande e buona fortuna.
Linea 6 - Essere fonte di nutrimento può essere una fortuna e un pericolo
Siete la fonte di nutrimento. O è la vostra saggezza a nutrire il prossimo, oppure siete un agnello sacrificale. Una volta esaurito il vostro compito sarete liberi di dedicarvi a qualcosa di nuovo.
				 */
			/// </summary>
			public class TheNourishment : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Tu metti da parte il tuo guscio sacro di tartaruga e mi osservi con gli angoli della bocca cadenti. Disastroso.",
							"Volgersi verso il basso per cercare nutrimento. [1] Deviare dalla norma per cercare nutrimento dal colle. [2] Per un'impresa: disastroso.",
							"Deviare per cercare nutrimento. [3] Oracolo disastroso. Per dieci anni non farne uso. Nulla che sia propizio.",
							"Volgersi verso il basso per cercare nutrimento. [1] Fausto. Una tigre fissa, in agguato; la sua brama è insaziabile. Nessuna sfortuna.",
							"Deviare dalla norma. [2] Oracolo fausto per chi rimane a casa. Non bisogna attraversare il grande fiume.",
							"Origine del nutrimento. Pericoloso. Fausto. Propizio attraversare il grande fiume.",
						};
					}
					
				}
				public override int Id => 27;
				public override string Sentency { get; set; } = "Il Nutrimento. Oracolo fausto. Osserva il nutrimento e presta attenzione a ciò che è nella tua bocca.";

				public override string Comment => "Il Nutrimento. «Oracolo fausto»: nutrire ciò che è corretto, è fausto. «Osserva il nutrimento», cioè osserva quali sono le cose che un uomo nutre. «Presta attenzione a ciò che è nella tua bocca»: cioè, osserva da che cosa un uomo è nutrito. Il cielo e la terra nutrono tutti gli esseri. I saggi nutrono i meritevoli e, attraverso di loro, le masse popolari. Il tempo del Nutrimento riveste grande importanza.";
				public override string Image => "Il Monte con il Tuono sotto: questa è l'immagine del Nutrimento. Ispirandosi ad essa, il signore è prudente nel parlare e si frena nel bere e nel mangiare.";
				public override string Series => "Le cose che sono domate devono essere nutrite. Per questo segue il segno: gli Angoli della bocca. Gli angoli della bocca significano: nutrimento.";
				public override string MixedSign => "Gli Angoli della bocca significano il nutrimento di ciò che è retto.";
				public override string AddictionalSentence => "";
			}
			/// <summary>
			/// 28
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yin (6): Per una stuoia cerimoniale usare erba bianca. Nessuna sfortuna.
Commento: «Per una stuoia cerimoniale usare erba bianca»: una linea morbida [umile] si trova in basso.
Linea 1 - Una stuoia bianca sotto all'offerta
Approntate con cura un luogo soffice dove depositare un carico pesante. A contatto con grandi forze siate deferenti e flessibili all'eccesso.
Linea 2 - Yang (9): Un salice appassito produce nuove gemme. Un uomo anziano prende una moglie giovane. Nulla che non sia propizio.
Commento: «Uomo anziano, moglie giovane»: sono reciprocamente inadatti [linea dura al secondo posto].
Linea 2 - Il salice avvizzito getta germogli, l'uomo anziano trova una moglie giovane
Un'unione insolita ma feconda di due estremi; la forza equilibrata dalla grazia. Una nuova prospettiva di vita. Un'azione di equilibrio.
Linea 3 - Yang (9): La trave maestra si incurva. Disastroso.
Commento: «La trave maestra che si incurva» rappresenta un segno nefasto, perché non sarà in grado di sostenere.
Linea 3 - La trave del tetto cede fino quasi a crollare
Non potete sopportare la pressione. È una sventura per voi. Cercate aiuto.
Linea 4 - Yang (9): La trave maestra si curva in alto (= resiste). Fausto. Ci sarà un'inaspettata calamità e umiliazione. [1]
Commento: «La trave maestra si curva in alto» è segno fausto, perché non si piega verso il basso.
Linea 4 - La trave del tetto viene rinforzata
Avete la forza e la solidità per sopportare la pressione. Potreste esservi lasciati alle spalle con successo una situazione disagevole ed eccessiva. Non aumentate il fardello.
Linea 5 - Yang (9): Un salice appassito produce nuovi fiori. Una donna anziana prende un marito giovane. Nessuna sfortuna, nessuna lode.
Commento: «Un salice appassito produce nuovi fiori»: come potrebbe ciò durare a lungo?. «Donna anziana e marito giovane»: ma è una vergogna ugualmente.
Linea 5 - Il salice avvizzito produce fiori, la donna anziana trova un uomo giovane
Un'operazione di cosmesi che non pone rimedio alla vera situazione. Un'esplosione di gloria inutile. Potreste assumere il controllo di qualcosa non più in fase di splendore. Non durerà a lungo.
Linea 6 - Yin (6): Superando un fiume la testa è sommersa dall'acqua. Disastroso. Nessuna sfortuna.
Commento: «Superare un fiume» è un segno infausto, ma non ci sarà alcuna sfortuna.
Linea 6 - Sopra la testa
Vi state addentrando troppo, sopravvalutando le vostre capacità. Andare aldilà di ciò che è ragionevole provoca disastri. Buone intenzioni potrebbero essersi tramutate in illusioni di grandezza.
				 */
			/// </summary>
			public class TheGreatExceed : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Per una stuoia cerimoniale usare erba bianca. Nessuna sfortuna.",
							"Un salice appassito produce nuove gemme. Un uomo anziano prende una moglie giovane. Nulla che non sia propizio.",
							"La trave maestra si incurva. Disastroso.",
							"La trave maestra si curva in alto (= resiste). Fausto. Ci sarà un'inaspettata calamità e umiliazione. [1]",
							"Un salice appassito produce nuovi fiori. Una donna anziana prende un marito giovane. Nessuna sfortuna, nessuna lode.",
							"Superando un fiume la testa è sommersa dall'acqua. Disastroso. Nessuna sfortuna.",
						};
					}
					
				}
				public override int Id => 28;
				public override string Sentency { get; set; } = "La Preponderanza del grande. La trave maestra si incurva. Propizio quando c'è un luogo in cui andare. Riuscita.";

				public override string Comment => "La Preponderanza del grande: il grande è eccessivo. «La trave maestra si incurva», perché le due linee estreme sono deboli [morbide] e c'è eccessiva forza nel mezzo [quattro linee dure, due delle quali centrali]. Il Mite e il \"gioioso\" sono attivi. «Propizio quando c'è un luogo in cui andare; riuscita». Il tempo della Preponderanza del grande riveste grande importanza.";
				public override string Image => "Il Lago sommerge gli Alberi (=Legno): questa è l'immagine della Preponderanza del grande. Ispirandosi ad essa, il signore riamane da solo e non teme nulla, si ritira dal mondo, senza alcuna preoccupazione.";
				public override string Series => "Senza nutrimento non ci si può muovere. Per questo segue il segno: la Preponderanza del grande.";
				public override string MixedSign => "La Preponderanza del grande è l'eccesso.";
				public override string AddictionalSentence => "Nella più remota antichità si componevano le salme coprendole di uno spesso strato di ramoscelli secchi e seppellendole poi in mezzo alla campagna, senza tumuli né boschetti. Il lutto non aveva una durata fissa. I santi dei tempi posteriori introdussero invece l'uso di bare e sarcofaghi. Questo lo trassero certamente dal segno: la Preponderanza del grande.";
			}
			/// <summary>
			/// 29
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yin (6): Un abisso dentro un abisso. Si cade in una fossa dentro l'abisso. Disastroso.
Commento: «Un abisso dentro un abisso; si cade dentro la fossa»: perdere la via è disastroso.
Linea 1 - Cadere dentro la fossa, sciagura
Siete caduti in un luogo pieno di pericoli; una fossa dentro la fossa. La mancanza di familiarità potrebbe avervi indotto a sovrastimare le vostra capacità e a commettere una serie di errori di disattenzione.
Linea 2 - Yang (9): Nell'abisso vi è pericolo. Cercare solo piccoli successi.
Commento: «Cercare solo piccoli successi», purché non ci si allontani dal centro.
Linea 2 - Circondato da fosse, piccole vincite
Per il momento siete salvi, ma siete circondati dal pericolo. Con cautela cercate di migliorare la vostra condizione aggirando gli ostacoli che non potete superare.
Linea 3 - Yin (6): Avanti e indietro, abisso sopra abisso. Pericolo; fermarsi per ora. Si è già caduti in una fossa dentro l'abisso. Non agire così.
Commento: «Avanti e indietro, abisso sopra abisso»: alla fine non ci sarà nessun risultato positivo.
Linea 3 - Cadere dentro la fossa, rimanere immobile
Non potete fare nulla riguardo alla vostra situazione di pericolo, per cui non fate nulla. Avete convissuto tanto tempo con il pericolo che ora vi siete abituati alla sua presenza.
Linea 4 - Yin (6): Una brocca di vino, una ciotola di riso come aggiunta. Sceglierli fatti di terracotta, offrirli semplicemente attraverso una finestra. Alla fine nessun danno.
Commento: «Un bicchiere di vino, una ciotola di riso come aggiunta»: è una linea di confine tra linee dure e morbide.
Linea 4 - Vino e grano calati con la corda
L'aiuto vi giunge nell'ora del bisogno. Alla fine non subirete danni. Fate economia nei periodi di crisi. Sarà sufficiente un piccolo gesto sincero.
 
Linea 5 - Yang (9): Abisso non completamente riempito, soltanto spianato. Nessuna sfortuna.
Commento: «L'abisso non è completamente riempito», perché la linea è centrale ma non ancora grande.
Linea 5 - La terra finisce prima che la fossa sia riempita
I vostri guai finiranno poco prima di sopraffarvi e distruggervi. Non sarete seppelliti, ma per poco.
Linea 6 - Yin (6): Legato con corde e funi, confinato in una foresta di spine, per tre anni non trovato. Disastroso.
Commento: La linea morbida in alto ha perso la via: il «disastro» dura «tre anni».
Linea 6 - Chiuso in una prigione recintata di spine
Cercando di uscire dai guai avete peggiorato le cose. Non potevate fare da solo. Non è una bella situazione. Non c'è via di fuga, ma sopravviverete.
				 */
			/// </summary>
			public class TheAbyssal : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Un abisso dentro un abisso. Si cade in una fossa dentro l'abisso. Disastroso.",
							"Nell'abisso vi è pericolo. Cercare solo piccoli successi.",
							"Avanti e indietro, abisso sopra abisso. Pericolo; fermarsi per ora. Si è già caduti in una fossa dentro l'abisso. Non agire così.",
							"Una brocca di vino, una ciotola di riso come aggiunta. Sceglierli fatti di terracotta, offrirli semplicemente attraverso una finestra. Alla fine nessun danno.",
							"Abisso non completamente riempito, soltanto spianato. Nessuna sfortuna.",
							"Legato con corde e funi, confinato in una foresta di spine, per tre anni non trovato. Disastroso.",
						};
					}
					
				}
				public override int Id => 29;
				public override string Sentency { get; set; } = "L'Abissale. Essere sinceri. Legati saldamente al cuore. Riuscita. Le azioni portano ricompense.";

				public override string Comment => "L'Abissale: «un abisso dentro un abisso» è il raddoppiamento del trigramma \"pericoloso\". L'Acqua [simboleggiata da questo trigramma] scorre senza mai accumularsi in alcun luogo: l'azione è \"pericolosa\", ma non si deve perdere la fiducia. «Riuscita nel cuore»  perché una linea dura è centrale [in entrambi i trigrammi]. «Le azioni portano ricompense», perché il procedere porterà buoni risultati. Il pericolo del cielo sta nel fatto che non si può scalarlo. Il pericolo della terra sono i monti e i fiumi, i colli e le alture. I Re e i Principi utilizzano questi luoghi pericolosi come barriere per difendere il proprio Paese. Il tempo per utilizzare l'esagramma \"pericoloso\" riveste grande importanza.";
				public override string Image => "L'Acqua scorre ininterrottamente: questa è l'immagine dell'Abissale. Ispirandosi ad essa, il signore compie continuamente azioni virtuose e esercita l'arte di educare la gente.";
				public override string Series => "Le cose non possono essere durevolmente in stato di preponderanza. Per questo segue il segno: l'Abissale. L'Abissale significa una fossa.";
				public override string MixedSign => "L'abissale è diretto verso il basso.";
				public override string AddictionalSentence => "";
			}
			/// <summary>
			/// 30
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yang (9): Camminare con cautela. Mostrare rispetto. Nessuna sfortuna.
Commento: Il «rispetto» nel «camminare con cautela» serve ad evitare una sfortuna.
Linea 1 - Ai piedi scarpe d'oro
È la prima volta che partecipate. I primi passi tendono a essere goffi e impulsivi; siate sensibili e perseguite l'adattamento. Potreste incontrare una persona notevole.
Linea 2 - Yin (6): Luminosità gialla. Totalmente fausto.
Commento: «Luminosità gialla; totalmente fausto»: poiché si è ottenuta la via centrale.
Linea 2 - Un bagliore dorato, molto fortunati
Il vostro ambiente vi è favorevole, di supporto. Percorrete la strada dell'equilibrio, lontana dalle azioni estreme. Vedete le cose con chiarezza e nella giusta prospettiva.
Linea 3 - Yang (9): Luminosità al tramonto. Non si batte sul tamburo né si canta, e gli anziani si lamentano. Disastroso.
Commento: «Luminosità al tramonto»: come può ciò durare a lungo?
Linea 3 - Al tramonto del sole, battete i tamburi o sospirate con dolore
Prima o poi ogni cosa finisce. Potete accettare la realtà con un sorriso, oppure lasciarvi deprimere. Ciò su cui avete fatto affidamento potrebbe non essere più adatto a voi.
Linea 4 - Yang (9): Subitanea è la sua venuta: divampa, muore, viene gettato via.
Commento: «Subitanea è la sua venuta»: non c'è luogo che lo possa supportare.
Linea 4 - Il fuoco divampa, arde, si affievolisce e in breve muore
Uno scoppio improvviso di entusiasmo senza un successivo impegno sui tempi lunghi, non durerà molto. Fuoco di paglia. Forse tenderete a saltare da una cosa all'altra.
Linea 5 - Yin (6): Lacrime a fiumi. Sospiri e lamenti. Fausto.
Commento: «Fausto» benché la quinta linea sia morbida:  in quanto aderisce alla posizione del Re e del Principe.
Linea 5 - Lacrime recano buona salute
Il dolore e il pentimento per gli errori commessi sono dimostrazione della vostra sincerità. Farete del vostro meglio, nonostante la capacità limitata di rendervi utili. Potreste aver perso, o aver sorpassato il vostro sistema di supporto.
Linea 6 - Yang (9): Il Re usa questo per spedizioni punitive. C'è un trionfo: si uccide il capo e si catturano i seguaci. Nessuna sfortuna.
Commento: «Il Re usa questo per una spedizioni punitive», allo scopo di portare ordine nel Paese.
Linea 6 - Marciate e catturate i nemici
È la vostra opportunità, una volta per tutte, di scovare l'origine delle influenze distruttive. Una volta isolata la causa del problema non è necessario un intervento su vasta scala.
				 */
			/// </summary>
			public class TheBrightness : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Camminare con cautela. Mostrare rispetto. Nessuna sfortuna.",
							"Luminosità gialla. Totalmente fausto.",
							"Luminosità al tramonto. Non si batte sul tamburo né si canta, e gli anziani si lamentano. Disastroso.",
							"Subitanea è la sua venuta: divampa, muore, viene gettato via.",
							"Lacrime a fiumi. Sospiri e lamenti. Fausto.",
							"Il Re usa questo per spedizioni punitive. C'è un trionfo: si uccide il capo e si catturano i seguaci. Nessuna sfortuna.",
						};
					}
					
				}
				public override int Id => 30;
				public override string Sentency { get; set; } = "L'Aderente. Propizio oracolo. Riuscita. Fausto per allevare mucche.";

				public override string Comment => "L'Aderente significa: essere attaccati. Il Sole e la Luna aderiscono al cielo; i cento cereali, le erbe e gli alberi aderiscono alla terra. La doppia luminosità [due trigrammi Fuoco] aderisce a ciò che è corretto, trasformando e perfezionando ogni cosa sotto il cielo. La linea morbida aderisce al centro e al luogo corretto [seconda linea dei due trigrammi]. Perciò si pronostica «riuscita» e  si dice: «fausto per allevare mucche».";
				public override string Image => "La Luce è raddoppiata: questa è l'immagine dell'Aderente. Ispirandosi ad essa, un grande uomo, continuando a diffondere chiarore, illumina le quattro regioni del mondo.";
				public override string Series => "In una fossa vi è certamente una sporgenza a cui attaccarsi. Per questo segue il segno: l'Aderente. L'Aderente significa: essere attaccati.";
				public override string MixedSign => "L'aderente è diretto verso l'alto.";
				public override string AddictionalSentence => "Fu Hsi fece delle corde annodate e le adoperò come reti e trappole per la caccia e la pesca. Questo lo trasse certamente dal segno: l'Aderente.";
			}
			/// <summary>
			/// 31
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yin (6): Stimolazione nell'alluce.
Commento: «Stimolazione nell'alluce»: la volontà è diretta verso il trigramma esterno.
Linea 1 - Agitare le dita dei piedi
Quando le persone agitano i piedi significa che sono impazienti di rimettersi in movimento, di dedicare l'attenzione a qualcos'altro. Non siate troppo frettolosi nel reagire a questo primo stadio. Potrebbe essere soltanto l'inizio di grandi cose.
Linea 2 - Yin (6): Stimolazione nei polpacci. Disastroso. Fausto per chi rimane a casa.
Commento: «Disastroso; ma fausto per chi rimane a casa»: essere devoti e non si patisce danno.
Linea 2 - Polpacci irrequieti
Resistete all'impulso di buttarvi in avanti. Vi siete lasciati coinvolgere troppo presto. Potreste avere la sensazione di essere stati spinti in qualcosa per cui non eravate ancora pronti. Potrete muovervi solo quando sarete invitati a farlo.
Linea 3 - Yang (9): Stimolazione nelle cosce.  Prende ciò che segue. [1]Procedere conduce a umiliazione.
Commento: «Stimolazione nelle cosce»:  cioè incapace di stare quieto; la volontà è diretta verso la  «seguente [linea morbida] presa» sotto.
Linea 3 - Muovere un passo, afferrato per i calcagni
Sentite il desiderio di agire senza sapere esattamente cosa volete. Le circostanze potrebbero trattenervi. Imparate a camminare prima di correre.
Linea 4 - Yang (9): Oracolo fausto. Il rimpianto svanisce. Ti muovi avanti e indietro, esitando. I tuoi amici seguono i tuoi pensieri.
Commento: «Oracolo fausto; il rimpianto svanisce»: non c'è nulla di dannoso nel muoversi. «Ti muovi avanti e indietro, esitando»: tuttavia, nessun grande splendore.
Linea 4 - Gli amici reagiscono alle vostre preoccupazioni
Siete immersi nell'attività e nella confusione. Non perdete la concentrazione o la sensibilità nei confronti del problema fondamentale. I vostri sentimenti per gli altri li ispireranno a venirvi in aiuto.
 
Linea 5 - Yang (9): Stimolazione nella schiena. Nessun rimpianto.
Commento: «Stimolazione nella schiena»: la volontà è diretta alle estremità, centrale e corretta.
Linea 5 - Un dolore alla nuca
Rimanete saldi nei vostri principi e nella vostra fede. Non lasciatevi trascinare dagli altri, anche se ciò dovesse portare alla rottura di un rapporto. Senza rimpianti, non perdete di vista la ricompensa.
Linea 6 - Yin (6): Stimolazione nelle guance, nelle mascelle, nella lingua.
Commento: «Stimolazione nelle guance, nelle mascelle, nella lingua», perché apre la bocca e chiacchiera.
Linea 6 - Usare la bocca
Avete il dono della persuasione, usatelo per il bene di tutti. Resistete alla tentazione di adulare il prossimo. Fate attenzione ai discorsi sbadati e crudeli.
				 */
			/// </summary>
			public class TheMutualInfluence : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Stimolazione nell'alluce.",
							"Stimolazione nei polpacci. Disastroso. Fausto per chi rimane a casa.",
							"Stimolazione nelle cosce.  Prende ciò che segue. [1]Procedere conduce a umiliazione.",
							"Oracolo fausto. Il rimpianto svanisce. Ti muovi avanti e indietro, esitando. I tuoi amici seguono i tuoi pensieri.",
							"Stimolazione nella schiena. Nessun rimpianto.",
							"Stimolazione nelle guance, nelle mascelle, nella lingua.",
						};
					}
					
				}
				public override int Id => 31;
				public override string Sentency { get; set; } = "L'Attrazione. Riuscita. Propizio oracolo per prendere una moglie. Fausto.";

				public override string Comment => "L'Attrazione vuol dire: influsso reciproco. Un trigramma cedevole sopra e un trigramma forte sotto; le due energie si attraggono e si rispondono, così che si uniscono. [Esse sono] il \"quieto\" e il \"gioioso\". Un trigramma maschile [il figlio minore] che si pone sotto un trigramma femminile [la figlia minore] significa «riuscita; propizio oracolo per prendere una moglie; fausto». Il cielo e la terra si attraggono e tutte le creature prendono forma e vengono all'essere. I santi influiscono sul cuore della gente e il tutto il mondo giunge a pace e quiete. Osservando questi fenomeni di attrazione, possiamo comprendere la natura di tutti gli esseri nel cielo e sulla terra.";
				public override string Image => "Il Monte con il Lago sopra: questa è l'immagine dell'Attrazione. Ispirandosi ad essa, il signore apre il suo cuore e la sua mente, facendo sì che gli uomini gli si avvicinino.";
				public override string Series => "Dopo che vi sono cielo e terra vi sono i singoli esseri. Dopo la comparsa dei singoli esseri vi sono i due sessi. Dopo che vi sono il maschile e il femminile vi è il rapporto tra marito e moglie. Dopo che esiste il rapporto tra marito e moglie vi è il rapporto tra padre e figlio. Dopo che esiste il rapporto tra padre e figlio vi è il rapporto tra principe e servitore. Dopo che esiste il rapporto tra principe e servitore vi è la differenza tra alto e basso. Dopo che esiste la differenza tra alto e basso possono intervenire le regole dell'ordine e del diritto.";
				public override string MixedSign => "L'attrazione si compie rapidamente.";
				public override string AddictionalSentence => "";
			}
			/// <summary>
			/// 32
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yin (6): Ostinata durata. Oracolo di disastro. Nulla che sia propizio.
Commento: Il responso nefasto dell'«ostinata durata» deriva dal cercare troppa profondità all'inizio.
Linea 1 - Persistente con impazienza, non è bene
Siete determinati a prendere un impegno a lungo termine, prima ancora di sapere qual è la vostra meta. Pretendete troppo. Adattatevi a ciò che accade ora.
Linea 2 - Yang (9): Il rimpianto svanisce.
Commento: Con la seconda linea dura «il rimpianto svanisce» perchè è durevolmente nella posizione centrale.
Linea 2 - Niente più pentimenti
Lasciandovi alle spalle il passato e ogni rimpianto, siete liberi di concentrarvi sul futuro. Le circostanze potrebbero impedire il pieno dispiegamento del vostro talento. Non attuate azioni estreme; siate calmi e adattabili.
Linea 3 - Yang (9): Non conferire durata alla propria virtù porterà al fallimento. Oracolo di umiliazione.
Commento: «Non conferire durata alla propria virtù» perché non è supportato.
Linea 3 - Non restate al vostro posto
Se rispettate i vostri obblighi riscuoterete fiducia. Potreste essere tentati di agire oltre le vostre possibilità per trovarvi da qualche altra parte, oppure essere tormentati dall'indecisione e dall'insoddisfazione.
Linea 4 - Yang (9): Caccia senza selvaggina.
Commento: E' durevolmente fuori dalla propria posizione [linea yang in posizione yin]:  come si potrebbe avere la preda?
Linea 4 - Non cattura nulla durante la caccia
Vi state occupando di qualcosa in una maniera completamente sbagliata. Potreste essere impazienti di progredire, ma, per prima cosa, dovete sapere che cosa volete e dove cercare.
Linea 5 - Yin (6): Conferire durata alla propria virtù . Oracolo fausto per una moglie, disastroso per un marito.
Commento: «Oracolo fausto per una moglie» perché segue un uomo per tutta la vita. Il «marito» dovrebbe prendere decisioni: seguire sua moglie sarebbe «disastroso».
Linea 5 - Obbedienza leale, salutare per i seguaci, pessima per i capi
Se avete il comando, accettate la responsabilità mostrando aggressività e adattabilità. Se non avete il comando è giusto che seguiate le decisioni di coloro che ce l'hanno. A quale categoria appartenete?
Linea 6 - Yin (6): Irrequieta durata. Disastroso.
Commento: «Irrequieta durata» nella posizione più elevata ottiene grande fallimento.
Linea 6 - L'irrequietezza come stato persistente porta sciagura
L'unica cosa costante è l'irrequietezza nella vostra mente. La fretta e l'impazienza vi rende vulnerabili agli attacchi e alla sventura. Rilassatevi e calmate il vostro cuore inquieto.
				 */
			/// </summary>
			public class TheDuration : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Ostinata durata. Oracolo di disastro. Nulla che sia propizio.",
							"Il rimpianto svanisce.",
							"Non conferire durata alla propria virtù porterà al fallimento. Oracolo di umiliazione.",
							"Caccia senza selvaggina.",
							"Conferire durata alla propria virtù . Oracolo fausto per una moglie, disastroso per un marito.",
							"Irrequieta durata. Disastroso.",
						};
					}
					
				}
				public override int Id => 32;
				public override string Sentency { get; set; } = "La Durata. Riuscita. Nessuna sfortuna. Propizio oracolo. Propizio quando c'è un luogo in cui andare.";

				public override string Comment => "La Durata significa: ciò che resiste a lungo. Un trigramma forte sopra e un trigramma cedevole sotto;  il Tuono e il Vento operano insieme; il Mite e il \"mobile\", con linee morbide e dure che corrispondono tutte tra loro: questa è la Durata. «Durata. Riuscita. Nessuna sfortuna. Propizio oracolo», questo significa costanza nel seguire la giusta via. La legge del cielo e della terra è durevole, lunga e non cessa mai. «Propizio quando c'è un luogo in cui andare »: ogni fine deve essere seguita da un nuovo inizio. Il sole e la luna, rispettando la legge celeste, riescono a risplendere durevolmente. Le quattro stagioni, cambiando di continuo, si susseguono costantemente. Gli uomini saggi rimangono costanti nel seguire la giusta via così che tutte le cose nel mondo sono trasformate fino al compimento. Osservando la legge della durata, possiamo comprendere la natura di tutti gli esseri nel cielo e sulla terra.";
				public override string Image => "Il Tuono e il Vento: questa è l'immagine della Durata. Ispirandosi ad essa, il signore rimane saldo, senza cambiare mai direzione.";
				public override string Series => "La via del marito e della moglie deve essere lunga e continua. Per questo segue il segno: la Durata. Durata significa: resistente a lungo.";
				public override string MixedSign => "La Durata significa ciò che resiste a lungo.";
				public override string AddictionalSentence => "La Durata produce la fermezza della virtù. Il segno della Durata mostra molteplici esperienze senza tedio. Il segno della Durata produce l'unità della virtù.";
			}
			/// <summary>
			/// 33
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yin (6): La coda del maiale (= ritirata in coda). [1] Pericoloso. Non adatto quando c'è un luogo in cui andare.
Commento: «Pericolo» nella «coda del maiale»: se non si va da nessuna parte come potrebbe essere pericoloso?
Linea 1 - Il maialino scappa
Arretrate di fronte al pericolo non appena lo percepite. Non siete in grado di fronteggiarlo. Non esponete nulla che gli altri possano aggredire.
Linea 2 - Yin (6): Legarlo con una pelle di bue giallo. Non potrà liberarsi.
Commento: «Legarlo con una pelle di bue giallo» per affermare la volontà.
Linea 2 - Legato con il cuoio giallo di bue, nessuno può slegarlo
Siete calmi e determinati, avete una fiducia incrollabile in un grande ideale o in una persona che ammirate. Questo legame dovrebbe esservi di conforto, non isolarvi.
Linea 3 - Yang (9): Legare il maiale (= ritirata interrotta). In una malattia: pericoloso. Fausto tenere servi e serve.
Commento: «Pericolo» nel «legare il maiale»:  c'è pericolo e affaticamento. «Fausto tenere servi e serve» ma non si possono adoperare per grandi cose.
Linea 3 - Il piccolo maiale è legato
Anche contro la vostra volontà può accadere che dobbiate ritirarvi da una situazione che reputavate sicura ma che ora non lo è più. Tornate alla base e concentratevi su ciò che conta veramente.
Linea 4 - Yang (9): Un bel maiale (= ritirata volontaria). Per un signore, fausto. Per una persona meschina, problemi.
Commento: «Per un signore» «un bel maiale»; per una persona meschina, ciò causa problemi.
Linea 4 - Solo una grande persona può offrire un piccolo maiale.
Togliersi dalla confusione per concentrarsi su un obiettivo specifico non è semplice come sembra. Se non sono queste le vostre intenzioni, preparatevi a fare ciò che è necessario.
Linea 5 - Yang (9): Un maiale lodato (= ritirata eccellente). Oracolo fausto.
Commento: «Un maiale lodato; oracolo fausto» a causa della corretta [posizione] per la volontà.
Linea 5 - Il piccolo maiale viene ricompensato per avercela fatta
Avete portato a termine ciò che era nei vostri intenti. Congratulazioni. Lasciate il gioco finché siete in testa. La capacità di andarsene quando la festa è finita è un'eccellente qualità.
Linea 6 - Yang (9): Un maiale paffuto (= ritirata feconda). Nulla che non sia propizio.
Commento: «Un maiale paffuto; nulla che non sia propizio» perché non c'è posto per il dubbio.
Linea 6 - Piccolo maiale, grasso e felice
Avete fatto del vostro meglio ottenendo il successo. Adesso potete ritirarvi dalla lotta della vita e stare in pace.
				 */
			/// </summary>
			public class TheRetreat : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"La coda del maiale (= ritirata in coda). [1] Pericoloso. Non adatto quando c'è un luogo in cui andare.",
							"Legarlo con una pelle di bue giallo. Non potrà liberarsi.",
							"Legare il maiale (= ritirata interrotta). In una malattia: pericoloso. Fausto tenere servi e serve.",
							"Un bel maiale (= ritirata volontaria). Per un signore, fausto. Per una persona meschina, problemi.",
							"Un maiale lodato (= ritirata eccellente). Oracolo fausto.",
							"Piccolo maiale, grasso e felice",
						};
					}
					
				}
				public override int Id => 33;
				public override string Sentency { get; set; } = "La Ritirata. Riuscita. Propizio oracolo per le piccole cose.";

				public override string Comment => "La Ritirata. «Riuscita»: questo significa che la riuscita sta nella ritirata. La linea dura [quinta linea] si trova nella posizione approriata e trova corrispondenza [con la seconda linea], procedendo in armonia col tempo. È un responso che ha grande attualità. «Propizio oracolo per le piccole cose»: perché [le linee morbide] stanno avanzando e crescendo [dal basso]. Il tempo della Ritirata riveste grande importanza.";
				public override string Image => "Il Cielo con il Monte sotto: questa è l'immagine della Ritirata. Ispirandosi ad essa, il signore si allontana dalle persone meschine, non con astio ma con dignità.";
				public override string Series => "Le cose non possono dimorare durevolmente al loro posto. Per questo segue il segno: la Ritirata. La Ritirata significa retrocedere.";
				public override string MixedSign => "La Ritirata significa retrocedere.";
				public override string AddictionalSentence => "";
			}
			/// <summary>
			/// 34
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yang (9): Potenza nei piedi. Imprendere qualcosa è disastroso. Essere sinceri.
Commento: «Potenza nei piedi»: la sua sincerità è alla fine.
Linea 1 - Piedi irrequieti, continuare reca sciagura
Siete impazienti, ma non ancora pronti per l'avventura. Non siete ancora abbastanza forti per conquistarvi la libertà. Se cominciate adesso, presto sarete esausti. Prendete tempo.
Linea 2 - Yang (9): Oracolo fausto. [1]
Commento: La seconda linea dura pronostica un «oracolo fausto» perché è centrale.
Linea 2 - Leale e perseverante
Se siete in svantaggio, continuate a essere leali e perseveranti, così potrete liberarvi delle circostanze che vi ostacolano. Accettate aiuto. La forza equilibrata dalla padronanza di sé.
Linea 3 - Yang (9): Le persone meschine agiscono mediante la potenza, il signore non agisce così. Oracolo pericoloso. Un capro cozza contro una siepe e s'impiglia le corna.
Commento: «Le persone meschine ricorrono alla potenza»; questo un signore non lo fa.
Linea 3 - Un ariete carica il recinto e vi rimane impigliato con le corna
Mostrando i muscoli non otterrete il vostro bene. Renderete soltanto la situazione più complicata. Avrete l'opportunità di trattenervi dall'usare la forza per ottenere ciò che volete.
Linea 4 - Yang (9): Oracolo fausto. Il rimpianto svanisce. La siepe si rompe, non si è più impigliati. La potenza poggia sull'asse di un grande carro.
Commento: «La siepe si rompe, non si è più impigliati»: egli può procedere.
Linea 4 - Un ariete carica il recinto e si libera
È la vostra opportunità. Siete forti e le barriere sono deboli. Usate la vostra autorità con saggezza e mantenetene il controllo.
 
Linea 5 - Yin (6): Perdere una capra nel campo. Nessun rimpianto.
Commento: «Perdere una capra nel campo», perché [la posizione] non è adeguata [alla linea].
Linea 5 - Liberare l'ariete
Se qualcuno vuole la sua libertà, lasciatelo andare senza rimpianti. Lasciatelo fare a modo suo. Potete facilmente avere la meglio sulla vostra natura testarda. Non avete bisogno di forzare la situazione.
Linea 6 - Yin (6): Un capro cozza contro una siepe: non può tornare indietro, non può andare avanti. Nulla è propizio. Nelle difficoltà, fausto.
Commento: «Non può tornare indietro, non può andare avanti»: non è un buon presagio. «Nelle difficoltà, fausto»: la sfortuna non durerà a lungo.
Linea 6 - Le corna incastrate nel recinto
L'impiego della forza bruta vi renderà impopolari e voi rimarrete incastrati. Rilassatevi, la situazione migliorerà da sé. Usate la grazia e otterrete i risultati desiderati.
				 */
			/// </summary>
			public class TheGreatPower : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Potenza nei piedi. Imprendere qualcosa è disastroso. Essere sinceri.",
							"Oracolo fausto. [1]",
							"Le persone meschine agiscono mediante la potenza, il signore non agisce così. Oracolo pericoloso. Un capro cozza contro una siepe e s'impiglia le corna.",
							"Oracolo fausto. Il rimpianto svanisce. La siepe si rompe, non si è più impigliati. La potenza poggia sull'asse di un grande carro.",
							"Perdere una capra nel campo. Nessun rimpianto.",
							"Un capro cozza contro una siepe: non può tornare indietro, non può andare avanti. Nulla è propizio. Nelle difficoltà, fausto.",
						};
					}
					
				}
				public override int Id => 34;
				public override string Sentency { get; set; } = "La Grande potenza. Propizio oracolo.";

				public override string Comment => "La Grande potenza vuol dire: i grandi sono potenti. Una linea dura è nel \"mobile\" [trigramma superiore] e diventa forte. «La Grande potenza; propizio oracolo»: ciò che è grande è corretto. Essere corretto e grande significa che si può osservare la natura del cielo e della terra.";
				public override string Image => "Il Tuono sopra il Cielo: questa è l'immagine della Grande potenza. Ispirandosi ad essa, il signore non percorre sentieri che non corrispondano all'ordine stabilito.";
				public override string Series => "Le cose non possono sempre ritirarsi. Per questo segue: la Grande potenza.";
				public override string MixedSign => "La Grande potenza si mostra nel fermarsi.";
				public override string AddictionalSentence => "Nella più remota antichità gli uomini dimoravano in caverne e selve. I santi dei tempi posteriori cambiarono tutto questo passando alle costruzioni: in alto stava una trave maestra, da questa scendeva un tetto come riparo da vento e pioggia. Questo lo trassero certamente dal segno: la Grande potenza.";
			}
			/// <summary>
			/// 35
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yin (6): Progredire venendo respinto. Oracolo fausto. [Il rimpianto] svanisce. Essere sinceri: rimanere tranquilli. [2]  Nessuna sfortuna.
Commento: «Progredire venendo respinto»: bisogna procedere da solo, correttamente. «Rimanere tranquilli; nessuna sfortuna»: non si è stati chiamati.
Linea 1 - Le vostre proposte vengono respinte
La vostra posizione non è abbastanza forte perché possiate ottenere tutto ciò che volete senza l'aiuto di nessuno. Le vincite si mischiano con le perdite. Continuate a lavorarci e verrà il vostro momento. Dapprima il rifiuto, poi il successo.
Linea 2 - Yin (6): Progredire con apprensione. Oracolo fausto. Si riceve questa benedizione dalla propria antenata.
Commento: «Si riceve questa benedizione» per la posizione centrale e corretta.
Linea 2 - Dalla vostra ava vi giunge aiuto
Vi rattrista il fatto che alcuni colleghi invidiosi vi ostacolino il cammino. Non dovete preoccuparvi. Il vostro talento verrà notato da un capo illuminato che vi darà il suo appoggio. La pazienza vi porterà fin dove volete arrivare.
 
Linea 3 - Yin (6): Tutti ti appoggiano. Il rimpianto svanisce.
Commento: La volontà delle «persone che ti appoggiano» si muove verso l'alto.
Linea 3 - Tutti vi approvano, non vi sono dubbi
I dubbi si dissolvono. Vi siete guadagnati la fiducia della gente, che adesso approva le vostre decisioni. Avete un'indole flessibile e una volontà ferma.
Linea 4 - Yang (9): Progredire con le mani piegate come le zampe di un topo. Oracolo pericoloso.
Commento: «Mani piegate come le zampe di un topo; oracolo pericoloso», perché [la linea] non è adeguata [alla posizione].
Linea 4 - Avanzare come uno scoiattolo,  pericolo
Questo non è né il momento né il luogo per i grandi guadagni. Potreste essere tentati di prendere qualcosa di soppiatto che non vi appartiene. Agite in maniera franca e diretta, oppure non agite affatto.
 
Linea 5 - Yin (6): Il rimpianto svanisce. Perdere la preda. Non essere dispiaciuto. Per chi va avanti: fausto. Nulla che non sia propizio.
Commento: «Perdere la preda; non essere dispiaciuto»: procedere porterà grande e buona fortuna.
Linea 5 - Ciò che era perso viene riguadagnato
Anche se non foste in grado di prevedere con chiarezza l'esito, non preoccupatevi di vincere o perdere. Fate soltanto ciò che il vostro cuore vi dice di fare e avrete successo. Recupererete qualcosa che avevate perso.
Linea 6 - Yang (9): Progredire con le corna. Da servirsene solo per attaccare la città. Pericoloso. Fausto. Nessuna sfortuna. Oracolo di umiliazione.
Commento: «Da servirsene solo per attaccare la città», perché la via non è ancora splendida.
Linea 6 - Le lance pronte all'attacco, ideali per espugnare una città
Talvolta è necessaria un'azione aggressiva, soprattutto quando avete aspettato troppo per porre rimedio a qualcosa. Quando avrete raggiunto il vostro scopo riassumete un comportamento più pacifico.
				 */
			/// </summary>
			public class TheProgress : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Progredire venendo respinto. Oracolo fausto. [Il rimpianto] svanisce. Essere sinceri: rimanere tranquilli. [2]  Nessuna sfortuna.",
							"Progredire con apprensione. Oracolo fausto. Si riceve questa benedizione dalla propria antenata.",
							"Tutti ti appoggiano. Il rimpianto svanisce.",
							"Progredire con le mani piegate come le zampe di un topo. Oracolo pericoloso.",
							"Il rimpianto svanisce. Perdere la preda. Non essere dispiaciuto. Per chi va avanti: fausto. Nulla che non sia propizio.",
							"Progredire con le corna. Da servirsene solo per attaccare la città. Pericoloso. Fausto. Nessuna sfortuna. Oracolo di umiliazione.",
						};
					}
					
				}
				public override int Id => 35;
				public override string Sentency { get; set; } = "Il Progresso. Il marchese di Kang viene onorato con cavalli. Essi si riproducono. Li fa accoppiare tre volte in un giorno. [1]";

				public override string Comment => "Il Progresso vuol dire: andare avanti. Il trigramma della Luce emerge sopra  il trigramma della Terra. \"Devoto\" sotto \"luminoso\" dà grande Luce. Una linea morbida avanza e si muove verso il trigramma superiore. Perciò si dice: «Il marchese di Kang viene onorato con cavalli. Essi si riproducono. Egli viene ricevuto tre volte in un giorno».";
				public override string Image => "La Luce (= Fuoco) emerge sulla Terra: questa è l'immagine del Progresso. Ispirandosi ad essa, il signore da sé illumina la sua splendente virtù.";
				public override string Series => "Gli esseri non possono rimanere sempre nello stato di potenza. Per questo segue il segno: il Progresso. Progresso significa espansione.";
				public override string MixedSign => "Il Progresso significa il giorno.";
				public override string AddictionalSentence => "";
			}
			/// <summary>
			/// 36
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yang (9): L'uccello del fuoco che grida sta volando con le ali abbassate.  [1] Il signore in viaggio non mangia nulla per tre giorni. C'è un luogo in cui andare. L'oste sparla di lui.
Commento: «Il signore in viaggio» non deve mangiare.
Linea 1 - Danneggiato in volo, senza cibo per tre giorni
Soltanto voi notate i segni della decadenza e decidete di uscire in punta di piedi dalla situazione. Coloro che non si accorgono di nulla potrebbero trovare strano il vostro comportamento e criticarvi per il vostro abbandono.
Linea 2 - Yin (6): L'uccello del fuoco che grida è ferito nella coscia sinistra. Servirsi dell'aiuto di un forte cavallo. Fausto.
Commento: «Fausto» perché la seconda linea morbida mostra obbedienza alle regole.
Linea 2 - Ferito alla gamba sinistra, tratto in salvo con un cavallo robusto
Subite un danno, ma trovate modo di salvarvi. Siete in grado di proteggervi dagli attacchi e di trarvi in salvo. Danneggiato ma non impedito.
Linea 3 - Yang (9): L'uccello del fuoco che grida è colpito durante una caccia nel sud. Si cattura il grande capo. L'oracolo rapido non è possibile [che sia favorevole].
Commento: La volontà di «cacciare nel sud» ha grande successo.
Linea 3 - La cattura del capo dei ribelli
Avete il potere di affrontare e risolvere ciò che non va. Sebbene correggiate l'errore più grande, ci vorrà un po' di tempo prima che ogni cosa ritorni al suo posto. Il regno delle tenebre sta per finire.
Linea 4 - Yin (6): Penetrando nel fianco sinistro si trova il cuore dell'uccello del fuoco che grida. Abbandonare cancello e cortile.
Commento: «Penetrare nel fianco sinistro» significa trovare la vera intenzione del «cuore».
Linea 4 - Nel cuore delle tenebre
Siete a conoscenza di una brutta situazione poiché è lì, vicino a voi. Per fortuna non vi fate corrompere e potete salvarvi. La scoperta di piani segreti.
 
Linea 5 - Yin (6): L'uccello del fuoco che grida del principe Chi. Propizio oracolo.
Commento: «L'oracolo del principe Chi»: la luce non può mai essere fatta cessare.
Linea 5 - Una luce nell'oscurità
Avete rapporti stretti con le persone sbagliate. La cosa migliore che potete fare è rifiutarvi di prendere parte a un'ingiustizia. Mantenete viva la vostra luce interiore e rimanete nascosti. La luce può essere oscurata, non estinta.
Linea 6 - Yin (6): L'uccello del fuoco smette di gridare. Rimpianto. (= Non c'è luce. Solo oscurità). [2] Prima sale nel cielo, poi sprofonda nella terra.
Commento: «Prima sale nel cielo» per illuminare le contrade di tutte le quattro regioni. «Poi sprofonda nella terra», perdendo il suo ruolo.
Linea 6 -  Sale fino in cielo, poi cade a terra
L'orgoglio precede la caduta. I vostri motivi sono validi, ma potreste esservi spinti troppo in avanti. Ora che siete stati riportati a terra, potete imboccare la strada più modesta verso il successo.
				 */
			/// </summary>
			public class TheObtenebrationLight : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"L'uccello del fuoco che grida sta volando con le ali abbassate.  [1] Il signore in viaggio non mangia nulla per tre giorni. C'è un luogo in cui andare. L'oste sparla di lui.",
							"L'uccello del fuoco che grida è ferito nella coscia sinistra. Servirsi dell'aiuto di un forte cavallo. Fausto.",
							"L'uccello del fuoco che grida è colpito durante una caccia nel sud. Si cattura il grande capo. L'oracolo rapido non è possibile [che sia favorevole].",
							"Penetrando nel fianco sinistro si trova il cuore dell'uccello del fuoco che grida. Abbandonare cancello e cortile.",
							"L'uccello del fuoco che grida del principe Chi. Propizio oracolo.",
							"L'uccello del fuoco smette di gridare. Rimpianto. (= Non c'è luce. Solo oscurità). [2] Prima sale nel cielo, poi sprofonda nella terra.",
						};
					}
					
				}
				public override int Id => 36;
				public override string Sentency { get; set; } = "L'Ottenebramento della luce. Propizio oracolo nelle difficoltà.";

				public override string Comment => "L'Ottenebramento della luce. Il trigramma della Luce sprofonda sotto il trigramma della Terra: questo è l'Ottenebramento della luce. Il trigramma interno è \"luminoso\", quello esterno, con tutte linee morbide, è \"devoto\", come quando si soffrono grandi avversità. Questo successe al Re Wen. «Propizio oracolo nelle difficoltà», perché c'è luce velata dall'oscurità. La volontà può rimanere corretta nonostante le difficoltà interne: come mostrò il principe Chi.";
				public override string Image => "La Luce (= Fuoco) sprofonda nella Terra: questa è l'immagine dell'Ottenebramento della luce. Ispirandosi ad essa, il signore si avvicina alla gente: egli vela il suo splendore e rimane pur chiaro.";
				public override string Series => "Il progredire si imbatterà certamente in resistenza e danneggiamento. Per questo segue il segno: l'Ottenebramento della luce. Ottenebramento significa danneggiamento, lesione.";
				public override string MixedSign => "L'Ottenebramento della luce significa lesione.";
				public override string AddictionalSentence => "";
			}
			/// <summary>
			/// 37
			/*
			 * LINEE MOBILI	LINEE MOBILI
Linea 1 - Yang (9): Salda chiusura all'interno della famiglia. Il rimpianto svanisce.
Commento: «Salda chiusura all'interno della famiglia»: la volontà non è ancora mutata.
Linea 1 - Rimanete in casa e chiudete le porte
La casa è il luogo sicuro per voi, potete chiudere le porte davanti al mondo ostile. Decidete chi è della famiglia e chi non lo è. Chi è dentro e chi fuori?
Linea 2 - Yin (6): Non seguire il capriccio: rimanere in famiglia a provvedere per il cibo. Oracolo fausto.
Commento: Il responso «fausto» della seconda linea morbida deriva dall'essere devota al [la quinta linea nel] Mite (= il trigramma superiore).
Linea 2 - Rimanete in casa a provvedere ai pasti
Rimanete in casa o all'interno della struttura sociale. Non è il momento adatto per seguire da soli la vostra strada. In questo momento gli obblighi adombrano i vostri personali desideri.
Linea 3 - Yang (9): Nella famiglia si sgrida. Pericolo di rimpianto. Fausto. La donna e i bambini si trastullano e ridono: si finisce in umiliazione.
Commento: «Nella famiglia si sgrida»:  nulla è ancora perduto. «La donna e i bambini si trastullano», vuol dire che la disciplina della famiglia va perduta.
Linea 3 - Turbato dal rigore, pur sempre preferibile alla frivolezza umiliante
L'eccesso di disciplina in un gruppo potrebbe provocare risentimento, ma la sua assenza produce caos. L'accordo sulle regole, costantemente rinforzato, mantiene l'equilibrio.
Linea 4 - Yin (6): La famiglia si sta arricchendo. Molto fausto.
Commento: «La famiglia si sta arricchendo; molto fausto»: ella è devota [quarta linea] e sta nella [corretta] posizione.
Linea 4 - Lei è il tesoro della casa
Se anche una persona non si occupa direttamente delle cose, potrebbe essere ugualmente in grado di creare felicità e benessere per la famiglia.
Linea 5 - Yang (9): Il Re è presente nella famiglia. Non temere. Fausto.
Commento: «Il Re è presente nella famiglia»: i loro rapporti sono di amore reciproco.
Linea 5 - Il re è nel castello
Se ogni cosa è in buon ordine e l'armonia regna nelle relazioni domestiche, non avete nulla da nascondere e da temere. I membri di una famiglia, per loro natura, si amano e si perdonano vicendevolmente, creando una vita sicura e felice.
Linea 6 - Yang (9): Essere sinceri, meritando rispetto. Alla fine fausto.
Commento: Il responso «fausto» del «meritare rispetto», indica che prima si richiede molto alla propria persona.
Linea 6 - Meritare il rispetto
Potreste essere soli. Meritate il rispetto degli altri mostrandovi cordiali e disponibili ad aiutare. Siate sinceri e date il buon esempio. Fate assegnamento su voi stessi, non sugli altri.
			 */
			/// </summary>
			public class TheFamily : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Salda chiusura all'interno della famiglia. Il rimpianto svanisce.",
							"Non seguire il capriccio: rimanere in famiglia a provvedere per il cibo. Oracolo fausto.",
							"Nella famiglia si sgrida. Pericolo di rimpianto. Fausto. La donna e i bambini si trastullano e ridono: si finisce in umiliazione.",
							"La famiglia si sta arricchendo. Molto fausto.",
							"Il Re è presente nella famiglia. Non temere. Fausto.",
							"Essere sinceri, meritando rispetto. Alla fine fausto.",
						};
					}
					
				}
				public override int Id => 37;
				public override string Sentency { get; set; } = "La Famiglia. Propizio oracolo per una donna.";

				public override string Comment => "La Famiglia: la posizione giusta della donna è all'interno [della casa] [una linea yin è centrale nel trigramma inferiore], la posizione giusta dell'uomo è all'esterno [della casa] [una linea yang è centrale nel trigramma superiore]. L'uomo e la donna nella loro giusta posizione mostrano la grande volontà del cielo e della terra. Una famiglia ha dei capi rigorosi: sono il padre e la madre [la seconda e la quinta linea]. Quando il padre è realmente padre ed il figlio è figlio, quando il fratello maggiore è un fratello maggiore e il fratello minore è un fratello minore, il marito è marito e la moglie è moglie, allora la famiglia si trova sulla giusta via. Quando si ottiene ordine in ogni famiglia, il mondo si mantiene in una stabile condizione.";
				public override string Image => "Il Vento scaturisce dal Fuoco: questa è l'immagine della Famiglia. Ispirandosi ad essa, il signore è coerente nelle sue parole e costante nelle sue azioni.";
				public override string Series => "Chi viene offeso fuori si ritira nella sua famiglia. Per questo segue il segno: la Famiglia.";
				public override string MixedSign => "La Famiglia è l'interno.";
				public override string AddictionalSentence => "";
			}
			/// <summary>
			/// 38
			/*
			 * LINEE MOBILI	LINEE MOBILI
Linea 1 - Yang (9): Il rimpianto svanisce. Perdere cavalli. Non li rincorrere: torneranno da soli. Vedere un uomo malvagio. Nessuna sfortuna.
Commento: «Vedere un uomo malvagio», evitando la sfortuna.
Linea 1 - Il vostro cavallo ritorna di sua volontà
Non avete colpa della vostra sciagura. Includete gli antagonisti nel vostro mondo e affrontate le vostre paure più intime. I vostri guai seguiranno il loro corso e ciò che viene perso sarà recuperato.
Linea 2 - Yang (9): Incontrare il proprio signore in un vicolo. Nessuna sfortuna.
Commento: «Incontrare il proprio signore in un vicolo»: non si è persa la via.
Linea 2 - Incontri segreti in un vicolo angusto
I rapporti formali con qualcuno potrebbero essere difficili o forzati. Durante un incontro casuale, o in un luogo insolito, informale, segreto cogliete l'occasione per instaurare un legame duraturo.
Linea 3 - Yin (6): Si vede un carro trainato faticosamente da un bue tenuto a freno; il carrettiere ha la fronte tatuata e il naso tagliato. Non c'è un inizio. Ci sarà una buona fine.
Commento: «Si vede un carro trainato faticosamente»: [la linea] non è adatta alla posizione. «Non c'è un inizio ma ci sarà una buona fine»: perché si incontra una [quarta linea] dura.
Linea 3 - Carro e buoi bloccati, brutto inizio, bella fine
Siete messi in guardia dal compiere una certa mossa, dopo aver visto i guai in cui sono incappate le persone che prima di voi hanno compiuto quella stessa azione. I dubbi vi trattengono. Potreste separarvi temporaneamente da un compagno.
Linea 4 - Yang (9): Isolati per contrapposizione: si incontra un uomo autentico. Dimostrare sincerità l'uno all'altro. Pericoloso. Nessuna sfortuna.
Commento: «Dimostrare sincerità l'uno all'altro», «nessuna sfortuna»: la volontà sarà realizzata.
Linea 4 - Completamente soli, troverete un buon amico
Potete superare l'isolamento e la solitudine. Trovate persone con interessi affini con cui comunicare con sincerità. Arriverete a fidarvi reciprocamente.
 
Linea 5 - Yin (6): Il rimpianto svanisce. Gli antenati stanno mangiando carne. Nel procedere, quale sfortuna ci potrebbe essere?
Commento: «Gli antenati stanno mangiando carne»: il procedere otterrà buona fortuna.
Linea 5 - Unitevi alla festa nel tempio degli avi
Accettate l'invito a partecipare alla festa della vita. Che male potrebbe farvi? Prendete parte al divertimento. Superate le barriere che vi separano dagli amici.
Linea 6 - Yang (9): Isolati per contrapposizione. Si vede un maiale coperto di sudiciume, un carro pieno di diavoli. Prima si tende l'arco, poi lo si ripone. Non alleato coi banditi ma cerca un matrimonio. Andando avanti si incontra la pioggia, poi fausto.
Commento: Il responso «fausto» dell'«incontrare la pioggia» significa che i dubbi accumulati svaniranno.
Linea 6 - Maiali coperti di fango
Stare per vostro conto vi ha reso cauti. Una minaccia apparente vi mette all'erta. La tensione e il sospetto si tramutano in sollievo quando vedete che non s'intende fare del male.
			 */
			/// </summary>
			public class TheContrasting : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Il rimpianto svanisce. Perdere cavalli. Non li rincorrere: torneranno da soli. Vedere un uomo malvagio. Nessuna sfortuna.",
							"Incontrare il proprio signore in un vicolo. Nessuna sfortuna.",
							"Si vede un carro trainato faticosamente da un bue tenuto a freno; il carrettiere ha la fronte tatuata e il naso tagliato. Non c'è un inizio. Ci sarà una buona fine.",
							"Isolati per contrapposizione: si incontra un uomo autentico. Dimostrare sincerità l'uno all'altro. Pericoloso. Nessuna sfortuna.",
							"Il rimpianto svanisce. Gli antenati stanno mangiando carne. Nel procedere, quale sfortuna ci potrebbe essere?",
							"Isolati per contrapposizione. Si vede un maiale coperto di sudiciume, un carro pieno di diavoli. Prima si tende l'arco, poi lo si ripone. Non alleato coi banditi ma cerca un matrimonio. Andando avanti si incontra la pioggia, poi fausto.",
						};
					}
					
				}
				public override int Id => 38;
				public override string Sentency { get; set; } = "La Contrapposizione. Fausto per le piccole cose.";

				public override string Comment => "La Contrapposizione: il Fuoco si muove verso l'alto, il Lago si muove verso il basso, come due sorelle [figlia mediana e figlia minore] che vivono insieme, ma le loro volontà non sono le stesse. \"Gioioso\" e \"luminoso\" danno luce. [Nella quinta posizione] una linea morbida si muove, avanzando verso l'alto, ottiene la posizione centrale e corrisponde alla linea dura [seconda linea]. Ciò significa: «fausto per le piccole cose». Il cielo e la terra sono contrapposti, ma la loro azione è comune; l'uomo e la donna sono contrapposti, ma tendono all'unione; tutti gi esseri sono contrapposti gli uni agli altri, ma hanno lo stesso tipo di funzioni. Il tempo della Contrapposizione riveste grande importanza.";
				public override string Image => "Il Fuoco sopra, il Lago sotto: questa è l'immagine della Contrapposizione. Ispirandosi ad essa, il signore riconosce le differenze malgrado le somiglianze.";
				public override string Series => "Quando la via della famiglia è finita sopravvengono malintesi. Per questo segue il segno: la Contrapposizione. Contrapposizione significa malintesi.";
				public override string MixedSign => "La Contrapposizione è l'esterno.";
				public override string AddictionalSentence => "Gli uomini dell'antichità incordarono un legno come arco e indurirono pezzi di legno nel fuoco come frecce. L'utilità di arco e freccia consiste nel tenere il mondo in timore. Questo lo trassero certamente dal segno: la Contrapposizione.";
			}
			/// <summary>
			/// 39
			/*
			 * LINEE MOBILI	LINEE MOBILI
Linea 1 - Yin (6): Andare conduce a impedimenti, tornare incontra lode.
Commento: «Andare conduce a impedimenti, tornare incontra lode», perchè è appropriato aspettare.
Linea 1 - Non si può procedere, chi ritorna viene lodato
Se andate avanti vi metterete nei guai perché siete deboli e non avete nessuno che vi aiuti. Verrete ammirato per il vostro buon senso che vi suggerisce di rimanere dove siete. Aspettate un momento più favorevole per proseguire.
Linea 2 - Yin (6): I servitori del Re trovano impedimento su impedimento. Non a causa di loro stessi.
Commento: «I servitori del Re trovano impedimento su impedimento», ma alla fine non ci sarà alcuna calamità.
Linea 2 - Il servitore incontra ostacoli per conto del re
Avete la responsabilità di risolvere un problema che non avete causato. Se non potete risolverlo da solo fatevi aiutare.
Linea 3 - Yang (9): Andare conduce a impedimenti, viceversa tornare.
Commento: «Andare conduce a impedimenti, viceversa tornare»: il trigramma interno porta felicità.
Linea 3 - Il movimento è impedito, tornate dagli amici
Se proseguite troverete alcuni ostacoli. Ritornate verso una base sicura dove avete degli amici. Aspettate che vi giunga un aiuto.
Linea 4 - Yin (6): Andare conduce a impedimenti, tornare con unione.
Commento: «Andare conduce a impedimenti, tornare con unione»: [la linea] è appropriata e stabile.
Linea 4 - Ora siete bloccati, avanzate con l'appoggio di qualcuno
Siete troppo deboli per avanzare proprio adesso. Fermatevi, radunate le forze con l'aiuto degli altri e poi procedete.
Linea 5 - Yang (9): Grandi impedimenti. Arrivano gli amici.
Commento: «Grandi impedimenti; arrivano gli amici»: mitigato dal fatto che è centrale.
Linea 5 - Gli amici vi sorreggono quando cadete
Proprio quando state per cadere qualcuno corre ad afferrarvi. Nei momenti difficili avete amici che possono aiutarvi.
Linea 6 - Yin (6): Andare conduce a impedimenti, tornare conduce a prosperità. Fausto. Propizio vedere un grande uomo.
Commento: «Andare conduce a impedimenti, tornare conduce a prosperità», perché la volontà è nel trigramma interno. «Propizio vedere un grande uomo» e seguire il più nobile.
Linea 6 - Bloccati, ritornate alla saggezza interiore
Siete abbastanza saggi per sapere che alcuni ostacoli si affrontano meglio rimanendo immobili e agendo secondo i vostri pensieri e inclinazioni. Potreste rendervi utili agli altri con questo consiglio.
			 */
			/// </summary>
			public class TheDifficulties : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Andare conduce a impedimenti, tornare incontra lode.",
							"I servitori del Re trovano impedimento su impedimento. Non a causa di loro stessi.",
							"Andare conduce a impedimenti, viceversa tornare.",
							"Andare conduce a impedimenti, tornare con unione.",
							"Grandi impedimenti. Arrivano gli amici.",
							"Andare conduce a impedimenti, tornare conduce a prosperità. Fausto. Propizio vedere un grande uomo.",
						};
					}
					
				}
				public override int Id => 39;
				public override string Sentency { get; set; } = "L'Impedimento. Propizio andare verso l'ovest e il sud; non propizio andare verso l'est e il nord. Propizio vedere un grande uomo. Oracolo fausto.";

				public override string Comment => "L'Impedimento vuol dire: difficoltà. Il trigramma superiore è \"pericoloso\"; vedere il pericolo e sapersi \"fermare\" [trigramma inferiore]: ciò è saggezza. «L'Impedimento. Propizio andare verso l'ovest e il sud», perché la linea centrale del trigramma superiore è corretta. «Non  propizio andare verso l'est e il nord», perché lì la via finisce. «Propizio vedere un grande uomo»: procedere porterà buoni risultati. La posizione adatta [delle linee dalla seconda alla sesta] dà un «oracolo fausto» per riportare all'ordine  il Paese. Il tempo dell'Impedimento è estremamente importante.";
				public override string Image => "Il Monte con l'Acqua sopra: questa è l'immagine dell'Impedimento. Ispirandosi ad essa, il signore si volge verso la propria persona per coltivare la virtù.";
				public override string Series => "Dalla contrapposizione nascono necessariamente difficoltà. Per questo segue il segno: l'Impedimento. Impedimento significa difficoltà.";
				public override string MixedSign => "Impedimento significa difficoltà.";
				public override string AddictionalSentence => "";
			}
			/// <summary>
			/// 40
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yin (6): Nessuna sfortuna. [1] 
Commento: La [prima] linea dura che ha corrispondenza con la [quarta] linea morbida significa «nessuna sfortuna».
Linea 1 - Se deve essere fatto, fatelo ora, non vi accadrà nulla di male
Potete sistemare qualcosa prima che venga arrecato un vero danno. Non attardatevi. Prendete a prestito forza e saggezza da coloro che ne possiedono lavorando con loro. Aiutare ed essere aiutati.
Linea 2 - Yang (9): Nel campo di caccia, tre volpi sono catturate. Si ottiene una freccia d'oro. Oracolo fausto.
Commento: L'«oracolo fausto» della seconda linea dura è dovuto al fatto che ha ottenuto la via centrale.
Linea 2 - Cattura tre volpi, ricompensato con una freccia d'oro
Le tre volpi sono avidità, odio e ignoranza. Se eliminerete questi difetti verrete ampiamente ricompensati. Agite ora con semplicità e sincerità. Potrebbe esservi affidata una posizione di responsabilità.
Linea 3 - Yin (6): Trasportare in spalla un carico mentre si va sul carro. Attirare l'attenzione dei banditi. Oracolo di umiliazione.
Commento: «Trasportare in spalla un carico mentre si va sul carro»: è una cosa vergognosa. Sei stato tu stesso ad attirare l'attenzione [dei banditi] e a chi andrà la sfortuna?
Linea 3 - Sistemare i bagagli in cima alla carrozza attira le aggressioni
Con l'assunzione di troppe responsabilità potreste fingere di essere chi non siete e imboccare scorciatoie. Questo comportamento non piacerà a molti, potreste essere attaccati. Abbandonate le ambizioni esagerate. Fate soltanto ciò che siete in grado di fare.
Linea 4 - Yang (9): Liberazione dell'alluce. Arrivano gli amici; essere sinceri.
Commento: «Liberazione dell'alluce»: [la linea] non è adeguata [alla posizione].
Linea 4 - Liberate il pollice
Siete coinvolti in una situazione da cui dovreste uscire immediatamente. Fortunatamente non è troppo tardi per farlo. Se non perdete tempo ne trarrete beneficio; se aspettate potreste perdere.
 
Linea 5 - Yin (6): Liberazione del signore legato. Fausto. Essere sinceri tra le persone meschine.
Commento: «Liberazione del signore»: le persone meschine si ritirano.
Linea 5 - Liberatevi e siate d'ispirazione per gli altri
La vostra determinazione e intelligenza vi ha procurato la libertà. Sarete d'esempio per gli altri. L'opposizione alle vostre idee si smorza. La collaborazione con gli altri risolve la negatività.
Linea 6 - Yin (6): Il duca tira a un falco da un alto muro della città; lo prende. Nulla che non sia propizio.
Commento: «Il duca tira a un falco»: così libera [se stesso] da un ribelle.
Linea 6 - Mirate al falco sul muro in alto, un colpo diretto
Qualcuno non coinvolto di persona può agire con imparzialità per superare un problema difficile. Puntate in alto e liberatevi degli ultimi ostacoli alla vostra libertà.
				 */
			/// </summary>
			public class TheLiberty : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Nessuna sfortuna. [1]",
							"Nel campo di caccia, tre volpi sono catturate. Si ottiene una freccia d'oro. Oracolo fausto.",
							"Trasportare in spalla un carico mentre si va sul carro. Attirare l'attenzione dei banditi. Oracolo di umiliazione.",
							"Liberazione dell'alluce. Arrivano gli amici; essere sinceri.",
							"Liberazione del signore legato. Fausto. Essere sinceri tra le persone meschine.",
							"Il duca tira a un falco da un alto muro della città; lo prende. Nulla che non sia propizio.",
						};
					}
					
				}
				public override int Id => 40;
				public override string Sentency { get; set; } = "La Liberazione. Propizio andare verso l'ovest e il sud. Se non c'è un luogo in cui andare, è fausto venire e ritornare. Se c'è un luogo in cui andare, è fausto partire subito.";

				public override string Comment => "La Liberazione: \"pericoloso\" e \"mobile\". \"Muoversi\" per sfuggire al \"pericolo\": questa è la Liberazione. «La Liberazione. Propizio andare verso l'ovest e il sud», perché procedendo conquisti la gente. «E' fausto venire e ritornare»: perché si mantiene la posizione centrale [che ha corrispondenza]. «Se c'è un luogo in cui andare, è fausto partire subito»: perché il procedere porterà buoni risultati. Quando il cielo e la terra si liberano, vengono tuono e pioggia. Quando vengono tuono e pioggia, i semi dei frutti, le gemme delle piante e degli alberi, si aprono e cominciano a germinare. Il tempo della Liberazione è estremamente importante.";
				public override string Image => "Il Tuono e la Pioggia si manifestano: questa è l'immagine della Liberazione. Ispirandosi ad essa, il signore perdona gli errori e rimette le colpe.";
				public override string Series => "Le cose non possono stare sempre tra impedimenti. Per questo segue il segno: la Liberazione. Liberazione significa distensione.";
				public override string MixedSign => "Liberazione significa distensione.";
				public override string AddictionalSentence => "";
			}
			/// <summary>
			/// 41
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yang (9): Concludere gli affari, andare in fretta. Nessuna sfortuna. Ponderare quanto si può diminuire.
Commento: «Concludere gli affari, andare in fretta»: il superiore concorda con la volontà.
Linea 1 - Una piccola offerta è del tutto accettabile
Fornite l'aiuto necessario e poi ritiratevi. Lasciate che le cose rimangano semplici. Sacrificarvi all'eccesso per gli altri potrebbe esservi dannoso, creare dipendenze e obblighi.
Linea 2 - Yang (9): Propizio oracolo. Imprendere qualcosa è disastroso. Non diminuire ma aumentare.
Commento: L'«oracolo propizio» della seconda linea dura è dovuto al fatto che è centrale per la volontà.
Linea 2 - Avanzate, ma non come in guerra
Non siate testardi, ma nemmeno troppo facilmente ac-contentabili. Aiutate gli altri finché ciò non vi indebolisce. Siate responsabili riguardo ai vostri doveri, senza aspirare a raggiungere una posizione più elevata.
 
Linea 3 - Yin (6): Tre persone viaggiano insieme e diminuiscono di una persona; una persona viaggia da sola e trova un amico.
Commento: «Una persona viaggia da sola»: tra tre persone nascono disaccordi.
Linea 3 - Uno ottiene supporto, e tre si muovono come uno solo
Se una persona prende posizione, gli altri si uniranno a lui. Uno per tutti e tutti per uno. Per essere efficace un gruppo di persone deve lavorare come una sola. Accantonate le necessità e i desideri individuali.
Linea 4 - Yin (6): Diminuire la malattia. Se il compito è svolto in fretta, l'esito sarà felice. Nessuna sfortuna.
Commento: «Diminuire la malattia», così ci sarà veramente felicità..
Linea 4 - Smorzate la concitazione febbrile
Riducete lo stress e la concitazione nella vostra vita. Raffreddate il passo febbrile e le cose si sistemeranno da sole. Migliorate ora le vostre abitudini e gli amici vi verranno in aiuto.
Linea 5 - Yin (6): Forse si può aumentare con un guscio di tartaruga che vale dieci conchiglie. Non può essere rifiutato. Totalmente fausto.
Commento: La quinta linea è morbida ma «totalmente fausta» a causa della protezione che proviene dalla linea superiore.
Linea 5 - Il dono di molti preziosi gusci di tartaruga non può essere rifiutato
Nel rendere più semplice la vostra vita avete creato lo spazio per una meravigliosa e inattesa opportunità. Accettate l'aiuto che vi giunge dall'alto. Ciò significa guadagnare senza cercare guadagno.
Linea 6 - Yang (9): Non diminuire ma aumentarlo. Nessuna sfortuna. Oracolo fausto. Propizio quando c'è un luogo in cui andare. Si ottengono dei servitori senza una famiglia.
Commento: «Non diminuire ma aumentare»: si riuscirà a realizzare completamente la volontà.
Linea 6 - Non più diminuzione, ora c'è accrescimento
L'obiettivo di rendere più semplice la vostra vita è stato raggiunto. Ora potete essere più espansivi. Spartite la vostra buona sorte con gli altri e traete beneficio per voi stessi senza privare gli altri.
				 */
			/// </summary>
			public class TheDecrease : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Concludere gli affari, andare in fretta. Nessuna sfortuna. Ponderare quanto si può diminuire.",
							"Propizio oracolo. Imprendere qualcosa è disastroso. Non diminuire ma aumentare.",
							"Tre persone viaggiano insieme e diminuiscono di una persona; una persona viaggia da sola e trova un amico.",
							"Diminuire la malattia. Se il compito è svolto in fretta, l'esito sarà felice. Nessuna sfortuna.",
							"Forse si può aumentare con un guscio di tartaruga che vale dieci conchiglie. Non può essere rifiutato. Totalmente fausto.",
							"Non diminuire ma aumentarlo. Nessuna sfortuna. Oracolo fausto. Propizio quando c'è un luogo in cui andare. Si ottengono dei servitori senza una famiglia.",
						};
					}
					
				}
				public override int Id => 41;
				public override string Sentency { get; set; } = "La Diminuzione. Essere sinceri. Totalmente fausto. Nessuna sfortuna. L'oracolo è possibile [che sia favorevole]. Propizio quando c'è un luogo in cui andare. Che cosa bisognerebbe usare? Si usino due ciotole di riso per il sacrificio.";

				public override string Comment => "La Diminuzione. Il trigramma inferiore è diminuito [togliendo una linea yang] e il trigramma superiore è aumentato [aggiungendo una linea yang]; questa via è rivolta verso l'alto. «La Diminuzione. Essere sinceri. Totalmente fausto. Nessuna sfortuna. L'oracolo è possibile. Propizio quando c'è un luogo in cui andare. Che cosa bisognerebbe usare? Si usino due ciotole di riso per il sacrificio»: le due ciotole di riso sono in accordo con il tempo. Perdere una linea dura o guadagnare una linea morbida è in accordo con il tempo. Diminuire ed aumentare, riempire e svuotare: queste azioni si effettuano in accordo col tempo.";
				public override string Image => "Il Monte con il Lago sotto: questa è l'immagine della Diminuzione. Ispirandosi ad essa, il signore doma la propria rabbia e raffrena i propri desideri.";
				public override string Series => "Con la distensione qualche cosa va certamente perduta. Per questo segue il segno: la Diminuzione.";
				public override string MixedSign => "I segni Diminuzione e Accrescimento sono l'inizio della fioritura e del declino.";
				public override string AddictionalSentence => "La Diminuzione mostra la cura della virtù. Mostra prima le difficoltà e poi ciò che è facile. Così tiene lontano il danno.";
			}
			/// <summary>
			/// 42
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yang (9): Propizio servirsene per una grande realizzazione. Totalmente fausto. Nessuna sfortuna.
Commento: «Totalmente fausto; nessuna sfortuna», sebbene la posizione inferiore non sia per le grandi faccende.
Linea 1 - Realizzate grandi cose
All'inizio di un'impresa l'energia è molta e le intenzioni sono ferme. Qualsiasi ostacolo può essere affrontato. Sfruttate al meglio questo momento per procedere. Se disponete del consiglio di un esperto, approfittatene.
Linea 2 - Yin (6): Forse si può aumentare con un guscio di tartaruga che vale dieci conchiglie. Non può essere rifiutato. Oracolo a lungo termine: fausto. Il Re lo usa per un'offerta a Dio. Fausto.
Commento: «Forse si può aumentare»: ciò proviene dal trigramma esterno.
Linea 2 - Il dono di molti preziosi gusci di tartaruga non può essere rifiutato
Perseverate nell'altruismo e nella disponibilità. Potreste ricevere qualcosa di gran valore. Accettatelo di buon grado. Un dono inaspettato è una benedizione.
Linea 3 - Yin (6): Aumentare. Servirsene in tempi di disastro. Nessuna sfortuna. Essere sinceri. Camminando nella strada centrale, riferire al Duca con un sigillo di giada.
Commento: «Aumentare; servirsene in tempi di disastro»: questa è una regola fissa.
Linea 3 - Trovate un guadagno dalla sventura, l'autorità del sigillo di giada
È un momento di transizione in cui potete ricondurre un evento sfortunato a vostro vantaggio. La vostra sincerità vi sarà di sostegno. Imparerete dalle vostre perdite. Troverete aiuto nelle avversità.
Linea 4 - Yin (6): Riferire camminando nella strada centrale. Il Duca dà il consenso. Propizio servirsene per trasferire la capitale.
Commento: «Riferire al Duca, che dà il consenso», aumentando la volontà.
Linea 4 - L'autorità di spostare la capitale
Potrebbe esservi affidata una missione importante. Avete provato di essere degni e ora potete affidarvi al supporto di altri. Servitevi di persone influenti per la vostra causa.
Linea 5 - Yang (9): Essere sinceri. Se hai buon cuore, non hai bisogno di chiedere. Totalmente fausto. Essendo sinceri la nostra virtù sarà favorita.
Commento: «Chi è sincero e ha buon cuore, non ha bisogno di chiedere» e  «la nostra virtù sarà favorita», realizzando completamente la volontà.
Linea 5 - L'onestà è la politica migliore
La virtù è ricompensa a se stessa. Agite senza pensare a un rendiconto. Siete fortunati ad avere amici fidati e a essere degni della fiducia altrui.
Linea 6 - Yang (9): Non aumentarlo, forse percuoterlo. Mantenere fermo il cuore. Non perseverare. Disastroso.
Commento: «Non aumentarlo»: è un'espressione che indica unilateralità. «Forse percuoterlo»: ciò viene dal trigramma esterno.
Linea 6 - La continua espansione attira le aggressioni
Siete andati fin dove potevate arrivare. Se continuate così sarete esposti agli attacchi. L'impulso di avidità vi ha alterato la mente. Non conoscete limiti. Rimettetevi in piedi e poi aiutate gli altri.
				 */
			/// </summary>
			public class TheGrafting : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Propizio servirsene per una grande realizzazione. Totalmente fausto. Nessuna sfortuna.",
							"Forse si può aumentare con un guscio di tartaruga che vale dieci conchiglie. Non può essere rifiutato. Oracolo a lungo termine: fausto. Il Re lo usa per un'offerta a Dio. Fausto.",
							"Aumentare. Servirsene in tempi di disastro. Nessuna sfortuna. Essere sinceri. Camminando nella strada centrale, riferire al Duca con un sigillo di giada.",
							"Riferire camminando nella strada centrale. Il Duca dà il consenso. Propizio servirsene per trasferire la capitale.",
							"Essere sinceri. Se hai buon cuore, non hai bisogno di chiedere. Totalmente fausto. Essendo sinceri la nostra virtù sarà favorita.",
							"Non aumentarlo, forse percuoterlo. Mantenere fermo il cuore. Non perseverare. Disastroso.",
						};
					}
					
				}
				public override int Id => 42;
				public override string Sentency { get; set; } = "L'Accrescimento. Propizio quando c'è un luogo in cui andare. Propizio attraversare il grande fiume.";

				public override string Comment => "L'Accrescimento: diminuire il superiore e accrescere l'inferiore, così la gente ha una gioia infinita. [La linea dura] scende dall'alto e si pone sotto l'inferiore: la sua via è immensa e luminosa. «Propizio quando c'è un luogo in cui andare», perché la buona fortuna è centrale e pienamente corretta. «Propizio attraversare il grande fiume», usando il legno superiore e ciò crea successo. L'Accrescimento è \"mobile\" e Mite, in progresso infinito ogni giorno. Il cielo elargisce e la terra genera rispondendo, così tutto si accresce senza restrizioni. La via dell'Accrescimento in ogni luogo procede in conformità col tempo.";
				public override string Image => "Il Vento e il Tuono: questa è l'immagine dell'Accrescimento. Ispirandosi ad essa, il signore nel vedere un bene la imita e se ha colpe le corregge.";
				public override string Series => "Se la diminuzione raggiunge il suo culmine, opera di certo accrescimento. Per questo segue il segno: l'Accrescimento.";
				public override string MixedSign => "I segni Diminuzione e Accrescimento sono l'inizio della fioritura e del declino.";
				public override string AddictionalSentence => "Quando il clan di Pao Hsi fu tramontato sorse il clan del Divino Coltivatore. Egli spaccò un pezzo di legno per farne un vomere e piegò un legno per farne il manico dell'aratro e insegnò a tutto il mondo il vantaggio di aprire la terra con l'aratro. Questo egli lo trasse certamente dal segno: l'Accrescimento. «L'accrescimento mostra la pienezza della virtù. L'accrescimento mostra la crescita della pienezza senza artifici. Così l'accrescimento favorisce ciò che è utile».";
			}
			/// <summary>
			/// 43
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yang (9): Sforzato nelle dita dei piedi. Incapace di andare avanti. Ciò provoca sfortuna.
Commento: «Andare avanti» sebbene «non si sia in grado»: ciò «provoca sfortuna».
Linea 1 - Inciampare
La vostra prima mossa in avanti è troppo frettolosa. Potreste cadere a faccia in avanti. Siete guidati da grandi forze capricciose. Fatevi furbi. Se non siete all'altezza della missione, non riuscirete a portarla a termine.
Linea 2 - Yang (9): Allarmato, gridare. Uomini armati ma nelle ore notturne. Non temere.
Commento: Nonostante gli «uomini armati», «non temere», perché si ottiene la posizione centrale.
Linea 2 - Scorribande notturne, nulla da temere
Il pericolo è attorno a voi. Se siete pronti non avrete nulla da temere. Uomo avvisato mezzo salvato. La forza è essere pronti. Come avere un allarme contro i ladri.
Linea 3 - Yang (9): Sforzato negli zigomi. Disastroso. Il signore, fermamente deciso, cammina da solo. Incontra la pioggia e si bagna, provocando rabbia. Nessuna sfortuna.
Commento: «Il signore è fermamente deciso»: alla fine non ci sarà nessuna sfortuna.
Linea 3 - Colpiti sulla guancia, bagnati dalla pioggia
Potrebbe essere seccante doversi occupare di qualcosa, ma l'orgoglio, l'imbarazzo o l'impazienza non dovrebbero impedirvi di giungere a un giusto compromesso. Altrimenti è meglio che andiate per la vostra strada.
Linea 4 - Yang (9): Nessuna carne sulle natiche. Egli cammina con difficoltà. Se una pecora è condotta, il rimpianto svanisce. Udire queste parole, ma non crederci.
Commento: «Cammina con difficoltà», perché una linea dura non è adatta alla [quarta] posizione. «Udire queste parole, ma non crederci»: non c'è chiara comprensione.
Linea 4 - Recare una pecora in segno di resa
La bandiera bianca della resa. Se non volete lasciarvi condurre, la vostra testardaggine vi porterà alla sconfitta e sarete comunque costretti a compiere cambiamenti. Siete liberi soltanto di seguire.
Linea 5 - Yang (9): Una capra selvaggia [1], decisa e risoluta, cammina nel mezzo della strada. Nessuna sfortuna.
Commento: «Camminare nel mezzo della strada: nessuna sfortuna»: [la linea]è centrale ma non ancora splendida.
Linea 5 - Occuparsi delle erbacce
È importante non lasciarsi influenzare dalla gente debole o dalle idee negative. Anche non abbandonare nessuno è una cosa importante. Fate ciò che va fatto e mantenete il vostro equilibrio.
Linea 6 - Yin (6): Nessun grido d'allarme. Alla fine disastroso.
Commento: Il disastro dovuto alla «mancanza di grido d'allarme» alla fine non durerà a lungo.
Linea 6 - Senza preavviso
Senza preavviso, il male viene allontanato dal potere. Se c'è qualcosa che vi provoca vergogna, sistematela. Non c'è riparo dalla luce. Preparatevi a disastri improvvisi.
				 */
			/// </summary>
			public class TheOverflowing : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Sforzato nelle dita dei piedi. Incapace di andare avanti. Ciò provoca sfortuna.",
							"Allarmato, gridare. Uomini armati ma nelle ore notturne. Non temere.",
							"Sforzato negli zigomi. Disastroso. Il signore, fermamente deciso, cammina da solo. Incontra la pioggia e si bagna, provocando rabbia. Nessuna sfortuna.",
							"Nessuna carne sulle natiche. Egli cammina con difficoltà. Se una pecora è condotta, il rimpianto svanisce. Udire queste parole, ma non crederci.",
							"Una capra selvaggia [1], decisa e risoluta, cammina nel mezzo della strada. Nessuna sfortuna.",
							"Nessun grido d'allarme. Alla fine disastroso.",
						};
					}
					
				}
				public override int Id => 43;
				public override string Sentency { get; set; } = "Lo Straripamento. Con decisione rendere nota la cosa alla corte del Re, con sincerità proclamarla. Pericoloso. Rapporto dalla propria città: non è propizio impugnare le armi. Propizio quando c'è un luogo in cui andare.";

				public override string Comment => "Lo Straripamento vuol dire: decisione. Le linee dure eliminano decise le linee morbide [per formare] il \"forte\" e il \"gioioso\", eliminando con armonia.  «Rendere nota la cosa alla corte del Re»: la linea morbida sale sopra sopra le cinque linee dure. «Proclamare con sincerità: pericoloso»: questo pericolo conduce però allo splendore. «Rapporto dalla propria città: non è propizio impugnare le armi»: ciò che era stimato viene annullato. «Propizio quando c'è un luogo in cui andare»: perché le linee dure stanno crescendo [dal basso] e portano ad una conclusione.";
				public override string Image => "Il Lago sopra al Cielo: questa è l'immagine dello Straripamento. Ispirandosi ad essa, il signore elargisce ricchezza ai suoi subordinati e rifugge dal vantarsi della propria virtù.";
				public override string Series => "Quando l'accrescimento continua senza sosta sopravviene di certo uno straripamento. Per questo segue il segno: lo Straripamento. Straripamento significa decisione.";
				public override string MixedSign => "Straripamento significa decisione. Il forte si volge deciso contro il debole.";
				public override string AddictionalSentence => "Nella più remota antichità si annodavano corde per governare. I santi dei tempi posteriori introdussero al loro posto documenti scritti, per governare i vari funzionari e per sorvegliare i sudditi. Questo lo trassero certamente dal segno: lo Straripamento.";
			}
			/// <summary>
			/// 44
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yin (6): Fermato da un freno di metallo. Oracolo fausto. Se c'è un luogo in cui andare: oracolo disastroso. Un maiale magro si dimena e non vuole muoversi.
Commento: «Fermato da un freno di metallo»: la via di una linea morbida è quella di lasciarsi controllare.
Linea 1 - Fermato da un freno di ferro
Vi sentite costretti e frustrati, ma forse avete paura che spezzando i legami diventereste sfrenati. Trovate il punto d'equilibrio tra controllo ed eccesso.
Linea 2 - Yang (9): Nel cesto c'è pesce. Nessuna sfortuna. Non propizio per gli ospiti.
Commento: «Nel cesto c'è pesce», ma non è adatto offrirlo agli ospiti.
Linea 2 - È avanzato un pesce, non datelo via
Non siete obbligati a dare via ciò che avete, se è appena sufficiente per voi. Fate tutti i passi necessari per proteggere le vostre risorse. Tutte le vostre uova sono in un cestino, perciò sorvegliatelo con attenzione.
Linea 3 - Yang (9): Nessuna carne sulle natiche. Egli cammina con difficoltà. Pericoloso. Nessuna grande sfortuna.
Commento: «Egli cammina con difficoltà», perché si muove senza lasciarsi controllare.
Linea 3 - Barcollare per la debolezza
Siete in una condizione di debolezza. Lo svantaggio può impedirvi di procurarvi guai peggiori. Se siete incerti, decidete che cosa volete veramente e agite con determinazione.
Linea 4 - Yang (9): Nel cesto non c'è pesce. Disastroso per cominciare un'azione.
Commento: «Disastro» dovuto alla «mancanza di pesce», perché si tiene distante dalla gente.
Linea 4 - Non c'è pesce nel cesto, sciagura
La credenza è spoglia. Se vi allontanate dalla gente e ve ne state in disparte, nessuno si fiderà di voi. Potreste perdere l'aiuto e l'assistenza degli altri.
 
Linea 5 - Yang (9): Un melone legato con foglie di salice. Splendore nascosto. Qualcosa che cade dal cielo.
Commento: La quinta linea dura è «splendore nascosto», perché è centrale e corretta. «Qualcosa che cade dal cielo»:  la volontà non devierà dai decreti [del cielo].
Linea 5 - Un melone avvolto nelle foglie, giunto dal cielo
Una fetta di melone dolce avvolta in tenere foglie speziate, una presentazione gustosa. Propizio per un'impresa di collaborazione. A tempo debito i frutti della vostra fatica matureranno in ricche ricompense. Potrebbe accadere inaspettatamente.
Linea 6 - Yang (9): Farsi incontro delle loro corna. Umiliazione. Nessuna sfortuna.
Commento: «Farsi incontro delle loro corna»: la linea superiore ha raggiunto la fine, perciò: «umiliazione».
Linea 6 - Le corna si incrociano; deplorevole, ma non è colpa vostra
Potreste essere duro con qualcuno la cui aggressività e testardaggine vi hanno costretto nell'angolo. Quando tutto è finito riprendete i vostri modi pacifici.
				 */
			/// </summary>
			public class TheMeet : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Fermato da un freno di metallo. Oracolo fausto. Se c'è un luogo in cui andare: oracolo disastroso. Un maiale magro si dimena e non vuole muoversi.",
							"Nel cesto c'è pesce. Nessuna sfortuna. Non propizio per gli ospiti.",
							"Nessuna carne sulle natiche. Egli cammina con difficoltà. Pericoloso. Nessuna grande sfortuna.",
							"Nel cesto non c'è pesce. Disastroso per cominciare un'azione.",
							"Un melone legato con foglie di salice. Splendore nascosto. Qualcosa che cade dal cielo.",
							"Farsi incontro delle loro corna. Umiliazione. Nessuna sfortuna.",
						};
					}
					
				}
				public override int Id => 44;
				public override string Sentency { get; set; } = "Il Farsi incontro. La donna è potente. Non bisogna prenderla come moglie.";

				public override string Comment => "Il Farsi incontro vuol dire: imbattersi. Un trigramma debole si fa incontro ad uno forte. «Non bisogna prenderla come moglie», perché [la coppia, come i trigrammi] non può rimanere a lungo insieme. Quando il cielo e la terra si incontrano tutte le creature sono pienamente manifestate. Le linee dure incontrano la posizione centrale e corretta, facendo sì che ogni cosa sotto il cielo progredisca con splendore. Il tempo e il significato del Farsi incontro riveste una grande importanza.";
				public override string Image => "Il Cielo con il Vento sotto: questa è l'immagine del Farsi incontro. Ispirandosi ad essa, il sovrano impartisce gli ordini, rendendoli pubblici a tutte le quattro regioni del Paese.";
				public override string Series => "Con la decisione ci si imbatte certamente in qualche cosa. Per questo segue il segno: il Farsi incontro. Farsi incontro significa imbattersi.";
				public override string MixedSign => "Farsi incontro significa imbattersi. Il debole si imbatte nel forte.";
				public override string AddictionalSentence => "";
			}
			/// <summary>
			/// 45
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yin (6): Essere sinceri, ma non fino alla fine; così c'è ora confusione, ora raccolta. Essi gridano, poi si stringono la mano e ridono. Non temere. Quando si va avanti: nessuna sfortuna.
Commento: «C'è ora confusione , ora raccolta»: la volontà è confusa.
Linea 1 - Incerti se unirvi, non abbiate paura
I primi tentativi di prendere decisioni in completa autonomia sono spesso goffi e confusi. Le persone discrete non rideranno di fronte alla vostra richiesta di aiuto. Quando avrete fatto chiarezza su ciò che volete, allora le cose accadranno da sé.
Linea 2 - Yin (6): Durevolmente fausto. [1] Nessuna sfortuna. Essere sinceri: propizio anche offrire un piccolo sacrificio.
Commento: «Durevolmente fausto; nessuna sfortuna»: la posizione centrale non cambia.
Linea 2 - Dietro consiglio, porgete una piccola offerta con sincerità
Un gesto sincero da parte vostra dimostra la volontà di darvi e di accettare aiuto. Lo sforzo compiuto compensa le vostre mancanze. Potreste ricevere un dono.
Linea 3 - Yin (6): Raccolta con lamenti. Nulla è propizio. Quando si va avanti: nessuna sfortuna. Piccola umiliazione.
Commento: «Quando si va avanti: nessuna sfortuna», perché sopra vi è il Mite.
Linea 3 - Non fate nulla e vi lamentate
Sentirsi respinti. Il vostro bisogno di compagnia potrebbe portarvi a compiere mosse avventate. Ciò non è abbastanza per arrecare offesa. Potrebbe essere il momento di lasciare e di cercare qualcosa di meglio.
Linea 4 - Yang (9): Molto fausto. Nessuna sfortuna.
Commento: «Molto fausto; nessuna sfortuna», benché la posizione [della linea] non sia appropriata.
Linea 4 - Grande fortuna, nessuna colpa
Benché non stiate risolvendo le cose nella maniera migliore, l'esito positivo vi mette al riparo dalle critiche. L'adulazione potrebbe aver distorto la vostra prospettiva.
Linea 5 - Yang (9): Raccolta delle persone di rango. Nessuna sfortuna. Non c'è sincerità. Totalmente [fausto]. [2] Oracolo a lungo termine: il rimpianto svanisce.
Commento: «Raccolta delle persone di rango»: la volontà non è ancora splendida.
Linea 5 - Stringersi attorno a un capo, oppure andare per proprio conto
Anche se siete in una posizione di comando, non tutti vi seguiranno automaticamente. È il vostro comportamento che ispirerà negli altri la fiducia in voi. Nonostante questo, alcuni andranno per proprio conto.
Linea 6 - Yin (6): Lamenti e sospiri,  lacrime a fiumi. Nessuna sfortuna.
Commento: «Lamenti e sospiri, lacrime a fiumi»: non c'è pace nella posizione in alto.
Linea 6 - Lamenti e pianti, non è colpa vostra
La vostra posizione elevata non è più sicura. Avete cercato di tenere insieme le cose, ma avete fallito. Non avete colpa. Ogni cosa prima o poi finisce e ora dovreste dedicarvi a qualcos'altro.
				 */
			/// </summary>
			public class TheCollection : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Essere sinceri, ma non fino alla fine; così c'è ora confusione, ora raccolta. Essi gridano, poi si stringono la mano e ridono. Non temere. Quando si va avanti: nessuna sfortuna.",
							"Durevolmente fausto. [1] Nessuna sfortuna. Essere sinceri: propizio anche offrire un piccolo sacrificio.",
							"Raccolta con lamenti. Nulla è propizio. Quando si va avanti: nessuna sfortuna. Piccola umiliazione.",
							"Molto fausto. Nessuna sfortuna.",
							"Raccolta delle persone di rango. Nessuna sfortuna. Non c'è sincerità. Totalmente [fausto]. [2] Oracolo a lungo termine: il rimpianto svanisce.",
							"Lamenti e sospiri,  lacrime a fiumi. Nessuna sfortuna.",
						};
					}
					
				}
				public override int Id => 45;
				public override string Sentency { get; set; } = "La Raccolta. Riuscita. Il Re si reca al tempio ancestrale. Propizio vedere un grande uomo. Riuscita. Propizio oracolo. Offrire grandi sacrifici è fausto. Propizio quando c'è un luogo in cui andare.";

				public override string Comment => "La Raccolta vuol dire: accumulazione. \"Devoto\" e \"gioioso\"; la quinta linea dura è centrale e trova corrispondenza con la seconda linea morbida. Perciò gli altri si raccolgono attorno a lui. «Il Re si reca al tempio ancestrale»: con pietà filiale, presenta la sua offerta. «Propizio vedere un grande uomo; riuscita», perché l'accumulazione si compie in modo corretto. «Offrire grandi sacrifici è fausto; propizio quando c'è un luogo in cui andare», perché ciò è devoto al comando del cielo. Osservando come tutte le cose sono raccolte insieme, riusciamo a scorgere la natura di tutte le cose nel cielo e sulla terra.";
				public override string Image => "Il Lago sopra la Terra: questa è l'immagine della Raccolta. Ispirandosi ad essa, il signore prepara le armi e si mette all'erta contro ogni imprevisto.";
				public override string Series => "Quando gli esseri si incontrano l'uno con l'altro, allora si accumulano. Per questo segue il segno: la Raccolta. Raccolta significa accumulazione.";
				public override string MixedSign => "Raccolta significa accumulazione.";
				public override string AddictionalSentence => "";
			}
			/// <summary>
			/// 46
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yin (6): Con verità ascendere. Molto fausto.
Commento: «Con verità ascendere; molto fausto»: il superiore concorda con la volontà.
Linea 1 - Viene accordata una promozione
La sorte vi arride nei primi stadi di una promozione, da una posizione inferiore a una migliore. Siete bene accetti nel gruppo. Avrete fortuna se sarete obbedienti e sensibili. Le menti si incontrano.
Linea 2 - Yang (9): Essere sinceri.  Propizio anche offrire un piccolo sacrificio. Nessuna sfortuna.
Commento: La «sincerità» della [seconda] linea dura porta felicità.
Linea 2 - Benedetta è la sincerità di una piccola offerta
Si richiede uno sforzo costante e fermo. I rapporti stretti non chiedono niente di più di un gesto sincero per celebrare l'inizio di una nuova impresa. Sarete di supporto a qualcuno con la lealtà e la sincerità.
Linea 3 - Yang (9): Ascendere in una città su un colle (= deserta).
Commento: «Ascendere in una città su un colle»: non c'è spazio per il dubbio.
Linea 3 - Impossessarsi di una città deserta
Qualcosa che vi giunge con estrema facilità potrebbe rivelarsi di poco valore o essere poco durevole. Appuratene il valore prima di agire.
Linea 4 - Yin (6): Il Re presenta un'offerta al monte Ch'i. Fausto. Nessuna sfortuna.
Commento: «Il Re presenta un'offerta al monte Ch'i», perché [una linea morbida è] devota nel seguire le responsabilità.
Linea 4 - Arrampicarsi sulla montagna recando offerte
State salendo e siete parte di forze grandiose. Sebbene non siate al centro del potere, siete invitati a partecipare. Un progresso paziente, passo dopo passo come la scalata di un monte.
Linea 5 - Yin (6): Oracolo fausto. Ascendere per gradi.
Commento: «Oracolo fausto; ascendere per gradi»: la volontà sarà completamente realizzata.
Linea 5 - Continuare a salire sulla scala
Muovetevi verso l'alto con pazienza e costanza, come su una scala, passo dopo passo. Apprezzate l'aiuto illuminato di cui fortunatamente disponete.
Linea 6 - Yin (6): Ascendere nell'oscurità. Oracolo propizio per chi non cessa mai di impegnarsi.
Commento: «Ascendere nell'oscurità»: sopra vi è impoverimento, non prosperità.
Linea 6 - Sorgere nell'oscurità con gli occhi chiusi
Vi state muovendo in un territorio sconosciuto. Guardatevi dalle forze nascoste e adattatevi all'ambiente nuovo. Immersi nell'oscurità, non potete scoraggiarvi a metà strada.
				 */
			/// </summary>
			public class TheAscend : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Con verità ascendere. Molto fausto.",
							"Essere sinceri.  Propizio anche offrire un piccolo sacrificio. Nessuna sfortuna.",
							"Ascendere in una città su un colle (= deserta).",
							"Il Re presenta un'offerta al monte Ch'i. Fausto. Nessuna sfortuna.",
							"Oracolo fausto. Ascendere per gradi.",
							"Ascendere nell'oscurità. Oracolo propizio per chi non cessa mai di impegnarsi.",
						};
					}
					
				}
				public override int Id => 46;
				public override string Sentency { get; set; } = "L'Ascendere. Suprema riuscita. Propizio vedere un grande uomo. Non temere. Fausto avanzare verso il sud.";

				public override string Comment => "L'Ascendere. Le linee morbide ascendono in accordo col tempo: Mite e \"devoto\". La linea dura [seconda linea] è centrale e trova corrispondenza [con la quinta linea]; ciò significa «suprema riuscita». «Propizio vedere un grande uomo; non temere»: perché ci sarà buona fortuna. «Fausto avanzare verso il sud»: la volontà sarà realizzata.";
				public override string Image => "Il Legno cresce all'interno della Terra: questa è l'immagine dell'Ascendere. Ispirandosi ad essa, il signore segue devotamente la sua virtù, accumulando piccole cose, fino a creare elevatezza e grandezza.";
				public override string Series => "L'accumularsi verso l'alto si chiama ascendere. Per questo segue il segno: l'Ascendere.";
				public override string MixedSign => "Ciò che ascende non ritorna indietro.";
				public override string AddictionalSentence => "";
			}
			/// <summary>
			/// 47
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yin (6): Natiche assillate da un mazza di legno. Entrare in una valle oscura. Per tre anni non vedere nessuno.
Commento: «Entrare in una valle oscura»: si è nascosti, non chiari.
Linea 1 - Percosso con i bastoni e imprigionato per tre anni
Il vostro desiderio di un avanzamento è frustrato. Potreste perdere ogni speranza per quella che è stata soltanto una battuta d'arresto di poco conto. Avete bisogno di aiuto per uscire dallo stato di depressione. Potrebbe essere un'esperienza valida da cui trarre insegnamento.
Linea 2 - Yang (9): Assillato con cibo e vino, mentre arrivano uomini cinti di scarlatto. Propizio per offrire sacrifici. Per un'impresa: disastroso. Nessuna sfortuna.
Commento: «Assillato con cibo e vino»: la posizione è centrale e porta buona fortuna.
Linea 2 - Esausto per il troppo bere e mangiare
Inseguire sempre i nostri desideri è stancante e alla fine insoddisfacente. Potremmo essere intrappolati da favori che abbiamo accettato. Se volete cambiare, sta per giungervi un aiuto. Dopo le difficoltà c'è il successo.
Linea 3 - Yin (6): Assillato da pietre, stretto da cardi spinosi. Entrare nella propria casa e non vedere la propria sposa. Disastroso.
Commento: «Stretto da cardi spinosi», perché [questa linea morbida] poggia su una linea dura. «Entrare nella propria casa e non vedere la propria sposa»: non è un buon presagio.
Linea 3 - Bloccato sotto una roccia, si aggrappa alle spine
Siete bloccato da una roccia in un luogo ostile. Potrebbe non esserci via d'uscita o la forza per uscire. Persino a casa potreste non trovare nessuno che vi dà conforto.
Linea 4 - Yang (9): Venire molto lentamente. Assillato in un carro di bronzo. L'umiliazione ha una fine.
Commento: «Venire molto lentamente»: la volontà è nel trigramma inferiore. La linea non è adatta alla posizione ma possiede degli associati.
Linea 4 - Trascinarsi in una carrozza di bronzo
Siete imbottigliati nel traffico. Non state ottenendo il supporto di cui avete bisogno. Non siete in grado di fare ciò che realmente vorreste. Poco alla volta ricostruirete la vostra energia e supererete questo stato di spossatezza.
Linea 5 - Yang (9): Naso e piedi vengono tagliati. Assillato da uomini cinti di porpora. Lentamente si libera. Propizio per offrire sacrifici.
Commento: «Naso e piedi vengono tagliati», vuol dire che la volontà non è realizzata. «Lentamente si libera», perché la linea è diritta e centrale. «Propizio per offrire sacrifici»: si otterrà buona fortuna.
Linea 5 - Caricato di responsabilità
L'esaurimento delle responsabilità. Essere bloccati in un affare senza ottenere ciò che si vuole. Se c'è mancanza di collaborazione, fate qualcosa per riunire le persone e allora le condizioni miglioreranno.
Linea 6 - Yin (6): Assillato da rampicanti e viti. Camminando incostante e instabile, dicendo: "il movimento produce rimpianto". Ci sarà rimpianto. Per un'impresa: fausto.
Commento: «Assillato da rampicanti e viti», perché la posizione non è adatta. «Il movimento produce rimpianto»:  rimpianto, ma «fausto» per avanzare.
Linea 6 - Impigliato nella vite
Esausti di essere esausti. Riuscite soltanto a pensare di essere in trappola. La trappola è opera vostra ed esiste nei vostri dubbi e nelle vostre incertezze. Cambiate. Superate voi stessi.
				 */
			/// </summary>
			public class TheExhaustion : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Natiche assillate da un mazza di legno. Entrare in una valle oscura. Per tre anni non vedere nessuno.",
							"Assillato con cibo e vino, mentre arrivano uomini cinti di scarlatto. Propizio per offrire sacrifici. Per un'impresa: disastroso. Nessuna sfortuna.",
							"Assillato da pietre, stretto da cardi spinosi. Entrare nella propria casa e non vedere la propria sposa. Disastroso.",
							"Venire molto lentamente. Assillato in un carro di bronzo. L'umiliazione ha una fine.",
							"Naso e piedi vengono tagliati. Assillato da uomini cinti di porpora. Lentamente si libera. Propizio per offrire sacrifici.",
							"Assillato da rampicanti e viti. Camminando incostante e instabile, dicendo: \"il movimento produce rimpianto\". Ci sarà rimpianto. Per un'impresa: fausto.",
						};
					}
					
				}
				public override int Id => 47;
				public override string Sentency { get; set; } = "L'Assillo. Riuscita. Fausto oracolo per un grande uomo. Nessuna sfortuna. Ci saranno cose da dire: non credere.";

				public override string Comment => "L'Assillo. Le linee dure sono accerchiate [dalla linee morbide]. \"Pericoloso\" e \"gioioso\" formano l'Assillo; non manca la «riuscita», sebbene sia solo per un signore. «Fausto oracolo per un grande uomo», perché le linee dure sono centrali. «Ci saranno cose da dire: non credere», perché il dare importanza alle parole avrà termine.";
				public override string Image => "Il Lago è rimasto senz'Acqua: questa è l'immagine dell'Assillo. Ispirandosi ad essa, il signore mette a repentaglio la sua vita per seguire la propria volontà.";
				public override string Series => "Se si ascende senza fermarsi si finisce certamente in assillo. Per questo segue il segno: l'Assillo.";
				public override string MixedSign => "l'Assillo significa incontro reciproco.";
				public override string AddictionalSentence => "L'assillo è la prova della virtù. L'assillo porta a perplessità e da qui al successo. Nell'assillo si impara a smorzare il proprio risentimento.";
			}
			/// <summary>
			/// 48
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yin (6): Un pozzo fangoso. Nessuno beve. Un pozzo abbandonato non attira nemmeno gli uccelli.
Commento: «Un pozzo fangoso; nessuno beve»: la posizione è troppo bassa. «Un pozzo abbandonato non attira nemmeno gli uccelli»: il tempo adatto è stato perso.
Linea 1 - Nessuno beve da un pozzo pieno di fango
Le vostre risorse si sono inquinate e ora sono inutilizzabili. Rifiuti tossici. La feccia della società. Non vale la pena appostarsi presso un vecchio pozzo. Quando si tocca il fondo, non resta che risalire.
Linea 2 - Yang (9): Dalla bocca del pozzo si tira ai pesci. La brocca rotta gocciola.
Commento: «Dalla bocca del pozzo si tira ai pesci»: non c'è nessuna cooperazione.
Linea 2 - Gettare pesce nel pozzo, il secchio perde
La vostra abilità viene malamente usata. State sprecando il vostro talento per obiettivi poco meritevoli. L'energia si sta disperdendo. Incanalate i vostri sforzi e puntate verso obiettivi validi.
Linea 3 - Yang (9): Un pozzo ripulito, ma nessuno beve. Il mio cuore si rattrista. Vi si potrebbe attingere. Il Re è illuminato e tutti accettano la sua benedizione.
Commento: «Un pozzo ripulito, ma nessuno beve»: questo è il procedere tristemente.  Se «il Re cerca l'illuminazione» si otterrà buona fortuna.
Linea 3 - Il pozzo è ripulito, ma nessuno beve
Le risorse sono a disposizione, ma rimangono inutilizzate. Il talento non viene riconosciuto e quindi va sprecato. Non bisognerebbe lasciarsi sfuggire le opportunità. Apprezzate ciò che la gente ha da offrire.
Linea 4 - Yin (6): Un pozzo rivestito di pietra. Nessuna sfortuna.
Commento: «Un pozzo rivestito di pietra; nessuna sfortuna», perché il pozzo viene riparato.
Linea 4 - Il pozzo viene riparato
Il pozzo è asciutto poiché le aperture sono ostruite. Ponete rimedio e tenete pulite le vostre vie di comunicazione. Rinfrescatevi e migliorate voi stessi in modo che i vostri fluidi riprendano a scorrere liberamente.
Linea 5 - Yang (9): Un pozzo limpido.  Una sorgente fresca. Si può bere.
Commento: «Bere alla sorgente fresca», perchè la posizione è centrale e corretta.
Linea 5 - La gente beve dal pozzo in cui scorre l'acqua pura
La vostra risorsa è di grande valore. Lasciate che venga usata e apprezzata. Rimanete in buona compagnia e dimostratevi disponibili.
Linea 6 - Yin (6): Un pozzo a cui si attinge pienamente. Non coprirlo. Essere sinceri. Totalmente fausto.
Commento: «Totalmente fausto» nella posizione più alta, significa un grande risultato.
Linea 6 - Il pozzo è stato lasciato scoperto
Le vostre risorse sono a disposizione di tutti. Lasciare il pozzo scoperto è una trascuratezza. Potreste agire con troppa leggerezza riguardo a voi stessi e alle vostre risorse. È una buona cosa, oppure vi state gettando via?
				 */
			/// </summary>
			public class TheWell : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Un pozzo fangoso. Nessuno beve. Un pozzo abbandonato non attira nemmeno gli uccelli.",
							"Dalla bocca del pozzo si tira ai pesci. La brocca rotta gocciola.",
							"Un pozzo ripulito, ma nessuno beve. Il mio cuore si rattrista. Vi si potrebbe attingere. Il Re è illuminato e tutti accettano la sua benedizione.",
							"Un pozzo rivestito di pietra. Nessuna sfortuna.",
							"Un pozzo limpido.  Una sorgente fresca. Si può bere.",
							"Un pozzo a cui si attinge pienamente. Non coprirlo. Essere sinceri. Totalmente fausto.",
						};
					}
					
				}
				public override int Id => 48;
				public override string Sentency { get; set; } = "Il Pozzo. Una città può essere trasferita, ma non il pozzo. Nessuna perdita, nessun guadagno. Andare e venire, e attingere al pozzo. Se la corda è troppo corta per attingere al pozzo, o se la brocca si rompe: disastroso.";

				public override string Comment => "Il Pozzo. Il Legno sotto l'Acqua significa: portare su l'acqua; questo è il Pozzo. Il pozzo nutre e non si esaurisce. «Una città può essere trasferita, ma non il pozzo», poiché le linee dure sono centrali. «Se la corda è troppo corta per attingere al pozzo», allora non ci sarà nessun risultato soddisfacente. «Se la brocca si rompe», ci sarà disastro.";
				public override string Image => "Sopra il Legno c'è l'Acqua: questa è l'immagine del Pozzo. Ispirandosi ad essa, il signore anima la gente durante il lavoro e la esorta ad aiutarsi a vicenda.";
				public override string Series => "Chi è assillato in alto si volge certamente verso il basso. Per questo segue il segno: il Pozzo.";
				public override string MixedSign => "Il Pozzo significa compenetrazione.";
				public override string AddictionalSentence => "Il pozzo mostra il campo della virtù. Il pozzo dimora al suo posto ma influisce anche su altre cose. Il pozzo aiuta a distinguere ciò che è retto.";
			}
			/// <summary>
			/// 49
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yang (9): Avvolgere con la pelle di un bue giallo.
Commento: «Avvolgere con la pelle di un bue giallo»: non è adatto per l'azione.
Linea 1 - Avvolti nella pelle di un bue giallo
Per fare qualsiasi cambiamento radicale avete bisogno del supporto sufficiente e di forza personale. Per il momento sviluppate la vostra sicurezza in voi stessi. Siate come neonati, avvolti bene in una coperta.
Linea 2 - Yin (6): Nel giorno stabilito si effettua la rivoluzione. Per un'impresa: fausto. Nessuna sfortuna.
Commento: «Nel giorno stabilito si effettua la rivoluzione»: l'azione è eccellente.
Linea 2 - È arrivato il giorno di fare quel cambiamento
È arrivato il giorno del cambiamento e vi trovate nella posizione ideale per sostenere la rivoluzione. Informate gli altri su quanto accade affinché possano fornire il loro contributo.
Linea 3 - Yang (9): Per un'impresa: disastroso. Oracolo: pericoloso. Dichiarare la rivoluzione per tre volte prima di ricorrervi. Essere sinceri.
Commento: «Dichiarare la rivoluzione per tre volte prima di ricorrervi»: altrimenti, cos'altro si potrebbe fare?
Linea 3 - Solo quando il cambiamento viene discusso tre volte, allora trova la fiducia
È importante discutere i problemi e ottenere un solido consenso prima di intraprendere una qualsiasi riforma. Quando avrete riordinato le cose, avrete l'accordo di cui avete bisogno.
Linea 4 - Yang (9): Il rimpianto svanisce. Essere sinceri. Cambiamento degli ordini: fausto.
Commento: Responso «fausto» per il «cambiamento degli ordini», perché la volontà incontra fiducia.
Linea 4 - Con fiducia cambiate le regole
Siete responsabili di avere promosso le riforme necessarie. Confidate nel successo, anche se potrebbero manifestarsi resistenze ai cambiamenti.
Linea 5 - Yang (9): Il grande uomo cambia come una tigre. Ancor prima di consultare l'oracolo, si ha sincerità.
Commento: «Il grande uomo cambia come una tigre»: il suo disegno è brillante.
Linea 5 - Le strisce brillanti della tigre
Avete fatto riforme straordinarie e sostanziali. Non avete bisogno dell'approvazione degli altri o dell'I Ching per sapere che cosa fare. Altri seguiranno con gioia i vostri passi.
Linea 6 - Yin (6): Il signore cambia come un leopardo, nelle persone mediocri la muta avviene solo nella faccia. Per un'impresa: disastroso. Oracolo per chi rimane a casa: fausto.
Commento: «Il signore cambia come un leopardo»: il suo disegno è appariscente. «Nelle persone mediocri la muta avviene solo nella faccia», perché la linea è devota e obbedisce all'autorità.
Linea 6 - Le belle macchie del leopardo
Dopo una trasformazione completa e fondamentale, sarete pronti a consolidare la vostra posizione. Gli altri si rivolgeranno a voi per l'attitudine al comando e la saggezza, imitando il vostro esempio.
				 */
			/// </summary>
			public class TheRevolution : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Avvolgere con la pelle di un bue giallo.",
							"Nel giorno stabilito si effettua la rivoluzione. Per un'impresa: fausto. Nessuna sfortuna.",
							"Per un'impresa: disastroso. Oracolo: pericoloso. Dichiarare la rivoluzione per tre volte prima di ricorrervi. Essere sinceri.",
							"Il rimpianto svanisce. Essere sinceri. Cambiamento degli ordini: fausto.",
							"Il grande uomo cambia come una tigre. Ancor prima di consultare l'oracolo, si ha sincerità.",
							"Il signore cambia come un leopardo, nelle persone mediocri la muta avviene solo nella faccia. Per un'impresa: disastroso. Oracolo per chi rimane a casa: fausto.",
						};
					}
					
				}
				public override int Id => 49;
				public override string Sentency { get; set; } = "La Rivoluzione. Nel giorno stabilito, si ottiene sincerità. Suprema riuscita. Propizio oracolo. Il rimpianto svanisce.";

				public override string Comment => "La Rivoluzione. L'Acqua e il Fuoco si smorzano a vicenda, come due sorelle [figlia mediana e figlia minore] che vivono insieme, sebbene le loro volontà non coincidano. Questa è la Rivoluzione. «Nel giorno stabilito, si ottiene sincerità»: nel fare una rivoluzione si incontra fiducia. La figura è: il \"luminoso\" si serve del \"gioioso\". «Suprema riuscita» perché [la quinta linea] è corretta. Quando la rivoluzione è appropriata, «il rimpianto svanisce». Il cielo e la terra si rivoluzionano continuamente per compiere il ciclo delle stagioni. T'ang e Wu  operarono rivoluzioni, devoti alla volontà del cielo e in accordo con i desideri del popolo. Il tempo della Rivoluzione riveste grande importanza.";
				public override string Image => "Il Fuoco dentro il Lago: questa è l'immagine della Rivoluzione. Ispirandosi ad essa, il signore mette ordine nel calendario e annuncia le stagioni.";
				public override string Series => "L'impianto di un pozzo deve necessariamente essere sovvertito col tempo. Per questo segue il segno: la Rivoluzione.";
				public override string MixedSign => "La Rivoluzione significa abolizione di ciò che è vecchio.";
				public override string AddictionalSentence => "";
			}
			/// <summary>
			/// 50
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yin (6): Un crogiolo coi piedi rovesciati: propizio per eliminare il cibo stantio. Prendere una concubina per ottenere un figlio [2]. Nessuna sfortuna.
Commento: «Un crogiolo coi piedi rovesciati»: questo non è ancora sbagliato; «propizio per eliminare il cibo stantio», per seguire ciò che migliore.
Linea 1 - Il crogiolo si rovescia, opportunità di pulirlo a fondo
Volgete una situazione sfavorevole a vostro vantaggio dando una bella ripulita. In circostanze particolari potreste essere costretti a improvvisare, ad arrangiarvi con quello che avete.
Linea 2 - Yang (9): Un crogiolo pieno di cibo. I nostri nemici si sono ammalati e non possono più avvicinarsi. Fausto.
Commento: «Un crogiolo pieno di cibo»: è qualcosa su cui essere cauti; «i nostri nemici si sono ammalati»: alla fine non ci sarà nessuna calamità.
Linea 2 - Il crogiolo è pieno di cibo
Ciò in cui credete si realizzerà. Avete ciò che vi serve e quindi potete essere indipendenti. Altri potrebbero invidiare la vostra fortuna, ma non possono nuocervi.
Linea 3 - Yang (9): Un crogiolo coi manici alterati. Il suo movimento è impedito, il grasso di fagiano non viene mangiato. Quando cade la pioggia, il rimpianto svanisce. Alla fine fausto.
Commento: «Un crogiolo coi manici alterati», significa la perdita del suo scopo.
Linea 3 - I manici del crogiolo sono troppo caldi per poter essere toccati, il cibo brucia
La vostra impazienza ha guastato un'opportunità. Ora è troppo calda per essere toccata. Quando le cose si saranno raffreddate potrete intervenire nella situazione e ci sarà un miglioramento.
Linea 4 - Yang (9): Un crogiolo con le gambe rotte. La vivanda del Duca viene rovesciata. La figura ne rimane imbrattata. Disastroso.
Commento: «La vivanda del Duca viene rovesciata»: come si potrebbe avere ancora fiducia?
Linea 4 - Si spezzano le gambe del crogiolo, il cibo si rovescia, sciagura
La mancanza di supporto e la noncuranza hanno causato un crollo e arrecato danno. Forse vi siete sovraccaricati e avete ceduto allo sforzo.

 
Linea 5 - Yin (6): Un crogiolo con manici gialli e asta di bronzo. Propizio oracolo.
Commento: «Un crogiolo con manici gialli»: la linea centrale ha solida sostanza.
Linea 5 - Il crogiolo ha manici di bronzo
Ogni cosa funziona adesso. Siete pronti ad ascoltare la verità. Tutti collaborano affinché ogni cosa funzioni. Avete un appiglio per intervenire nella situazione.
Linea 6 - Yang (9): Un crogiolo con asta di giada. Molto fausto. Nulla che non sia propizio.
Commento: «L'asta di giada» nel posto più alto mostra la [linea] dura che sale su una linea morbida.
Linea 6 - Il crogiolo viene trasportato con un'asta di giada
Tutto ciò che farete verrà a vostro vantaggio. Siete fermi eppure flessibili. Avete la mano fredda nelle situazioni calde. Grande fortuna.
				 */
			/// </summary>
			public class TheCrucible : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Un crogiolo coi piedi rovesciati: propizio per eliminare il cibo stantio. Prendere una concubina per ottenere un figlio [2]. Nessuna sfortuna.",
							"Un crogiolo pieno di cibo. I nostri nemici si sono ammalati e non possono più avvicinarsi. Fausto.",
							"Un crogiolo coi manici alterati. Il suo movimento è impedito, il grasso di fagiano non viene mangiato. Quando cade la pioggia, il rimpianto svanisce. Alla fine fausto.",
							"Un crogiolo con le gambe rotte. La vivanda del Duca viene rovesciata. La figura ne rimane imbrattata. Disastroso.",
							"Un crogiolo con manici gialli e asta di bronzo. Propizio oracolo.",
							"Un crogiolo con asta di giada. Molto fausto. Nulla che non sia propizio.",
						};
					}
					
				}
				public override int Id => 50;
				public override string Sentency { get; set; } = "Il Crogiolo. Totalmente fausto. Riuscita.";

				public override string Comment => "Il Crogiolo: è l'immagine di un oggetto [un calderone cerimoniale] . Il Legno è messo nel Fuoco per la cottura sacrificale. I santi cuociono offerte per il per il Signore Iddio e preparano vivande nelle solennità per nutrire i santi e gli uomini di valore.  Il Mite significa percezione, occhi chiari e orecchie acute. Una linea morbida avanza e si muove verso l'alto, ottenendo la posizione centrale [nel trigramma superiore] e avendo corrispondenza con la [seconda] linea dura. Perciò vi è «suprema riuscita». [1]";
				public override string Image => "Il Fuoco sopra il Legno: questa è l'immagine del Crogiolo. Ispirandosi ad essa, il signore rimane nella giusta posizione e porta a compimento il destino.";
				public override string Series => "Nulla trasmuta le cose tanto quanto il crogiolo. Per questo segue il segno: il Crogiolo.";
				public override string MixedSign => "Il Crogiolo significa accettazione di ciò che è nuovo.";
				public override string AddictionalSentence => "";
			}
			/// <summary>
			/// 51
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yang (9): Il tuono viene: allarme! allarme! Dopo, ridere e parlare: ah! ah! Fausto.
Commento: «Il tuono viene: allarme! allarme!», ma la paura reca buona fortuna. «Ridere e parlare: ah! ah!», perché dopo segue la norma.
Linea 1 - Dopo il fulmine, ridere con sollievo
Una sorpresa vi lascia scossi, ma avete imparato qualcosa di importante. Dopo il panico iniziale, diminuisce la tensione. Talvolta i rischi vanno corsi.
Linea 2 - Yin (6): Il tuono viene con pericolo. Forse centomila denari persi. Arrampicarsi sui nove colli, non inseguirli. Dopo sette giorni si riotterranno.
Commento: «Il tuono viene con pericolo»: [una linea morbida] poggia su una linea dura.
Linea 2 - Fulmini pericolosi, scappate sulle colline
Come davanti alla minaccia di un terremoto o di un'inondazione dovete mettervi in salvo. Riavrete ciò che avete perso appena la minaccia si sarà dileguata.
Linea 3 - Yin (6): Il tuono fa uscire di senno. Se col tuono si va avanti: nessuna catastrofe.
Commento: «Il tuono fa uscire di senno»: [la linea] non è adatta alla posizione.
Linea 3 - Fulmini fragorosi vi scuotono
Gli shock a cui siete sottoposti potrebbero risvegliarvi e indurvi a fare cose che mai avreste pensato di poter fare. Agite ora, con la mente sveglia.
Linea 4 - Yang (9): Il tuono, e dopo il fango.
Commento: «Il tuono, e dopo il fango», perché non è ancora splendido.
Linea 4 - Fulmine, cadete nel fango
Lo shock vi ha immobilizzato. Avete ceduto e ora siete immobilizzati nel fango. Potreste essere bloccati nel mezzo di un progetto. Bloccati ai comandi. Impantanati.
Linea 5 - Yin (6): Il tuono va e viene. Pericoloso. Forse non si perde nulla. C'è qualcosa da fare.
Commento: «Il tuono va e viene; pericoloso»: è rischioso andare avanti; ma le cose da fare sono centrali, perciò non c'è nessuna grande perdita.
Linea 5 - Fulmini dappertutto, preparatevi, nessuna perdita
Mantenete l'equilibrio malgrado gli shock e la confusione e i vostri piani non subiranno ritardi. Ci sono cose da fare e ingenti guadagni da riscuotere. Ogni cosa cadrà al suo posto.
Linea 6 - Yin (6): Il tuono arreca rovina e sguardi che errano spaventati. Per un'impresa: disastroso. Il tuono non ci colpisce ma colpisce i nostri vicini. Nessuna sfortuna. I parenti della moglie hanno da ridire.
Commento: «Il tuono arreca rovina», perché non è raggiunta la posizione centrale. «Disastroso» ma «nessuna sfortuna» perché la paura dei vicini ci mette in guardia.
Linea 6 - Il fulmine che si allontana, colpisce il suo vicino
Una situazione che non avete saputo affrontare si sta allontanando. Il pericolo non è diretto contro di voi, ma contro quelli che vi stanno attorno. Non siete nella posizione per agire con aggressività. Lasciate che passi.
				 */
			/// </summary>
			public class TheThunder : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Il tuono viene: allarme! allarme! Dopo, ridere e parlare: ah! ah! Fausto.",
							"Il tuono viene con pericolo. Forse centomila denari persi. Arrampicarsi sui nove colli, non inseguirli. Dopo sette giorni si riotterranno.",
							"Il tuono fa uscire di senno. Se col tuono si va avanti: nessuna catastrofe.",
							"Il tuono, e dopo il fango.",
							"Il tuono va e viene. Pericoloso. Forse non si perde nulla. C'è qualcosa da fare.",
							"Il tuono arreca rovina e sguardi che errano spaventati. Per un'impresa: disastroso. Il tuono non ci colpisce ma colpisce i nostri vicini. Nessuna sfortuna. I parenti della moglie hanno da ridire.",
						};
					}
					
				}
				public override int Id => 51;
				public override string Sentency { get; set; } = "L'Eccitante. Riuscita. Il tuono viene: allarme! allarme! Ridere e parlare: ah! ah! Il tuono spaventa per cento miglia, ma non si lasciano cadere il cucchiaio sacrificale e il calice.";

				public override string Comment => "L'Eccitante. «Riuscita. Il tuono viene: allarme! allarme!», ma la paura reca buona fortuna. «Ridere e parlare: ah! ah!», perché dopo segue la norma. «Il tuono spaventa per cento miglia»: cioè spaventa quelli che sono lontani e fa trasalire quelli che sono vicini. [«Ma non si lasciano cadere il cucchiaio sacrificale e il calice»:] coloro che si fanno avanti per proteggere il tempio degli avi e l'altare della terra natia sono in grado di sovrintendere ai sacrifici.";
				public override string Image => "Il Tuono è raddoppiato: questa è l'immagine dell'Eccitante. Ispirandosi ad essa, il signore, con timore e paura, mette ordine nella sua vita ed esamina le sue colpe.";
				public override string Series => "Tra i custodi degli oggetti sacri il figlio maggiore occupa il primo posto. Per questo segue il segno: l'Eccitante. L'Eccitante significa moto.";
				public override string MixedSign => "L'Eccitante significa insorgere.";
				public override string AddictionalSentence => "";
			}
			/// <summary>
			/// 52
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yin (6): Tenere fermi i propri piedi. Nessuna sfortuna. Oracolo propizio a lungo termine.
Commento: «Tenere fermi i propri piedi»: [la linea dura in basso] non perde correttezza.
Linea 1 - Tenete fermi i piedi
Questo non è il momento di lasciarsi travolgere o di impegnarsi in azioni impulsive. Fermatevi subito prima di commettere un passo falso. Accontentatevi della situazione presente.
Linea 2 - Yin (6): Tenere fermi i propri polpacci. Non si può salvare ciò che segue. [2] Il cuore non è lieto.
Commento: «Non si può salvare ciò che segue», perché questo non si ritira per ascoltarlo.
Linea 2 - Tenete le gambe immobili
Vorreste agire ma vi viene consigliato di trattenervi. Potreste non essere in grado di impedire a qualcuno di agire in maniera insensata.
Linea 3 - Yang (9): Tenere fermi i propri fianchi. Strapparsi i lombi. Pericoloso. Il cuore soffoca.
Commento: «Tenere fermi i propri fianchi»: il pericolo brucia il cuore.
Linea 3 - Rimanendo così immobili vi procurate un malore
Reprimere a viva forza i desideri genererà soltanto tensione. Cercando di guadagnare la calma con una rigidità artificiale vi procurerà un attacco d'ansia. Rilassatevi, concedetevi una tregua.
Linea 4 - Yin (6): Tenere fermo il proprio tronco. Nessuna sfortuna.
Commento: «Tenere fermo il proprio tronco», cioè fermare l'intero corpo.
Linea 4 - Rimanete completamente immobili, evitate il pericolo
Siate calmi, gentili e onesti. Affrontate le situazioni difficili mantenendo la calma e rilassandovi. Se farete questo, il turbinio attorno a voi non vi coinvolgerà.
Linea 5 - Yin (6): Tenere ferme le proprie mascelle. Le parole sono ordinate. Il rimpianto svanisce.
Commento: «Tenere ferme le proprie mascelle»: [la posizione è] corretta e centrale.
Linea 5 - Tenete la bocca chiusa
Potreste parlare molto senza riferire nulla, anche sotto pressione. D'altro canto la vostra lingua lunga potrebbe crearvi problemi. Pensate prima di parlare.
Linea 6 - Yang (9): Magnanimo arresto. Fausto.
Commento: Il responso «fausto» dell'«arresto magnanimo» proviene dal fatto che [la linea dura] è generosa alla fine.
Linea 6 - Perfettamente immobile
Fermatevi quando sarete arrivati in cima. Siete stati perseveranti e avete raggiunto la meta. Non proseguite, altrimenti precipiterete dalla montagna.
				 */
			/// </summary>
			public class TheStop : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Tenere fermi i propri piedi. Nessuna sfortuna. Oracolo propizio a lungo termine.",
							"Tenere fermi i propri polpacci. Non si può salvare ciò che segue. [2] Il cuore non è lieto.",
							"Tenere fermi i propri fianchi. Strapparsi i lombi. Pericoloso. Il cuore soffoca.",
							"Tenere fermo il proprio tronco. Nessuna sfortuna.",
							"Tenere ferme le proprie mascelle. Le parole sono ordinate. Il rimpianto svanisce.",
							"Magnanimo arresto. Fausto.",
						};
					}
					
				}
				public override int Id => 52;
				public override string Sentency { get; set; } = "L'Arresto. Tenere ferma la propria schiena così da non avvertire più il proprio corpo. [1] Attraversare il proprio cortile dove non si è visti dalla propria gente. Nessuna sfortuna.";

				public override string Comment => "L'Arresto significa: stare fermo. Stare fermi quando è tempo di stare fermi, procedere quando è tempo di procedere: così moto e quiete non mancano il momento giusto. Questa via è splendida e luminosa. Stare fermo nella quiete è stare fermo nel proprio posto. I trigrammi superiore e inferiore sono esattamente gli stessi e non hanno corrispondenza. Per questo è detto: «non avvertire più il proprio corpo; attraversare il proprio cortile dove non si è visti dalla propria gente; nessuna sfortuna».";
				public override string Image => "Monti sovrapposti: questa è l'immagine dell'Arresto. Ispirandosi ad essa, il signore non lascia che i suoi pensieri vadano oltre i limiti della sua posizione.";
				public override string Series => "Le cose non possono muoversi continuamente, bisogna costringerle a fermarsi. Per questo segue il segno: l'Arresto. L'Arresto significa stare fermo.";
				public override string MixedSign => "L'Arresto significa stare fermo.";
				public override string AddictionalSentence => "";
			}
			/// <summary>
			/// 53
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yin (6): L'oca selvatica progredisce gradualmente verso la riva. Il figlio minore è in pericolo. Ci sono lagnanze. Nessuna sfortuna.
Commento: «Il figlio minore è in pericolo», significa: nessuna sfortuna.
Linea 1 - La piccola oca lotta per raggiungere la sponda
La prima fase di qualsiasi viaggio è sempre rischiosa. Siete esposti alle critiche per la vostra inesperienza. Siate perseveranti e non perdete di vista l'obiettivo. Chiedete aiuto se ne avete bisogno.
Linea 2 - Yin (6): L'oca selvatica progredisce gradualmente verso la roccia. Mangiare e bere in pace e concordia. Fausto.
Commento: «Mangiare e bere in pace e concordia»: non mangiare solo per riempirsi.
Linea 2 - L'oca selvatica atterra sulle rocce aride
La prima parte del viaggio si è svolta nel migliore dei modi e avete trovato un posto sicuro, per quanto provvisorio, in cui fermarvi. Dividete questa fortuna con gli amici. È il momento di godere della compagnia reciproca.
Linea 3 - Yang (9): L'oca selvatica progredisce gradualmente verso l'altopiano. Il marito parte e non ritorna. La moglie è incinta ma abortisce. Disastroso. Propizio respingere i banditi.
Commento: «Il marito parte e non ritorna», perché si è staccato dal gruppo dei compagni. «La moglie è incinta ma abortisce», perdendo così la retta via. «Propizio respingere i banditi»: con dedizione ci si protegge a vicenda.
Linea 3 - L'oca selvatica raggiunge gli altipiani del deserto
Nella vostra fretta di riuscire vi siete allontanati per vostro conto e avete smarrito la strada. I progetti non giungeranno a compimento. Pensate a difendere la vostra posizione, anziché attaccare gli altri. Potreste essere traditi da un compagno.
Linea 4 - Yin (6): L'oca selvatica progredisce gradualmente verso l'albero. Forse raggiunge un ramo piatto. Nessuna sfortuna.
Commento: «Forse raggiunge un ramo piatto»: essa è devota e mite.
Linea 4 - L'oca selvatica riesce ad appollaiarsi su un ramo piatto di un albero
Trovate un posto in cui fermarvi per un po'. Il viaggio non è ancora terminato. Il luogo che avete trovato è una nicchia in cui potete bene inserirvi, ma non è la sede definitiva.
Linea 5 - Yang (9): L'oca selvatica progredisce gradualmente verso la vetta. Per tre anni la moglie non rimane incinta, alla fine non sarà sconfitta. Fausto.
Commento: «Alla fine non sarà sconfitta; fausto»: si ottiene ciò che si desidera.
Linea 5 - L'oca selvatica alla fine raggiunge la vetta
Grazie alla forza e alla flessibilità, alla fine raggiungerete il vostro obiettivo. Potreste impiegare più tempo del previsto. In definitiva siete invincibili. I progetti danno frutti. Sono possibili matrimoni e figli.
Linea 6 - Yang (9): L'oca selvatica progredisce gradualmente verso l'alto cielo. Le sue piume possono essere usate per la danza sacra. Fausto.
Commento: «Le sue piume possono essere usate per la danza sacra; fausto»: non è permesso il disordine.
Linea 6 - L'oca selvatica vola tra le nuvole, le sue piume vengono usate per la danza sacra
Siete andati oltre il normale successo e forse percepite il distacco dagli amici. Le vostre azioni serviranno d'ispirazione per gli altri.
				 */
			/// </summary>
			public class TheGradualProgress : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"L'oca selvatica progredisce gradualmente verso la riva. Il figlio minore è in pericolo. Ci sono lagnanze. Nessuna sfortuna.",
							"L'oca selvatica progredisce gradualmente verso la roccia. Mangiare e bere in pace e concordia. Fausto.",
							"L'oca selvatica progredisce gradualmente verso l'altopiano. Il marito parte e non ritorna. La moglie è incinta ma abortisce. Disastroso. Propizio respingere i banditi.",
							"L'oca selvatica progredisce gradualmente verso l'albero. Forse raggiunge un ramo piatto. Nessuna sfortuna.",
							" L'oca selvatica progredisce gradualmente verso la vetta. Per tre anni la moglie non rimane incinta, alla fine non sarà sconfitta. Fausto.",
							"L'oca selvatica progredisce gradualmente verso l'alto cielo. Le sue piume possono essere usate per la danza sacra. Fausto.",
						};
					}
					
				}
				public override int Id => 53;
				public override string Sentency { get; set; } = "Lo Sviluppo. Fausto per il matrimonio di una ragazza. Propizio oracolo.";

				public override string Comment => "Lo Sviluppo significa: progresso graduale. «Fausto per il matrimonio di una ragazza». Il progresso della linea che ottiene la posizione giusta significa che il procedere porterà buoni risultati. Il progresso corretto può mettere ordine nel Paese. Nella [quinta] posizione una linea dura ottiene il centro. \"Quieto\" e Mite (= \"penetrante\"): questo rende il moto inesauribile.";
				public override string Image => "L'Albero (= Legno) sopra il Monte: questa è l'immagine dello Sviluppo. Ispirandosi ad essa, il signore dimora in dignitosa virtù per migliorare i costumi della gente.";
				public override string Series => "Le cose non possono stare ferme per sempre. Per questo segue il segno: lo Sviluppo. Sviluppo significa progredire.";
				public override string MixedSign => "Lo Sviluppo mostra come la ragazza viene data in sposa e deve quindi aspettare le azioni dell'uomo.";
				public override string AddictionalSentence => "";
			}
			/// <summary>
			/// 54
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yang (9): La ragazza che si sposa e la figlia minore come concubina. Uno zoppo riesce a camminare. Per un'impresa: fausto.
Commento: «La ragazza che si sposa e la figlia minore come concubina»: è una pratica normale. «Uno zoppo riesce a camminare: fausto», perché [due linee dure] si sostengono l'una con l'altra.
Linea 1 - La sorella minore diventa la seconda moglie
Attraverso buone conoscenze si possono ottenere avanzamenti, anche se non siete pienamente qualificati, o siete carenti su qualche fronte.
Linea 2 - Yang (9): Un orbo riesce a vedere. Propizio oracolo per un solitario.
Commento: «Propizio oracolo per un solitario»: non c'è nessuna deviazione dalla regola.
Linea 2 - Vedere con un occhio solo, rimanere appartati
Se non potete vedere con chiarezza, rimanete nascosti. Potreste sentirvi impantanati. Un rapporto imperfetto può essere migliorato. Concentratevi su una cosa alla volta.
Linea 3 - Yin (6): La ragazza che si sposa e la figlia maggiore come concubina. Al contrario, è lei che si sposa come concubina.
Commento: «La ragazza che si sposa e la figlia maggiore come concubina»: non è adatta [alla posizione].
Linea 3 - Sperare di diventare moglie, finire concubina
Le vostre aspettative sono troppo ambiziose per essere esaudite in tempi brevi. Aspettate un'occasione migliore, altrimenti potreste pensare di esservi venduti a un prezzo inferiore al vostro valore.
Linea 4 - Yang (9): La ragazza che si sposa rinvia il termine. Un tardo matrimonio sopravviene a suo tempo.
Commento: La volontà di «rinviare il termine» significa aspettare prima di procedere.
Linea 4 - Ritardare il matrimonio, ogni cosa a tempo debito
Richiederà più tempo del previsto raggiungere l'obiettivo. Aspettate che i tempi siano maturi per fare qualcosa. Un ritardo potrebbe tornare a vostro vantaggio.
Linea 5 - Yin (6): Il Re Yi dà la figlia in sposa. Le vesti ricamate della sposa non sono così sfarzose come le splendide vesti della concubina. La luna è quasi piena. Fausto.
Commento: «Il Re Yi dà la figlia in sposa; le vesti ricamate della sposa non sono così sfarzose come le splendide vesti della concubina»: nella posizione centrale l'agire è comunque nobile.
Linea 5 - La sposa è vestita con meno sfarzo delle damigelle
Non avete nessun bisogno di mettervi in mostra, né di essere un pezzo grosso. Se avete il predominio, arretrate e lasciate che siano gli altri a risplendere.
Linea 6 - Yin (6): La moglie tiene una cesta, ma dentro non c'è alcun frutto. Il marito sgozza una pecora, ma il sangue non scorre. Nulla è propizio.
Commento: Una linea morbida in alto non ha «nessun frutto» perché «tiene una cesta vuota».
Linea 6 - Il cesto è vuoto
O possedete qualcosa di poco o nessun valore, oppure state facendo qualcosa senza averne le intenzioni. Gesti vuoti di significato.
				 */
			/// </summary>
			public class TheGirlWhoMarries : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"La ragazza che si sposa e la figlia minore come concubina. Uno zoppo riesce a camminare. Per un'impresa: fausto.",
							"Un orbo riesce a vedere. Propizio oracolo per un solitario.",
							"La ragazza che si sposa e la figlia maggiore come concubina. Al contrario, è lei che si sposa come concubina.",
							"La ragazza che si sposa rinvia il termine. Un tardo matrimonio sopravviene a suo tempo.",
							"Il Re Yi dà la figlia in sposa. Le vesti ricamate della sposa non sono così sfarzose come le splendide vesti della concubina. La luna è quasi piena. Fausto.",
							"La moglie tiene una cesta, ma dentro non c'è alcun frutto. Il marito sgozza una pecora, ma il sangue non scorre. Nulla è propizio.",
						};
					}
					
				}
				public override int Id => 54;
				public override string Sentency { get; set; } = "La Ragazza che si sposa. Per un'impresa: disastroso. Nulla è propizio.";

				public override string Comment => "La Ragazza che si sposa designa il grande senso di cielo e terra. Se il cielo e la terra non interagiscono, le miriadi di esseri non prosperano. La Ragazza che si sposa rappresenta la fine e l'inizio dell'umanità . \"Gioioso\" e \"mobile\": questa è la Ragazza che si sposa. «Per un'impresa: disastroso», perché la posizione delle linee non è adatta. «Nulla è propizio», perché le linee morbide stanno sopra le linee dure.";
				public override string Image => "Il Tuono sopra il Lago: questa è l'immagine della Ragazza che si sposa. Ispirandosi ad essa, il signore, per amore della perpetua durata, accetta ciò che è insoddisfacente.";
				public override string Series => "Progredendo si perviene certamente al luogo appropriato. Per questo segue il segno: la Ragazza che si sposa (= la ragazza che arriva alla casa nuziale).";
				public override string MixedSign => "La Ragazza che si sposa indica la fine della verginità.";
				public override string AddictionalSentence => "";
			}
			/// <summary>
			/// 55
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yang (9): Incontrare il signore nostro compagno. Per dieci giorni, nessuna sfortuna. Procedere porta riconoscimento.
Commento: «Per dieci giorni, nessuna sfortuna», oltre dieci giorni: disastroso.
Linea 1 - Incontrare il vostro compagno, bene per dieci giorni
Una persona importante vuole aiutarvi. Se usufruirete di questa generosità, considerate l'eventualità che possa volere qualcosa in cambio.
Linea 2 - Yin (6): Abbondante è la cortina. A mezzogiorno si vedono le stelle intorno al polo. Andare avanti porta un rischio di malattia. C'è sincerità da manifestare. Fausto.
Commento: «C'è sincerità da manifestare», cioè la sincerità manifesta la volontà.
Linea 2 - Le tende sono così spesse che anche a mezzogiorno bisogna accendere il lume
I gesti stravaganti serviranno soltanto a oscurare e a confondere le vostre vere intenzioni. Siate aperti e sinceri. Non è necessario che vi nascondiate dietro una facciata lussuosa.
Linea 3 - Yang (9): Abbondante è l'oscurità [o: le bandiere]. A mezzogiorno si vedono anche le stelle più piccole. [1]  E' rotto il braccio destro. Nessuna sfortuna.
Commento: «Abbondante è l'oscurità»: non è appropriato intraprendere grandi faccende. «È rotto il braccio destro»: alla fine non si può utilizzare.
Linea 3 - Così tante bandiere oscurano il cielo; un braccio destro rotto
Le bandiere servono per segnalare. Siete talmente bombardati da messaggi incrociati da subire una lesione. Non è grave, vi rimetterete presto. È persino possibile che ne ricaviate un vantaggio
Linea 4 - Yang (9): Abbondante è la cortina. A mezzogiorno si vedono le stelle intorno al polo. Incontrare il signore nostro simile. Fausto.
Commento: «Abbondante è la cortina», perché [la linea] non è adatta alla posizione. «A mezzogiorno si vedono le stelle intorno al polo»: c'è oscurità, non luce. «Incontrare il signore nostro simile»: è fausto procedere.
Linea 4 - Le tende pesanti oscurano tutta la luce
Lontani dalla luce, nell'oscurità. Siete in una posizione che vi penalizza e vi limita. Le menzogne e la confusione abbondano. Non potete vedere né essere visti. Quando avrete chiarito la confusione, vedrete la luce.
Linea 5 - Yin (6): Viene chiarezza. C'è prosperità e lode. Fausto.
Commento: Il responso è «fausto» nonostante la quinta linea sia morbida: ci sarà buona fortuna.
Linea 5 - La vostra splendente luce interiore vi procura buone cose
Siete bene accolti, i vostri doni e il vostro talento riscuotono apprezzamento. Avete qualcosa da celebrare. Disponete del supporto di persone valide.
Linea 6 - Yin (6): Abbondante è il tetto. Nascosta è la casa. Spiare attraverso il cancello: tutto è silenzioso, non c'è nessuno. Per tre anni non si vede nessuno. Disastroso.
Commento: «Abbondante è il tetto»: è salito fino ai confini del cielo. «Spiare attraverso il cancello: tutto è silenzioso, non c'è nessuno», perché si è nascosto.
Linea 6 - Sbirciare attraverso il cancello del palazzo vuoto da tre anni
Le cose non vanno bene come sembra. Lo splendore esteriore non coprirà le debolezze interiori. Potreste vivere al di sopra delle vostre possibilità. Tenendo la buona sorte per voi soli, rimanete isolati con la vostra presunzione.
				 */
			/// </summary>
			public class TheAbundage : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Incontrare il signore nostro compagno. Per dieci giorni, nessuna sfortuna. Procedere porta riconoscimento.",
							"Abbondante è la cortina. A mezzogiorno si vedono le stelle intorno al polo. Andare avanti porta un rischio di malattia. C'è sincerità da manifestare. Fausto.",
							"Abbondante è l'oscurità [o: le bandiere]. A mezzogiorno si vedono anche le stelle più piccole. [1]  E' rotto il braccio destro. Nessuna sfortuna.",
							"Abbondante è la cortina. A mezzogiorno si vedono le stelle intorno al polo. Incontrare il signore nostro simile. Fausto.",
							"Viene chiarezza. C'è prosperità e lode. Fausto.",
							"Abbondante è il tetto. Nascosta è la casa. Spiare attraverso il cancello: tutto è silenzioso, non c'è nessuno. Per tre anni non si vede nessuno. Disastroso.",
						};
					}
					
				}
				public override int Id => 55;
				public override string Sentency { get; set; } = "L'Abbondanza. Riuscita. Il Re la raggiunge. Non rattristarsi. Essere come il sole a mezzogiorno.";

				public override string Comment => "L'Abbondanza significa: grandezza. \"Luminoso\" e \"mobile\": questa è l'Abbondanza. «Il Re la raggiunge»: l'onore è grande. «Non rattristarsi: essere come il sole a mezzogiorno[o al solstizio]»: cioè illuminare appropriatamente ogni cosa sotto il cielo. A mezzogiorno [o al solstizio] il sole comincia a calare [o i giorni cominciano a declinare]; quando la luna è piena comincia a calare. Pienezza e vuotezza di cielo e terra crescono e calano con le stagioni. Quanto più vero è tutto ciò per gli uomini! Quanto più vero è tutto ciò per gli spiriti o per gli dei!";
				public override string Image => "Il Tuono e il lampo (= il Fuoco) arrivano insieme: questa è l'immagine dell'Abbondanza. Ispirandosi ad essa, il signore decide i processi e esegue le punizioni.";
				public override string Series => "Ciò che ottiene un posto in cui sia di casa diventa certamente grande. Per questo segue il segno: l'Abbondanza. Abbondanza significa grandezza.";
				public override string MixedSign => "L'Abbondanza significa molte occasioni.";
				public override string AddictionalSentence => "";
			}
			/// <summary>
			/// 56
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yin (6): Il viandante indugia su cose troppo meschine. In questo modo attira la catastrofe.
Commento: «Il viandante indugia su cose troppo meschine»: la volontà finisce in catastrofe.
Linea 1 - Viandante distratto e meschino
Se sapete dove andare e se siete ben preparati, vivrete una grande avventura. La concitazione e la ristrettezza di vedute, al contrario, portano a commettere errori e generano frustrazione e malevolenza.
Linea 2 - Yin (6): Il viandante giunge alla locanda. Ha con sé ciò che possiede. Ottiene un giovane servo. Oracolo: [fausto]. [1]
Commento: L'«oracolo» di «ottenere un giovane servo» significa: alla fine, nessuna calamità.
Linea 2 - Il viandante trova una locanda e un servitore
Avete trovato un bel posto in cui fermarvi dove le condizioni sono adeguate e la gente è cordiale. Eppure questa non può essere la vostra dimora permanente; potete fare in modo di fermarvi per un po' di tempo.
Linea 3 - Yang (9): Il viandante brucia la locanda in cui alloggia. Perde il suo giovane servo. Oracolo pericoloso.
Commento: «Il viandante brucia la locanda in cui alloggia», questo è un danno per lui stesso; se il viandante si associa all'inferiore, giustamente subisce una perdita.
Linea 3 - La locanda brucia e il viandante perde il servitore
Quando un temperamento impulsivo provoca discussioni, ciò che viene detto non può essere più ritirato. Il vostro rifugio non è più sicuro. Avete perso la benevolenza degli altri ed è meglio che vi rimettiate in cammino.
Linea 4 - Yang (9): Il viandante trova un rifugio in cui riposare. Mette da parte ciò che è di sua proprietà e l'ascia di viaggio. Il mio cuore non è lieto.
Commento: «Il viandante trova un rifugio in cui riposare», ma [la linea yang in una posizione yin] non ha ottenuto la posizione corretta. «Mette da parte ciò che è di sua proprietà e l'ascia di viaggio», ma nel suo «cuore non è lieto».
Linea 4 - Il viandante ottiene una proprietà e un'ascia
Trovate un posto per sistemarvi e i mezzi per vivere. Ma nel vostro cuore non siete soddisfatti. Vi sentite fuori posto. Non state facendo ciò che vorreste realmente fare.
Linea 5 - Yin (6): Colpire un fagiano con una singola freccia. Persa. [2] Alla fine si ottiene un decreto di lode.
Commento: «Alla fine si ottiene un decreto di lode» che proviene dall'alto.
Linea 5 - Il viandante abbatte un fagiano con una freccia
Dimostrate il vostro talento e guadagnate una buona posizione. Seppure in veste di estraneo, sarete bene accolti. Sfruttate al meglio le opportunità che vi vengono offerte senza farvene vanto.
Linea 6 - Yang (9): L'uccello brucia il proprio nido; il viandante prima ride, poi geme e piange. Perdere una vacca al pascolo. Disastroso.
Commento: Il viandante è salito al vertice: ciò significa che tutto è «bruciato». «Perdere una vacca al pascolo»: alla fine nulla ne è udito.
Linea 6 - Il rifugio del viandante va a fuoco
Se darete per scontata la vostra fortuna, essa si esaurirà. Lo scherno per le sventure degli altri potrebbe tramutarsi in pianto quando le sventure colpiranno voi.
				 */
			/// </summary>
			public class TheWanderer : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Il viandante indugia su cose troppo meschine. In questo modo attira la catastrofe.",
							"Il viandante giunge alla locanda. Ha con sé ciò che possiede. Ottiene un giovane servo. Oracolo: [fausto]. [1]",
							"Il viandante brucia la locanda in cui alloggia. Perde il suo giovane servo. Oracolo pericoloso.",
							"Il viandante trova un rifugio in cui riposare. Mette da parte ciò che è di sua proprietà e l'ascia di viaggio. Il mio cuore non è lieto.",
							"Colpire un fagiano con una singola freccia. Persa. [2] Alla fine si ottiene un decreto di lode.",
							"L'uccello brucia il proprio nido; il viandante prima ride, poi geme e piange. Perdere una vacca al pascolo. Disastroso.",
						};
					}
					
				}
				public override int Id => 56;
				public override string Sentency { get; set; } = "Il Viandante. Piccola riuscita. Oracolo per un viandante: fausto.";

				public override string Comment => "Il Viandante. «Piccola riuscita». La linea morbida nel centro del trigramma esterno è devota alle linee dure. \"Quieto\" e \"luminoso\", perciò:  «piccola riuscita». «Oracolo per un viandante: fausto». Il tempo del Viandante riveste grande importanza.";
				public override string Image => "Il Fuoco sul Monte: questa è l'immagine del Viandante. Ispirandosi ad essa, il signore è chiaro e cauto nell'applicare le punizioni e non indugia nei procedimenti giuridici.";
				public override string Series => "Quando la grandezza si esaurisce, è sicuro che essa perde la propria residenza. Per questo segue il segno: il Viandante.";
				public override string MixedSign => "Il Viandante significa essere poco attaccato.";
				public override string AddictionalSentence => "";
			}
			/// <summary>
			/// 57
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yin (6): Avanzare e retrocedere. Oracolo propizio per un guerriero.
Commento: «Avanzare e retrocedere»: la volontà è in dubbio; «oracolo propizio per un guerriero», perché ha una volontà controllata.
Linea 1 - Nel dubbio siate fermi come un guerriero
Venite o ve ne andate? L'indecisione si corregge maturando una volontà ferma e una chiarezza d'intenti. Quando avete un piano e le risorse per promuoverlo, allora potete avanzare.
Linea 2 - Yang (9): Penetrare sotto il letto. [1] Usare indovini e stregoni in gran numero. Fausto. Nessuna sfortuna.
Commento: Il responso «fausto» del «gran numero» è dovuto al fatto che si è ottenuta la posizione centrale.
Linea 2 - In ginocchio davanti al trono, benedetti dagli sciamani
Siete stati notati e sapete come adeguarvi alla volontà di coloro che vi hanno procurato una posizione di influenza. Approfittate dell'assistenza e dei suggerimenti professionali.
Linea 3 - Yang (9): Penetrare ripetuto. Umiliazione.
Commento: L'«umiliazione» del «penetrare ripetuto» è dovuta al fatto che la volontà si è esaurita.
Linea 3 - Inchinarsi più volte in atto di sottomissione
A causa della vostra testardaggine e incertezza è possibile che dobbiate sottomettervi contro la vostra volontà. Ripetutamente.
Linea 4 - Yin (6): Il rimpianto svanisce. Tre specie di selvaggina catturate in una caccia.
Commento: «Tre specie di selvaggina catturate in una caccia»: i risultati saranno buoni.
Linea 4 - Catturare tre specie di selvaggina
Potreste pensare di aver rinunciato a qualcosa per via della vostra docilità, ma il vostro comportamento vi ha recato vantaggio e raccoglierete i frutti. Agite con fiducia.
Linea 5 - Yang (9): Oracolo fausto. Il rimpianto svanisce. Nulla che non sia propizio. Nessun inizio ma una buona fine. Tre giorni prima del giorno del cambiamento e tre giorni dopo il giorno del cambiamento: fausto. [2]
Commento: Il responso «fausto» della quinta linea dura è dovuto al fatto che è centrale e corretta.
Linea 5 - Brutto inizio, bella fine
Questo è un cambiamento per il meglio: dalla cattiva sorte alla buona. Siate flessibili e adattabili e ne uscirete in testa. È bene ciò che finisce bene.
Linea 6 - Yang (9): Penetrare sotto il letto. [1] Perde ciò che è di sua proprietà e l'ascia di viaggio. Oracolo disastroso.
Commento: «Penetrare sotto il letto», perché in alto è giunta la fine; «perde ciò che è di sua proprietà e l'ascia di viaggio»: il corretto ha raggiunto il «disastroso».
Linea 6 - In ginocchio davanti al trono, perdete la vostra proprietà e la vostra ascia
In un eccesso di arrendevolezza o di avidità e ambizione, avete perso tutto. Ciò che è dato viene tolto.
				 */
			/// </summary>
			public class TheProceedHumbly : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Avanzare e retrocedere. Oracolo propizio per un guerriero.",
							"Penetrare sotto il letto. [1] Usare indovini e stregoni in gran numero. Fausto. Nessuna sfortuna.",
							"Penetrare ripetuto. Umiliazione.",
							"Il rimpianto svanisce. Tre specie di selvaggina catturate in una caccia.",
							"Oracolo fausto. Il rimpianto svanisce. Nulla che non sia propizio. Nessun inizio ma una buona fine. Tre giorni prima del giorno del cambiamento e tre giorni dopo il giorno del cambiamento: fausto. [2]",
							"Penetrare sotto il letto. [1] Perde ciò che è di sua proprietà e l'ascia di viaggio. Oracolo disastroso.",
						};
					}
					
				}
				public override int Id => 57;
				public override string Sentency { get; set; } = "Il Mite. Piccola riuscita. Propizio quando c'è un luogo in cui andare. Propizio vedere un grande uomo.";

				public override string Comment => "Il Mite. Il raddoppiamento del trigramma del Vento significa reiterare ordini. La linea dura del Mite è centrale e corretta [nella quinta posizione] quindi la volontà sarà realizzata. Le linee morbide sono devote alle linee dure [che stanno sopra di loro]; per questo è detto: «piccola riuscita; propizio quando c'è un luogo in cui andare; propizio vedere un grande uomo».";
				public override string Image => "Venti che si susseguono: questa è l'immagine del Mite. Ispirandosi ad essa, il signore diffonde i suoi ordini e porta avanti i suoi affari.";
				public override string Series => "Il viandante non ha nulla che lo accolga. Per questo segue il segno: il Mite (il Penetrante). Il Mite significa andare dentro.";
				public override string MixedSign => "Il Mite è nascosto.";
				public override string AddictionalSentence => "Il Mite mostra come opera la virtù. Mediante il Mite si è in grado di ponderare le cose e di rimanere nascosti. Mediante il Mite si è in grado di tener conto delle circostanze particolari.";
			}
			/// <summary>
			/// 58
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yang (9): Serenità in pace. Fausto.
Commento: Il responso «fausto» della «serenità in pace» deriva dal fatto che il procedere non è ancora diventato dubbioso.
Linea 1 - Discorso spontaneo
Fatevi avanti ed esprimete le vostre idee con gioia e spontaneità. Questa è l'immediata espressione naturale che scaturisce al cospetto di una cosa bella.
Linea 2 - Yang (9): Serenità sincera. Fausto. Il rimpianto svanisce.
Commento: Il responso «fausto» della «serenità sincera» deriva dal fatto che la volontà è degna di fiducia.
Linea 2 - Discorso sincero
Sapere chi siete e che cosa volete vi rende liberi di parlare con il cuore. Conquistate la fiducia del prossimo con la sincerità, non con l'adulazione.
Linea 3 - Yin (6): Serenità che viene allettando. Disastroso.
Commento: Il responso «disastroso» della «serenità che viene allettando» deriva dal fatto che la linea non è adatta alla posizione.
Linea 3 - Un'offerta allettante
State compiacendo voi stessi o state facendo qualcosa per compiacere qualcun altro? Potrebbero giungervi tentazioni dall'esterno. La felicità viene dal di dentro.
Linea 4 - Yang (9): Serenità calcolata. Non c'è tranquillità. Tenendosi lontani dalla malattia, ci sarà felicità.
Commento: «Felicità» nonostante sia una quarta linea dura: ci sarà buona fortuna.
Linea 4 - Calcolare le probabilità
Gioite di ciò che state facendo. È bello ciò che avete. Preoccuparsi e fare congetture vi confonderà e procurerà soltanto agitazione. Portate a termine ciò che avete cominciato.
Linea 5 - Yang (9): Sincerità verso ciò che degrada. Sarà pericoloso.
Commento: «Sincerità verso ciò che degrada»: la posizione è corretta e appropriata.
Linea 5 - Fidarsi di un ladro
Siete circondati da persone che vogliono abusare della vostra bontà e per questo potrebbero mentirvi. Guardatevi dalle persone di cui vi fidate.
Linea 6 - Yin (6): Serenità protratta [o: indotta].
Commento: Una linea morbida in alto dà «serenità protratta» perché non è ancora splendida.
Linea 6 - Incoraggiati a farsi avanti
Questa è l'occasione per voi di farvi avanti e partecipare. Siete anche tentati di indulgere verso voi stessi.
				 */
			/// </summary>
			public class TheClear : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Serenità in pace. Fausto.",
							"Serenità sincera. Fausto. Il rimpianto svanisce.",
							"Serenità che viene allettando. Disastroso.",
							"Serenità calcolata. Non c'è tranquillità. Tenendosi lontani dalla malattia, ci sarà felicità.",
							"Sincerità verso ciò che degrada. Sarà pericoloso.",
							"Serenità protratta [o: indotta].",
						};
					}
					
				}
				public override int Id => 58;
				public override string Sentency { get; set; } = "Il Sereno. Riuscita. Propizio oracolo.";

				public override string Comment => "Il Sereno vuol dire: gioia. C'è una linea dura centrale e una linea morbida sopra in ciascun trigramma. Il \"gioioso\" dà origine ad un «propizio oracolo». Esso è devoto al cielo e in accordo col desiderio della gente. Se la gente è guidata in modo gioioso, dimentica le proprie fatiche; se si affrontano le difficoltà con gioia, la gente dimentica la paura della morte. Il \"gioioso\" è grande e incoraggia la gente.";
				public override string Image => "Laghi collegati: questa è l'immagine del Sereno. Ispirandosi ad essa, il signore si unisce con i suoi amici nelle attività didattiche e nelle esercitazioni.";
				public override string Series => "Quando si è penetrati in una cosa allora si è allegri. Per questo segue il segno: il Sereno. Il Sereno significa rallegrarsi.";
				public override string MixedSign => "Il Sereno è palese.";
				public override string AddictionalSentence => "";
			}
			/// <summary>
			/// 59
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yin (6): Servirsi dell'aiuto di un cavallo vigoroso. Fausto.
Commento: Il responso «fausto» di una linea morbida in basso è dovuto al suo essere devota.
Linea 1 - Tratti in salvo con l'aiuto di un cavallo robusto
Con l'aiuto di un amico potete uscire da una situazione prima che vi sfugga di mano. Una situazione devastante può essere affrontata, se presa in tempo.
Linea 2 - Yang (9): Durante la dissoluzione si corre al proprio appoggio. Il rimpianto svanisce.
Commento: «Durante la dissoluzione si corre al proprio appoggio», ottenendo ciò che si desidera.
Linea 2 - Trasportati dall'inondazione
Potreste non sapere dove vi sta portando la marea degli eventi. Cavalcate l'onda dell'energia. Cogliete le opportunità quando si presentano. Qualcuno è dalla vostra parte e vi aiuterà.
Linea 3 - Yin (6): Dissolvere il proprio corpo. Nessun rimpianto.
Commento: «Dissolvere il proprio corpo»: la volontà è diretta verso il trigramma esterno.
Linea 3 - Travolti dall'inondazione
Gli eventi esterni vi hanno distolto dai vostri problemi. Aiutate voi stessi aiutando gli altri.
Linea 4 - Yin (6): Dissolvere il proprio gruppo. Totalmente fausto. La dissoluzione porta a un colle [o: un cumulo]. Non è ciò che ci si aspetta comunemente.
Commento: «Dissolvere il proprio gruppo; totalmente fausto»: lo splendore è grande.
Linea 4 - Trascinati dal gregge sulla cima della collina
Gli eventi potrebbero sradicarvi da un percorso abituale e allontanarvi dal pericolo. Abbandonate le vecchie abitudini. Fate cose insolite, cambiate la vostra vita.
Linea 5 - Yang (9): Dissolvere come il sudore. Un grande grido. Dissolvere la residenza del Re. Nessuna sfortuna. [1]
Commento: «La residenza del Re» non avrà «nessuna sfortuna», perchè la posizione è corretta.
Linea 5 - Madidi di sudore, venite convocati al palazzo del re
La proposta di una nuova esperienza potrebbe farvi sudare e tremare. Accettate l'offerta soltanto se riuscite a conservare la calma.
Linea 6 - Yang (9): Dissolvere il proprio sangue. Andarsene lontano, uscire: nessuna sfortuna.
Commento: «Dissolvere il proprio sangue», cioè: allontanarsi dal danno.
Linea 6 - Trascinati lontano dalle vostre angosce
Nonostante siate riusciti a sopravvivere a un incontro pericoloso, potreste ancora desiderare di trovare un ambiente più piacevole dove non venite più minacciati. Lontani dal pericolo.
				 */
			/// </summary>
			public class TheDissolution : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Servirsi dell'aiuto di un cavallo vigoroso. Fausto.",
							"Durante la dissoluzione si corre al proprio appoggio. Il rimpianto svanisce.",
							"Dissolvere il proprio corpo. Nessun rimpianto.",
							"Dissolvere il proprio gruppo. Totalmente fausto. La dissoluzione porta a un colle [o: un cumulo]. Non è ciò che ci si aspetta comunemente.",
							"Dissolvere come il sudore. Un grande grido. Dissolvere la residenza del Re. Nessuna sfortuna. [1]",
							"Dissolvere il proprio sangue. Andarsene lontano, uscire: nessuna sfortuna.",
						};
					}
					
				}
				public override int Id => 59;
				public override string Sentency { get; set; } = "La Dissoluzione. Riuscita. Il Re si reca al tempio ancestrale. Propizio attraversare il grande fiume. Propizio oracolo.";

				public override string Comment => "La Dissoluzione. «Riuscita»: perché la linea dura arriva nel trigramma inferiore e non si esaurisce, mentre la linea morbida ottiene una posizione nel trigramma superiore e ed in armonia con la linea [dura] sopra di lei. «Il Re si reca al tempio ancestrale», significa che il Re è nel centro [una linea yang in quinta posizione]. «Propizio attraversare il grande fiume» deriva dall'utilizzare il potere del Legno [il trigramma superiore, che galleggia sull'Acqua].";
				public override string Image => "Il Vento si muove sull'Acqua: questa è l'immagine della Dissoluzione. Ispirandosi ad essa, gli antichi Re sacrificavano al Signore Iddio ed erigevano templi ancestrali.";
				public override string Series => "Alla gioia segue la dispersione. Per questo segue il segno: la Dissoluzione. Dissoluzione significa disseminare.";
				public override string MixedSign => "Dissoluzione significa disseminare.";
				public override string AddictionalSentence => "Essi levigarono tronchi per farne navi e indurirono legname nel fuoco per farne remi. L'utilità delle navi e dei remi consisteva nel facilitare gli scambi. Questo lo trassero certamente dal segno: la Dissoluzione.";
			}
			/// <summary>
			/// 60
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yang (9): Non uscire dalla porta nel cortile. Nessuna sfortuna.
Commento: «Non uscire dalla porta nel cortile» vuol dire che si sa quando il movimento è impedito.
Linea 1 - Rimanete a casa, lontani dal pericolo
Se rimanete dove siete vi eviterete dei guai. Aspettate un'opportunità più favorevole. Distinguete ciò che è possibile da ciò che non lo è. Controllatevi. Siate consci dei vostri limiti.
Linea 2 - Yang (9): Non uscire dal cancello del cortile. Disastro.
Commento: «Non uscire dal cancello del cortile: disastro», perché si perde l'occasione alla fine.
Linea 2 - Se non lasciate la vostra casa, perdete la vostra occasione
Se vi tratterrete ora, perderete la vostra occasione. Potreste non riuscire ad agire quando sarebbe vantaggioso per voi farlo. Timori e dubbi. Trovate uno sfogo per le vostre energie prima di trovarvi in una condizione di frustrazione.
Linea 3 - Yin (6): Non conoscere delimitazione. Lamentarsi. Nessuna sfortuna.
Commento: «Non conoscere delimitazione» provoca il «lamentarsi»: di chi è la colpa (= la sfortuna)?
Linea 3 - Un'assenza deplorevole di controllo
Se non siete riusciti a controllare una situazione prima che vi sfuggisse di mano, la colpa è unicamente vostra. Siete responsabili della vostra condizione.
Linea 4 - Yin (6): Quieta delimitazione. Riuscita.
Commento: La «riuscita» della «quieta delimitazione» deriva dal fatto che si supporta la via [della linea dura] superiore.
Linea 4 - Sicuri all'interno dei confini della vostra vita
Siete abituati a condurre una vita ordinata. La disciplina e il controllo a cui vi conformate sono una struttura confortante e affidabile. Rimanete nella vostra routine.
Linea 5 - Yang (9): Piacevole delimitazione. Fausto. Procedere porta riconoscimento.
Commento: Il responso «fausto» della «piacevole delimitazione» deriva dal fatto che ha ottenuto la posizione centrale.
Linea 5 - Soddisfatti della disciplina
L'autodisciplina è la chiave della vostra felicità. Alla disciplina e a una struttura nella vostra vita, prevedibilmente seguiranno ordine e progresso.
Linea 6 - Yin (6): Amara delimitazione. Oracolo: disastroso. Il rimpianto svanisce.
Commento: «Amara delimitazione. Oracolo: disastroso», perché la sua via si esaurisce.
Linea 6 - Restrizione dolorosa
Rendendovi la vita troppo difficile, vi procurate esaurimento, amarezza e risentimento. Non fatevene una colpa. Alleggerite la situazione e prendete una direzione nuova.
				 */
			/// </summary>
			public class TheDelimitation : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Non uscire dalla porta nel cortile. Nessuna sfortuna.",
							"Non uscire dal cancello del cortile. Disastro.",
							"Non conoscere delimitazione. Lamentarsi. Nessuna sfortuna.",
							"Quieta delimitazione. Riuscita.",
							"Piacevole delimitazione. Fausto. Procedere porta riconoscimento.",
							"Amara delimitazione. Oracolo: disastroso. Il rimpianto svanisce.",
						};
					}
					
				}
				public override int Id => 60;
				public override string Sentency { get; set; } = "La Delimitazione. Riuscita. Amara delimitazione. L'oracolo non è possibile [che sia favorevole].";

				public override string Comment => "Delimitazione. «Riuscita»: le linee dure e morbide sono uguali in numero e le linee dure hanno la posizione centrale. «Amara delimitazione: l'oracolo non è possibile», perché la sua via si esaurisce. Il \"gioioso\" insieme al \"pericoloso\" danno posizioni adatte nella Delimitazione, muovendosi verso il centrale e il corretto [quinta linea]. Quando il cielo e la terra hanno le loro delimitazioni, le quattro stagioni completano le loro funzioni. Quando la Delimitazione è debitamente controllata [da un buon governo], i beni non vengono pregiudicati e la gente non è danneggiata.";
				public override string Image => "L'Acqua sopra il Lago: questa è l'immagine della Delimitazione. Ispirandosi ad essa, il signore istituisce numero e misura e discute la virtù e la retta condotta.";
				public override string Series => "Le cose non possono stare per sempre disperse. Per questo segue il segno: la Delimitazione.";
				public override string MixedSign => "Delimitazione  significa tenere a freno.";
				public override string AddictionalSentence => "";
			}
			/// <summary>
			/// 61
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yang (9): Rendere omaggio ai defunti (= essere preparati). Fausto. Ci sarà un'inaspettata calamità. Nessuna festa. [1]
Commento: La linea dura in basso indica che è «fausto» «rendere omaggio ai defunti», perché la volontà non è ancora mutata.
Linea 1 - Siate pronti
Costruite con calma la stabilità interiore della vostra mente. Non lasciatevi distrarre da questo compito. Potreste avere una sorpresa. Qualcuno potrebbe tradirvi. Siate pronti.
Linea 2 - Yang (9): Una gru chiama nell'ombra, il suo piccolo le risponde. Ho una coppa di buon vino, la voglio dividere con te.
Commento: «Il suo piccolo le risponde»: la linea centrale indica il desiderio del cuore.
Linea 2 - Una gru lancia un richiamo nell'ombra, il suo piccolo risponde
Una chioccia, nascosta nell'ombra, lancia un richiamo e il suo piccolo risponde. È una reazione naturale, perfetta e completa. Rispondere a un richiamo che viene dal cuore.
Linea 3 - Yin (6): Trovare il nemico. Ora batte il tamburo, ora cessa; ora pianti, ora canti.
Commento: «Ora batte il tamburo, ora cessa», perché [la linea] non è adatta alla posizione.
Linea 3 - Ora batte il tamburo, ora cessa; ora piange, ora canta
Una certa attività ha monopolizzato la vostra attenzione. Il vostro benessere e la vostra integrità dipendono da essa. Se l'oggetto della vostra attenzione è degno, prospererete, altrimenti fallirete.
Linea 4 - Yin (6): La luna è quasi piena. Un cavallo della pariglia va perduto. Nessuna sfortuna.
Commento: «Un cavallo della pariglia va perduto», perché si distacca dal suo simile e si volge verso l'alto.
Linea 4 - Un cavallo solitario si allontana dal branco
Prendete distanza da qualsiasi influenza negativa. Ciò potrebbe significare abbandonare il gruppo a cui appartenete. Ne trarrete vantaggio, specie se aspirate a qualcosa di più grande.
Linea 5 - Yang (9): Essere sinceri conduce a legarsi insieme. Nessuna sfortuna.
Commento: «Essere sinceri conduce a legarsi insieme», perché la posizione è corretta e appropriata.
Linea 5 - Sincerità di tutto cuore
Siete animati da nobili ideali che vi proteggono quando vacillate. La vostra integrità e sincerità convincerà gli altri a unirsi a voi.
Linea 6 - Yang (9): Canto di gallo che giunge fino al cielo. Oracolo disastroso.
Commento: «Canto di gallo che giunge fino al cielo»: come potrebbe ciò durare a lungo?
Linea 6 - Galline che volano alto
Quando una gallina arrogante vola troppo in alto nel cielo, cade a terra. Vi siete assunti troppe responsabilità. Se continuate così, soffrirete.
				 */
			/// </summary>
			public class TheInnerTruth : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Rendere omaggio ai defunti (= essere preparati). Fausto. Ci sarà un'inaspettata calamità. Nessuna festa. [1]",
							"Una gru chiama nell'ombra, il suo piccolo le risponde. Ho una coppa di buon vino, la voglio dividere con te.",
							"Trovare il nemico. Ora batte il tamburo, ora cessa; ora pianti, ora canti.",
							"La luna è quasi piena. Un cavallo della pariglia va perduto. Nessuna sfortuna.",
							"Essere sinceri conduce a legarsi insieme. Nessuna sfortuna.",
							"Canto di gallo che giunge fino al cielo. Oracolo disastroso.",
						};
					}
					
				}
				public override int Id => 61;
				public override string Sentency { get; set; } = "La Verità interiore. Maiali e pesci. Fausto. Propizio attraversare il grande fiume. Propizio oracolo.";

				public override string Comment => "La Verità interiore. Le linee morbide sono nella parte più interna dell'esagramma, con le linee dure in posizione centrale [seconda e quinta linea]: il \"gioioso\" e il Mite. Con la verità si riesce a trasformare il Paese. «Maiali e pesci: fausto», perché il potere della fiducia si estende persino a maiali e pesci. «Propizio attraversare il grande fiume», utilizzando un Legno [una barca] cavo [come gli spazi al centro dell'esagramma]. La Verità interiore dà un «propizio oracolo» che è in accordo col cielo.";
				public override string Image => "Il Vento sopra il Lago: questa è l'immagine della Verità interiore. Ispirandosi ad essa, il signore riesamina le cause penali e rinvia le condanne a morte.";
				public override string Series => "Attraverso la delimitazione le cose vengono rese degne di fiducia. Per questo segue il segno: la Verità interiore.";
				public override string MixedSign => "Verità interiore significa rimanere fidato.";
				public override string AddictionalSentence => "";
			}
			/// <summary>
			/// 62
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yin (6): L'uccello in volo. Implica disastro.
Commento: «L'uccello in volo, implica disastro»: non si può fare nulla.
Linea 1 - L'uccello in volo incontra la sventura
Manifestate una tendenza insopprimibile a muovervi prima di essere pronti. Questo vi porterà a fare un incontro sfortunato che non sarete in grado di evitare.
Linea 2 - Yin (6): Oltrepassare l'avo, incontrare l'ava. Non raggiungere il proprio sovrano, incontrare il funzionario. Nessuna sfortuna.
Commento: «Non raggiungere il proprio sovrano», perché al funzionario non è permesso eccedere.
Linea 2 - Assente il capo, incontro con il funzionario
Se non potete raggiungere il capo, accontentatevi di vedere il secondo in comando. Anche se potreste perdere una grande occasione, non ne perderete una di minor conto. Accontentatevi di raggiungere una parte dei vostri obiettivi.
Linea 3 - Yang (9): Non oltrepassare. Evita questo, altrimenti chi segue può aggredirti. Disastro.
Commento: «Altrimenti chi segue può aggredirti»: quale disastro è questo!
Linea 3 - Aggrediti alle spalle
Se vi allontanate troppo, potreste essere attaccati. Mettersi in mostra crea incidenti. State in guardia e proteggetevi le spalle.
Linea 4 - Yang (9): Nessuna sfortuna. Non oltrepassare, incontrare. Andare avanti è pericoloso. Bisogna stare in guardia.  Non servirsene per un oracolo a lungo termine.
Commento: «Non oltrepassare, incontrare», perché la linea non è adatta alla posizione. «Andare avanti è pericoloso; bisogna stare in guardia»: questo non può durare a lungo.
Linea 4 - Guardatevi da un incontro pericoloso
La strada intrapresa potrebbe portare alla sventura. Se decidete di proseguire, siate cauti. Sarebbe meglio che rimaneste dove siete. State attenti a non sfidare la vostra buona sorte.
Linea 5 - Yin (6): Dense nuvole ma nessuna pioggia, dalle nostre contrade occidentali. Il Duca arpiona un animale che sta nella caverna.
Commento: «Dense nuvole ma nessuna pioggia»: sono troppo in alto.
Linea 5 - Nuvole, nessuna pioggia
C'è aria di pioggia, ma le nuvole si allontanano senza che cada una sola goccia. Soddisferete un'ambizione minore, non la più grande. Accettate ciò che vi si presenta e siatene soddisfatti. Fate le cose semplici.
Linea 6 - Yin (6): Non incontrare, oltrepassare. L'uccello in volo se ne va lontano. [1] Disastro. Questo significa una catastrofica calamità.
Commento: «Non incontrare, oltrepassare»: ciò è dovuto alla prepotenza eccessiva.
Linea 6 - L'uccello in volo viene catturato
Siete solo capaci di andare incontro a guai peggiori. Dimostrate la vostra arroganza tentando un'impresa al di sopra delle vostre capacità.
				 */
			/// </summary>
			public class TheSmallExceed : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"L'uccello in volo. Implica disastro.",
							"Oltrepassare l'avo, incontrare l'ava. Non raggiungere il proprio sovrano, incontrare il funzionario. Nessuna sfortuna.",
							"Non oltrepassare. Evita questo, altrimenti chi segue può aggredirti. Disastro.",
							"Nessuna sfortuna. Non oltrepassare, incontrare. Andare avanti è pericoloso. Bisogna stare in guardia.  Non servirsene per un oracolo a lungo termine.",
							"Dense nuvole ma nessuna pioggia, dalle nostre contrade occidentali. Il Duca arpiona un animale che sta nella caverna.",
							"Non incontrare, oltrepassare. L'uccello in volo se ne va lontano. [1] Disastro. Questo significa una catastrofica calamità.",
						};
					}
					
				}
				public override int Id => 62;
				public override string Sentency { get; set; } = "La Preponderanza del piccolo. Riuscita. Propizio oracolo. Va bene fare piccole cose, non va bene fare cose grandi. L'uccello in volo reca un messaggio: Non è appropriato aspirare verso l'alto, è appropriato rimanere in basso. Molto fausto.";

				public override string Comment => "La Preponderanza del piccolo. Il piccolo è eccessivo, avendo «riuscita», e trapassa in un «propizio oracolo» quando si muove in armonia col tempo. Le linee morbide, poiché hanno una posizione centrale, sono «fauste per fare piccole cose»; le linee dure, poiché perdono le loro posizioni [prima e quinta] e non sono centrali, significano «non va bene fare cose grandi». Il segno ha la forma di un uccello in volo: «un uccello in volo reca un messaggio: non è appropriato aspirare verso l'alto, è appropriato rimanere in basso; molto fausto»: il trigramma superiore è rovesciato, quello in basso è devoto.";
				public override string Image => "Il Tuono sopra il Monte: questa è l'immagine della Preponderanza del piccolo. Ispirandosi ad essa, il signore eccede di poco il limite di cortesia nel comportamento, quello di cordoglio nei funerali, e quello di parsimonia nelle spese.";
				public override string Series => "Quando si ha la fiducia degli esseri, sicuramente la si mette in pratica. Per questo segue il segno: la Preponderanza del piccolo.";
				public override string MixedSign => "La Preponderanza del piccolo significa passare oltre.";
				public override string AddictionalSentence => "I signori spaccarono un legno e ne fecero un pestello e aprirono una cavità nella terra per avere un mortaio. L'utilità del pestello e del mortaio andò a vantaggio di tutti gli uomini. Questo lo trassero certamente dal segno: la Preponderanza del piccolo.";
			}
			/// <summary>
			/// 63
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yang (9): Frenare le proprie ruote. Bagnarsi la coda. Nessuna sfortuna.
Commento: «Frenare le proprie ruote», significa «nessuna sfortuna».
Linea 1 - La piccola volpe si bagna la coda
Ritiratevi e mettete un freno in questa prima fase. Difficoltà di poco conto sono un monito per quelle venture. Preparatevi prima che si presentino problemi.
Linea 2 - Yin (6): Una donna perde il suo velo. Non cercarlo; lo riotterrà il settimo giorno.
Commento: «Lo riotterrà il settimo giorno», grazie alla via centrale [di questa linea].
Linea 2 - Ella perde il velo, le sarà restituito entro una settimana
Ciò che è andato perso vi verrà restituito, senza nessuno sforzo particolare da parte vostra.
Linea 3 - Yang (9): Il Grande Avo attacca il Paese dei Diavoli; dopo tre anni lo vince. Non usare le persone meschine.
Commento: «Dopo tre anni lo vince»: ciò è estenuante [per le persone meschine].
Linea 3 - Tre anni per sottomettere le terre selvagge
Una persona deve essere forte e determinata per affrontare un'impresa difficile senza logorarsi. La vittoria potrebbe lasciarvi esausti. Siete all'altezza?
Linea 4 - Yin (6): Per tappare il buco della barca si usano vestiti stracciati. [1]  Stare in guardia per tutto il giorno.
Commento: «Stare in guardia per tutto il giorno», perché c'è ragione di sospettare.
Linea 4 - La vostra veste si imbeve d'acqua
Impedimenti di poco conto vi rendono dubbiosi, attenti e nervosi. Premunitevi contro incidenti prevedibili.
Linea 5 - Yang (9): Il vicino dell'Oriente macella un bue, non come il vicino dell'Occidente che fa una piccola offerta. E' quest'ultimo che riceve la vera benedizione.
Commento: «Il vicino dell'Oriente macella un bue» ma non si adegua al tempo come «il vicino dell'Occidente», il quale «riceve la vera benedizione»: avrà un esito molto fausto.
Linea 5 - Il sacrificio di un bue non è benedetto quanto un'offerta modesta
Un gesto semplice e sincero conta più di un'esibizione grandiosa e fasulla.
Linea 6 - Yin (6): Bagnarsi la testa. Pericoloso.
Commento: «Bagnarsi la testa»: come potrebbe ciò durare a lungo?
Linea 6 - Bagnarsi la testa
Riconoscete quando siete al di sopra delle vostre capacità. Il disinteresse per l'epilogo di un'impresa creerà problemi. Le difficoltà non sono eccessive, passeranno presto.
				 */
			/// </summary>
			public class TheAfterCompletion : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Frenare le proprie ruote. Bagnarsi la coda. Nessuna sfortuna.",
							"Una donna perde il suo velo. Non cercarlo; lo riotterrà il settimo giorno.",
							"Il Grande Avo attacca il Paese dei Diavoli; dopo tre anni lo vince. Non usare le persone meschine.",
							"Per tappare il buco della barca si usano vestiti stracciati. [1]  Stare in guardia per tutto il giorno.",
							"Il vicino dell'Oriente macella un bue, non come il vicino dell'Occidente che fa una piccola offerta. E' quest'ultimo che riceve la vera benedizione.",
							"Bagnarsi la testa. Pericoloso.",
						};
					}
					
				}
				public override int Id => 63;
				public override string Sentency { get; set; } = "Dopo il compimento. Riuscita. Propizio oracolo per le piccole cose. Fausto all'inizio, scompiglio alla fine.";

				public override string Comment => "Dopo il compimento. «Riuscita»: ma la riuscita è nel «piccolo». «Propizio oracolo», poiché sia le linee dure che quelle morbide sono centrali e adatte alla loro posizione. «Fausto all'inizio», perché la [seconda] linea morbida è centrale. Fermarsi «alla fine» porta «scompiglio», perché la via si esaurisce.";
				public override string Image => "L'Acqua sopra il Fuoco: questa è l'immagine delle cose Dopo il compimento. Ispirandosi ad essa, il signore pondera il disastro e se ne premunisce per tempo.";
				public override string Series => "Chi eccede le cose, è in grado di portarle a compimento. Per questo segue il segno: Dopo il compimento.";
				public override string MixedSign => "Dopo il compimento significa consolidamento.";
				public override string AddictionalSentence => "";
			}
			/// <summary>
			/// 64
			/*
			 LINEE MOBILI	LINEE MOBILI
Linea 1 - Yin (6): Bagnarsi la coda. Umiliazione.
Commento: «Bagnarsi la coda»: significa che non si conoscono i propri limiti.
Linea 1 - Una coda bagnata
Il ghiaccio, nel mezzo del fiume, si spacca sotto le zampe della piccola volpe. Evitate di farvi coinvolgere in qualcosa finché non saprete di poterla portare a termine. Siate consci dei vostri limiti.
Linea 2 - Yang (9): Frenare le proprie ruote. Oracolo fausto.
Commento: La seconda linea dura ha un «oracolo fausto» perché si muove correttamente nel centro.
Linea 2 - Frenare le ruote
Frenate, vi aspettano sorprese. Farete bene a procedere con lentezza ed estrema cautela. Prendete precauzioni contro possibili incidenti.
Linea 3 - Yin (6): Prima del compimento. Per un'impresa: disastroso. Propizio attraversare il grande fiume.
Commento: «Prima del compimento; per un'impresa: disastroso», perché la posizione [della linea] non è adatta.
Linea 3 - Quando siete pronti attraversate il fiume
Siete pronti a procedere, ma avete bisogno di aiuto per superare gli ostacoli sul vostro cammino. Ciò potrebbe significare tirarvi indietro e ricominciare daccapo per riuscire nell'intento.
Linea 4 - Yang (9): Oracolo fausto. Il rimpianto svanisce. Attaccare come tuono il Paese dei Diavoli; dopo tre anni, si è premiati nel Grande Paese.
Commento: «Oracolo fausto; il rimpianto svanisce»: la volontà sarà realizzata.
Linea 4 - Domate la terra selvaggia dopo tre anni
Un'impresa che dura tre anni richiede dedizione e forte volontà per lavorare a ciò in cui si crede. Agite con coraggio, forza e determinazione e verrete ricompensati.
Linea 5 - Yin (6): Oracolo fausto. Nessun rimpianto. Lo splendore del signore. Essere sinceri. Fausto.
Commento: «Lo splendore del signore»: la sua luce [linea yang in posizione yang] è fausta.
Linea 5 - Pagate il prossimo giro
Siate generosi e condividete la vostra buona sorte. Se vincete alla lotteria, il minimo che potete fare è pagare da bere a tutti.
Linea 6 - Yang (9): Essere sinceri mentre si beve vino. Nessuna sfortuna. Bagnarsi la testa. Si è sinceri, ma questo lo si perde.
Commento: «Si beve vino» e «ci si bagna la testa»: non si conosce la moderazione.
Linea 6 - Ubriachi al punto da bagnarvi la testa
Non c'è nulla di male nel bere, ma non eccedete. Avete un'esagerata opinione di voi stessi. Il successo vi ha dato alla testa?
				 */
			/// </summary>
			public class TheBeforeTheAge : Hexagram
			{
				public override string[] MobileInterpretations
				{
					get
					{
						return new[]
						{
							"Bagnarsi la coda. Umiliazione.",
							"Frenare le proprie ruote. Oracolo fausto.",
							"Prima del compimento. Per un'impresa: disastroso. Propizio attraversare il grande fiume.",
							"Oracolo fausto. Il rimpianto svanisce. Attaccare come tuono il Paese dei Diavoli; dopo tre anni, si è premiati nel Grande Paese.",
							"Oracolo fausto. Nessun rimpianto. Lo splendore del signore. Essere sinceri. Fausto.",
							"Essere sinceri mentre si beve vino. Nessuna sfortuna. Bagnarsi la testa. Si è sinceri, ma questo lo si perde.",
						};
					}
					
				}
				public override int Id => 64;
				public override string Sentency { get; set; } = "Prima del compimento. Riuscita. La piccola volpe, quando ha quasi compiuto la traversata, si bagna la coda. Nulla è propizio.";

				public override string Comment => "Prima del compimento. «Riuscita»: perché la linea morbida ottiene il centro [quinta linea]. «La piccola volpe ha quasi compiuto la traversata», ma non ha ancora superato il centro [del problema]. «Si bagna la coda. Nulla è propizio», poiché la cosa non continua fino alla fine. Sebbene nessuna linea sia nella posizione adatta, le linee dure e morbide si corrispondono [nei due trigrammi].";
				public override string Image => "Il Fuoco sopra l'Acqua: questa è l'immagine delle cose Prima del compimento. Ispirandosi ad essa, il signore con cautela distingue le cose e mette ciascuna di esse al suo posto.";
				public override string Series => "La successione delle cose non può finire. Per questo segue, in chiusura, il segno: Prima del compimento.";
				public override string MixedSign => "Prima del compimento è l'esaurimento del mascolino.";
				public override string AddictionalSentence => "";
			}
		}
	}
}
