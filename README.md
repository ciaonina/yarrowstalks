# README #

The ***I Ching*** ([î tɕíŋ]), or ***Classic of Changes***, is an ancient divination text and the oldest of the Chinese classics.

### Three-coin method ###

The three coin method came into currency over a thousand years later. The quickest, easiest, and most popular method by far, it has largely supplanted the yarrow stalks, but produces outcomes with different likelihoods. Three coins are tossed at once. Each coin is given a value of 2 or 3 depending upon whether it is tails or heads respectively. Six such tosses make the hexagram.